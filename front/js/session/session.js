$(document).ready(function () {
  $('#logout1').on("click",function(){
    document.cookie ='carrito=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    window.location.replace("/session/logout");
  });
    /*var form = $("#newClient");
    $('form > input').keyup(function() {

        *//*var empty = false;
         $('form > input').each(function() {
         if ($(this).val() == '') {
         empty = true;
         }
         });
         if (empty) {
         $('#register').attr('disabled', 'disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
         } else {
         $('#register').removeAttr('disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
         }*//*

        $('form > input').each(function() {
            if ($(this).val() == '') {
                empty = true;
            }
        });
        var pass = document.getElementById("password").value;
        var pass2 = document.getElementById("confirmPassword").value;
        var acepto = $( "input:checked" ).length;
        if ( pass=='' && pass2=='' && acepto==0) {
            $('#register').attr('disabled', 'disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        } else {
            $('#register').removeAttr('disabled'); // updated according to http://stackoverflow.com/questions/7637790/how-to-remove-disabled-attribute-with-jquery-ie
        }

    });*/
    var reference = $("#noreference");
    var reference2 = $("#noreference2");
    reference.on("click", function(){
        document.getElementById('namerefe').disabled=true;
        document.getElementById('lastnamerefe').disabled=true;
        document.getElementById('secondnamerefe').disabled=true;
        document.getElementById('emailrefe').disabled=true;
        document.getElementById('numberrefe').disabled=true;
        document.getElementById('phonerefe').disabled=true;
    });
    reference2.on("click", function(){
        document.getElementById('namerefe').disabled = false;
        document.getElementById('lastnamerefe').disabled = false;
        document.getElementById('secondnamerefe').disabled = false;
        document.getElementById('emailrefe').disabled = false;
        document.getElementById('numberrefe').disabled = false;
        document.getElementById('phonerefe').disabled = false;
    });

    /* Session */
    var session = $("#session");
    if(session.length>=1){
        session.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Es necesaria la dirección de correo electrónico'
                        },
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico váida.'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'La contraseña es necesaria.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#session-loading").removeClass("hide").addClass("in");
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#container-messages").removeClass("hide").addClass("in");
            var urlnow = window.location;
            var values = {
                key: $('#key-security').attr('data-key')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            var message_modal = $("#message_modal");
            $.ajax({
                type : "POST",
                url : "/session",
                data : data,
                dataType : "JSON",
                success : function(response){
                    $("#myModalSession").modal("show");
                    if(response.message=="SUCCESS" && response.code==200){
                        message_modal.html("SesiÃ³n correcta espere un momento ....");
                        window.location = urlnow;
                    }
                    else if(response.code==300){
                        message_modal.html("ContraseÃ±a incorrecta");
                        $("#psin").text(response.message);
                        $("#password-incorrect").removeClass("hide").addClass("in");
                    }else if(response.code==400){
                        message_modal.html("Email no validado");
                        $("#email-incorrect").removeClass("hide").addClass("in");
                    }
                    else if(response.code==404){
                        message_modal.html("Email no encontrado");
                        $("#all-incorrect").removeClass("hide").addClass("in");
                    }
                    $("#session-loading").removeClass("in").addClass("hide");
                    $("#btn-submit").removeAttr("disabled","disabled").val("Iniciar");
                },
                error : function(){

                },
                complete : function(){
                    setTimeout(function(){
                        $("#password-incorrect").removeClass("in").addClass("hide");
                        $("#email-incorrect").removeClass("in").addClass("hide");
                        $("#all-incorrect").removeClass("in").addClass("hide");
                        $("#container-messages").removeClass("in").addClass("hide");
                    },3000);
                }
            });
        });
    }
    /* End Session */
    $(document).on('click', '#iniciar-sesion', function(e) {
        e.preventDefault();
        jQuery("#iniciosesionmodal").modal("show")
    })
    /* New Register */
    var newClient = $("#newClientRegister");
    if(newClient.length>=1){
        var empty = false;
        $(".validate_keyup").on("keyup",function(){
            empty = true;
            var elements = document.getElementsByClassName("validate_keyup");
            for (var i = 0; i < elements.length; i++) {
                if(elements[i].value == "" || $( "input:checked" ).length == 0) {
                    $('#register').attr('disabled', 'disabled');
                    empty=false;
                    //Do something here
                }
            }
            if(empty){
                $('#register').removeAttr('disabled').removeClass("disabled");
            }
        });


        $('#register').attr('disabled', 'disabled');
        newClient.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                email: {
                    validators: {
                        validators: {notEmpty: {}},
                        emailAddress: {
                            message: 'Correo electrónico no válido.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe.',
                            url: '/session/validate-email',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'La contraseña es necesaria'
                        },
                        stringLength: {
                            min: 8,
                            max: 30,
                            message: 'Mínimo 8 caracteres.'
                        }
                    }
                },
                confirmPassword: {
                    validators: {
                        identical: {
                            field: 'password',
                            message: 'Las contraseñas no coinciden'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#session-loading").removeClass("hide").addClass("in");
            $("#btn-submit").attr("disabled","disabled").val("Creando usuario");
            $("#container-messages").removeClass("hide").addClass("in");
            $.ajax({
                type : "POST",
                url : "/session/save",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#email-validate").text(response.email);
                        $("#email-validate").attr(response.email);
                        $("#registercomplete").modal("show");
                        $('#registercomplete').on('hidden.bs.modal', function (e) {
                            window.location = "/"
                            /*window.location = response.url*/

                        });

                    }else if(response.code==300){
                        $("#password-incorrect").removeClass("hide").addClass("in");
                    }else if(response.code==400){
                        $("#email-incorrect").removeClass("hide").addClass("in");
                    }
                    else if(response.code==404){
                        $("#all-incorrect").removeClass("hide").addClass("in");
                    }
                    $("#session-loading").removeClass("in").addClass("hide");
                    $("#btn-submit").removeAttr("disabled","disabled").val("Crear usuario");
                },
                error : function(){

                }
            });
        });
    }
    /* New Register */
    /* New Register */
    var newClient2 = $("#newClientRegister2");
    if(newClient2.length>=1){


        var empty = false;
        $(".validate_keyup").on("keyup",function(){
            empty = true;
            var elements = document.getElementsByClassName("validate_keyup");
            for (var i = 0; i < elements.length; i++) {
                if(elements[i].value == "" || $( "input:checked" ).length == 0) {
                    $('#register').attr('disabled', 'disabled');
                    empty=false;
                    //Do something here
                }
            }
            if(empty){
                $('#register').removeAttr('disabled').removeClass("disabled");
            }
        });


        $('#register').attr('disabled', 'disabled');
        newClient2.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                email: {
                    validators: {
                        validators: {notEmpty: {}},
                        emailAddress: {
                            message: 'Correo electrónico no válido.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe.',
                            url: '/session/validate-email',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'La contraseña es necesaria'
                        },
                        stringLength: {
                            min: 8,
                            max: 30,
                            message: 'Mínimo 8 caracteres.'
                        }
                    }
                },
                confirmPassword: {
                    validators: {
                        identical: {
                            field: 'password',
                            message: 'Las contraseñas no coinciden'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#session-loading").removeClass("hide").addClass("in");
            $("#btn-submit").attr("disabled","disabled").val("Creando usuario");
            $("#container-messages").removeClass("hide").addClass("in");
            $.ajax({
                type : "POST",
                url : "/session/save",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#email-validate2").text(response.email);
                        $("#email-validate2").attr(response.email);
                        $("#registercomplete2").modal("show");
                        $('#registercomplete2').on('hidden.bs.modal', function (e) {
                            window.location = response.url
                        });

                    }else if(response.code==300){
                        $("#password-incorrect").removeClass("hide").addClass("in");
                    }else if(response.code==400){
                        $("#email-incorrect").removeClass("hide").addClass("in");
                    }
                    else if(response.code==404){
                        $("#all-incorrect").removeClass("hide").addClass("in");
                    }
                    $("#session-loading").removeClass("in").addClass("hide");
                    $("#btn-submit").removeAttr("disabled","disabled").val("Crear usuario");
                },
                error : function(){

                }
            });
        });
    }
    /* New Register */


    /* changePassword */
    var changePassword = $("#changePassword");
    if(changePassword.length>=1){
        changePassword.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                email: {
                    validators: {
                        validators: {notEmpty: {}},
                        emailAddress: {
                            message: 'Correo electrónico válido.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico no existe, pruebe con otra',
                            url: '/session/validate-email-password',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#session-loading").removeClass("hide").addClass("in");
            $("#btn-submit").attr("disabled","disabled").val("Enviando mensaje");
            $("#container-messages").removeClass("hide").addClass("in");
            $.ajax({
                type : "POST",
                url : "/session/mail",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#message-correct").removeClass("hide").addClass("in");
                        $("#email").val("");
                        jQuery("#sendchangepass").modal("show");
                    }else if(response.code==300){
                        $("#password-incorrect").removeClass("hide").addClass("in");
                    }else if(response.code==400){
                        $("#email-incorrect").removeClass("hide").addClass("in");
                    }else if(response.code==404){
                        $("#all-incorrect").removeClass("hide").addClass("in");
                    }
                    $("#session-loading").removeClass("in").addClass("hide");
                    $("#btn-submit").removeAttr("disabled","disabled").val("Solicitar");
                },
                error : function(){
                }
            });
        });
    }
    /* changePassword */
    /* updatePassword */
    var updatePassword = $("#updatePassword");
    if(updatePassword.length>=1){
        updatePassword.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                password: {
                    validators: {
                        notEmpty: {
                            message: 'La contraseña es necesaria'
                        },
                        stringLength: {
                            min: 8,
                            max: 30,
                            message: 'Mínimo 8 caracteres.'
                        }
                    }
                },
                confirmPassword: {
                    validators: {
                        identical: {
                            field: 'password',
                            message: 'Las contraseñas no coinciden'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#session-loading").removeClass("hide").addClass("in");
            $("#btn-submit").attr("disabled","disabled").val("Enviando mensaje");
            $("#container-messages").removeClass("hide").addClass("in");
            $.ajax({
                type : "POST",
                url : "/session/new-password",
                data :$(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        jQuery("#changepass").modal("show");
                        /*setTimeout(window.location.href= response.url,20000);*/
                    }else if(response.code==300){
                        $("#password-incorrect").removeClass("hide").addClass("in");
                    }else if(response.code==400){
                        $("#email-incorrect").removeClass("hide").addClass("in");
                    }else if(response.code==404){
                        $("#all-incorrect").removeClass("hide").addClass("in");
                    }
                    $("#session-loading").removeClass("in").addClass("hide");
                    $("#btn-submit").removeAttr("disabled","disabled").val("Solicitar");
                },
                error : function(){
                }
            });
        });
    }
    /* updatePassword */


    $(document).on('click','#login_2',function(e){
        e.preventDefault();
        //facebookLogin();


        /*var session = $("#session");

         var message_modal = $("#message_modal");
         $.ajax({
         type : "POST",
         url : "/session/sessionfacebook",
         data : session.serialize(),
         dataType : "JSON",
         success : function(response){
         $("#myModalSession").modal("show");
         if(response.message=="SUCCESS" && response.code==200){
         message_modal.html("SesiÃ³n correcta espere un momento ....");
         window.location = response.url;
         }
         else if(response.code==300){
         message_modal.html("ContraseÃ±a incorrecta");
         $("#psin").text(response.message);
         $("#password-incorrect").removeClass("hide").addClass("in");
         }else if(response.code==400){
         message_modal.html("Email no validado");
         $("#email-incorrect").removeClass("hide").addClass("in");
         }
         else if(response.code==404){
         message_modal.html("Email no encontrado");
         $("#all-incorrect").removeClass("hide").addClass("in");
         }
         $("#session-loading").removeClass("in").addClass("hide");
         $("#btn-submit").removeAttr("disabled","disabled").val("Iniciar");
         },
         error : function(){

         },
         complete : function(){
         setTimeout(function(){
         $("#password-incorrect").removeClass("in").addClass("hide");
         $("#email-incorrect").removeClass("in").addClass("hide");
         $("#all-incorrect").removeClass("in").addClass("hide");
         $("#container-messages").removeClass("in").addClass("hide");
         },3000);
         }
         });*/



    });

});
$(function() {

    var app_id      ='294572587695577';
    var scopes      ='email, user_friends, public_profile';



    /*
    var btn_login   ='<a href="#" id="login" class="btn btn-primary">Iniciar sesiÃ³n</a>';

    var div_session ="<div id='facebook-session'>" +
        "<strong class='namefacebook'></strong>" +
        "<img class='photofacebook'>" +
        "<a href='#' id='logout' class='btn btn-danger'>Cerrar sesiÃ³n</a>"+
        "</div>";
    */

    var typeRegister = "";
    $('.login2').on("click",function(){
        typeRegister=$(this).attr("data-login");
    });
    window.fbAsyncInit = function() {

        FB.init({
            appId      : app_id,
            status     : true,
            cookie     : true,
            xfbml      : true,
            version    : 'v2.1'
        });


        FB.getLoginStatus(function(response) {
            statusChangeCallback(response, function() {});
        });
    };

    var statusChangeCallback = function(response, callback) {
        /*console.log(response.email);*/

        if (response.status === 'connected') {
            getFacebookData();
        } else {
            callback(false);
        }
    }

    var checkLoginState = function(callback) {
        FB.getLoginStatus(function(response) {
            callback(response);
        });
    }

    var getFacebookData =  function() {
        FB.api('/me?fields=email,name,first_name,last_name', function(response) {
            //$('#login').after(div_session);
            //console.log(response);
           // $('#loginfacebookperfil').after(div_session);
            //$('#login').remove();
            //$('#facebook-session strong').text(response.name);
            //$('#facebook-session img').attr('src','http://graph.facebook.com/'+response.id+'/picture?type=large');
            sessionServer(response,typeRegister);
        });
    }

    var facebookLogin = function() {
        checkLoginState(function(data) {
            if (data.status !== 'connected') {
                FB.login(function(response) {
                    if (response.status === 'connected')
                        getFacebookData();
                }, {scope: scopes});
            }
        })
    }

    var facebookLogout = function() {
        checkLoginState(function(data) {
            if (data.status === 'connected') {
                FB.logout(function(response) {
                //    $('#facebook-session').before(btn_login);
                  //  $('#facebook-session').remove();
                })
            }
        })

    }
    $(document).on('click', '#login', function(e) {
        e.preventDefault();
        facebookLogin();
    })

    $(document).on('click', '#logout', function(e) {
        e.preventDefault();

        if (confirm("Â¿EstÃ¡ seguro?"))
            facebookLogout();
        else
            return false;
    })

});

function sessionServer(fid,type){

    $.ajax({
        type : "POST",
        url : "/session/sessionfacebook",
        data : {"fid":fid.id,"name":fid.name,"email":fid.email,"login":type},
        dataType : "JSON",
        success : function(response){
            console.log(fid);
            FB.logout(function(response) {
                // user is now logged out
            });
            if(response.code==200){
                window.location.href="/";
            }else{
                if(type=="loguear"){
                    alert("Se requiere registrar el correo primero");
                }else{
                    alert("el correo ya esta registrado, inicie sesión de manera normal");
                }
            }
        },
        error : function(){

        },
        complete : function(){

        }
    });
}

$(document).ready(function(){
    /* Personal Information */
    
    
    if($('#ayudaForm').length>=1){
        jQuery.validator.addMethod("noSpace", function(value, element) { 
            return value == '' || value.trim().length != 0; 
        }, "No space please and don't leave it empty");
        var form = $('#ayudaForm');
        form.validate({
            rules:{
                delegacion:{required:true},
                sr:{required:true,noSpace:true},
                desea:{required:true},
                como_asociado:{required:true},
                recomendado:{required:true},
                tel:{number: true,required:true},
                "n_ben1[]":{required:true},
                "d_ben1[]":{required:true},
                "t_ben1[]":{required:true},
                "c_ben1[]":{required:true}
            },
            messages:{
                delegacion:"Rellenar campo",
                sr:"Rellenar campo",
                desea:"Seleccionar",
                como_asociado:"Seleccionar",
                recomendado:"Rellenar campo",
                tel:{
                    number:"Solo numeros",
                    required:"Rellenar campo"
                },
                "n_ben1[]":"Rellenar campos",
                "d_ben1[]":"Rellenar campos",
                "t_ben1[]":"Rellenar campos",
                "c_ben1[]":"Rellenar campos"
            }   
        });
        form.steps({
            enableCancelButton: true,
            headerTag: "h3",
		     bodyTag: "section",
		     transitionEffect: "slideLeft",
             autoFocus: true,
             onStepChanging: function (event, currentIndex, newIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden";
                if(form.valid()!=true){
                    form.valid();
                    return false;
                }else{
                   swal("Usted deberá subir el formato escaneado a la sección de Ayuda"+
                    " Mutua en Mi perfil, además de entregar el original y tres copias a color al Coordinador "+
                    "de Ayuda Mutua de su Delegación");
                    return form.valid();
                }
            },
            onFinishing: function (event, currentIndex)
            {   
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onCanceled:function(event){
                swal({
                    title: '¿Estas seguro?',
                    text: "Si ha guardado algun dato de la solicitud, sera borrado ",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, Cancelalo',
                    cancelButtonText:"No"
                  }).then((result) => {
                    if (result.value) {
                      $.ajax({
                        url:"/ayudamutua/cancel",
                        type:"post",
                        dataType:"json",
                        success:function(resp){
                            swal("Su solicitud ha sido cancelada").then(
                                (result)=>{
                                    if(result.value){
                                        window.location.replace("/perfil");
                                    }
                                }
                             );
                        }
                     });
                    }
                  });
            },
            onFinished: function (event, currentIndex)
            {
            
                $.ajax({
                    url:"/ayudamutua/saveayuda",
                    type:"post",
                    data:form.serialize(),
                    dataType:"json",
                    success:function(){
                         swal("Datos guardados correctamente");
                    }
                });
            }
        });
        window.onload= function(){
            $('.actions ul li a').eq(0).text("Anterior");
            $('.actions ul li a').eq(1).text("Siguiente");
            $('.actions ul li a').eq(2).text("Acabar");
            $(".actions ul").append(
                    "<li><a id='bGuardarSalir' style='cursor:pointer' >Guardar y salir</a></li>"
                    );
            $('#bGuardarSalir').on("click",function(){
                $.ajax({
                    url:"/ayudamutua/saveayuda",
                    type:"post",
                    data:form.serialize(),
                    dataType:"json",
                    success:function(){
                         swal("Se han guardado los datos correctamente").then(
                                 (result)=>{
                                     if(result.value){
                                         window.location.replace("/perfil");
                                    }
                                }
                        );
                    }
                });
            });
            
           // alert(sumaPorcentaje("por_ben1[]"));
            sumaPorcentaje('por_ben1[]');
            sumaPorcentaje('por_ben2[]');
            
            $('#bAgregarBeneficiarios1').on("click",function(){
                $.ajax({
                    url:"/ayudamutua/beneficiariosoperations",
                    type:"post",
                    data:{type:"beneficiarios1",clid:$('#clid').val()},
                    dataType:"json",
                    success:function(resp){
                        if(resp.code=="200"){
                            $('#table_1 tbody').append(
                            "<tr id='beneficiario_"+resp.id+"' data-benid='"+resp.id+"'>"+
                               `<td>
                                   <input type="hidden" name="benid[]" value="`+resp.id+`" />
                                   <div class='col-md-3'>
                                        <label>N:</label><input type="text" name="n_ben1[]" class="form-control"/>
                                   </div>
                                   <div class='col-md-3'>
                                       <label>D:</label><input type="text" name="d_ben1[]" class="form-control"/>
                                   </div class='col-md-3'>
                                   <div class='col-md-3'>
                                       <label>T:</label><input type="text" name="t_ben1[]" class="form-control"/>
                                   </div>
                                   <div class='col-md-3'>
                                       <label>C:</label><input type="text" name="c_ben1[]" class="form-control"/>
                                   </div>
                               </td>
                               <td><input type="text" name="parentesco_ben1[]"></td>
                               <td><input type="number" name="por_ben1[]" value="0"></td>
                               <td>
                                   <button type="button" class="btn btn-danger" onclick =eliminarBeneficiario(`+resp.id+`)>Eliminar</button>
                               </td>`+ 
                            "<tr>"
                            );
                            sumaPorcentaje('por_ben1[]');
                        }else if(resp.code=="302"){
                            swal("No e pueden agregar más de 4 beneficiarios");
                        }
                    }
                });
            });
            $('#bAgregarBeneficiarios2').on("click",function(){
                $.ajax({
                    url:"/ayudamutua/beneficiariosoperations",
                    type:"post",
                    data:{type:"beneficiarios2",clid:$('#clid').val()},
                    dataType:"json",
                    success:function(resp){
                        if(resp.code=="200"){
                            $('#table_2 tbody').append(
                            "<tr id='beneficiario_"+resp.id+"' data-benid='"+resp.id+"'>"+
                               `<td>
                                    <input type="hidden" name="benid2[]" value="`+resp.id+`" />
                                   <div class='col-md-3'>
                                        <label>N:</label><input type="text" name="n_ben2[]" class="form-control"/>
                                   </div>
                                   <div class='col-md-3'>
                                       <label>D:</label><input type="text" name="d_ben2[]" class="form-control"/>
                                   </div class='col-md-3'>
                                   <div class='col-md-3'>
                                       <label>T:</label><input type="text" name="t_ben2[]" class="form-control"/>
                                   </div>
                                   <div class='col-md-3'>
                                       <label>C:</label><input type="text" name="c_ben2[]" class="form-control"/>
                                   </div>
                               </td>
                               <td><input type="text" name="parentesco_ben2[]"></td>
                               <td><input type="number" name="por_ben2[]" value="0"></td>
                               <td>
                                   <button type="button" class="btn btn-danger" onclick =eliminarBeneficiario(`+resp.id+`)>Eliminar</button>
                               </td>`+ 
                            "<tr>"
                            );
                        sumaPorcentaje('por_ben2[]');
                        }else if(resp.code=="302"){
                            swal("No e pueden agregar más de 3 beneficiarios");
                        }
                        
                    }
                });
                
            });
            $('.actions').find('ul').eq(2).remove();
            $('.actions').find('ul').eq(1).remove();
            $('input[name=lugar_nacimiento]').eq(0).on("keyup",function(){
                $('input[name=lugar_nacimiento]').eq(1).val($(this).val());
                
            });
            $('input[name=fecha_nacimiento]').eq(0).on("change",function(){
                console.log($(this).val());
                $('input[name=fecha_nacimiento]').eq(1).val($(this).val());
                
            })
            var valor = $('input[name="desea"]').length;
            for(var i=0;i<valor-1;i++){
                $('input[name="desea"]').eq(i).on("click",function(){
                    $('#fechaSalida input[name="fecha_salida"]').remove();
                });
            }
            $('#reingresar').on("click",function(){
                if($('#reingresar').is(":checked")){
                    $('#fechaSalida').append("<input class='form-control' name='fecha_salida' type='date' />");
                }
            });
        }
        
    }
    if($("#newWorkProfile").length>=1){

        $('#newWorkProfile').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Nombre es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 100,
                            message: 'El Nombre debe ser menor de 10 y mayor de 100 caracteres de longitud.'
                        }
                    }
                },
                permalink: {
                    validators: {
                        notEmpty: {
                            message: 'El Permalink o Url es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 100,
                            message: 'El Permalink o Url  debe ser menor de 10 y mayor de 100 caracteres de longitud.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $.ajax({
                type : "POST",
                url : "/work/save",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){

                        setTimeout(function(){
                            window.location = "/perfil"
                        },3000);
                    }else if(response.code==404){
                    }
                },
                error : function(){
                }
            });
        });
        if($("#filePublicacionesPerfil").length>=1){
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone('#filePublicacionesPerfil', {
                url: "/work/uploadfile",
                uploadMultiple : false,
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 15, // MB
                maxFiles: 2 ,
                parallelUploads : 1,
                addRemoveLinks : true,
                dictResponseError: "No se puede subir esta archivo!",
                autoProcessQueue: true,
                thumbnailWidth: 138,
                thumbnailHeight: 120,
                previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

                resize: function(file) {
                    var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                        srcRatio = file.width / file.height;
                    if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                        info.trgHeight = this.options.thumbnailHeight;
                        info.trgWidth = info.trgHeight * srcRatio;
                        if (info.trgWidth > this.options.thumbnailWidth) {
                            info.trgWidth = this.options.thumbnailWidth;
                            info.trgHeight = info.trgWidth / srcRatio;
                        }
                    } else {
                        info.trgHeight = file.height;
                        info.trgWidth = file.width;
                    }
                    return info;
                },
                sending : function(file, xhr, formData){
                },
                success: function(file, response){
                    var name_image = response;
                    $(".dz-preview").addClass("dz-success");
                    $("div.progress").remove();
                    $("#input-image-work-profile").val(name_image.name);
                },
                removedfile: function(file) {
                    $(file.previewElement).remove();
                }
            });
        }
        /*if($("#filePublicacionesPerfil").length>=1){
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone('#filePublicacionesPerfil', {
                url: "/work/uploadfile",
                uploadMultiple : false,
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 15, // MB
                maxFiles: 1 ,
                parallelUploads : 1,
                addRemoveLinks : true,
                dictResponseError: "No se puede subir esta archivo!",
                autoProcessQueue: true,
                previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

                sending : function(file, xhr, formData){
                },
                success: function(file, response){
                    var name_image = response;
                    $(".dz-preview").addClass("dz-success");
                    $("div.progress").remove();
                    $("#input-image-principal-work-profile").val(name_image.name);
                },
                removedfile: function(file) {
                    $(file.previewElement).remove();
                }
            });
        }*/
    }
    if($("#updateMembership").length>=1){
        $('#updateMembership').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                cuotid: {
                    validators: {
                        notEmpty: {
                            message: 'El tipo de asociado no puede estar vacio.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/user/updatemembership",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-success").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            window.location = "/perfil"
                        },3000);
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                    }
                },
                error : function(){
                    $("#message-box-info").removeClass("open");
                }
            });
        });

    }




    /* Update delegation */
    if($("#updateDelegation").length>=1){
        $('#updateDelegation').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                comid: {
                    validators: {
                        notEmpty: {
                            message: 'La delegación no puede estar vacio.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/user/updatedelegation",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-success").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            window.location = "/perfil"
                        },3000);
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                    }
                },
                error : function(){
                    $("#message-box-info").removeClass("open");
                }
            });
        });

    }



    /* Update Note */
    if($("#updateNote").length>=1){
        function validateEditor() {
            // Revalidate the content when its value is changed by Summernote
            $('#summernoteForm').formValidation('revalidateField', 'content2');
        }
        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        })
            .on('changeDate', function(e) {
                // Revalidate the date field
                $('#newNote').formValidation('revalidateField', 'dateP');
            });
        $('#updateNote').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El Nombre es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 100,
                            message: 'El Nombre debe ser mayor de 10 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                permalink: {
                    validators: {
                        notEmpty: {
                            message: 'El Permalink o Url es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 10,
                            max: 100,
                            message: 'El Permalink o Url  debe ser mayor de 10 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                status: {
                    validators: {
                        notEmpty: {
                            message: 'El status no puede estar vacio.'
                        }
                    }
                },
                catpuid: {
                    validators: {
                        notEmpty: {
                            message: 'La categoria no puede estar vacio.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
                /*contenido:$('.summernote').code()*/
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/work/update",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-success").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            window.location = "/work/index"
                        },3000);
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                    }
                },
                error : function(){
                    $("#message-box-info").removeClass("open");
                }
            });
        });

    }
    /* Validate title */
    if($("#note-title").length>=1){
        $("#note-title").change(function(){
            var $input_loader = $("#input-loader");
            $input_loader.removeClass("fade");
            var title_note = $(this).val();
            $.ajax({
                url : "/work/validateurl",
                type : "POST",
                data : {title:title_note,key: $('#key-security').attr('data-key'),value: $('#value-security').attr('data-value')},
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#note-permalink").val(response.new_url);
                        $('#newNote').formValidation('revalidateField', 'permalink');
                    }else{
                        $input_loader.addClass("hide");
                        alert("Ha ocurrido un error intente nuevamente.");
                    }
                },
                complete: function(){
                    $input_loader.addClass("hide");
                }
            });
        });
    }
    if($("#dropzone-user-edit").length>=1){
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#dropzone-user-edit', {
            url: "uploadimage",
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 2,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            success: function(file, response){
                var name_image = response;
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#user-image").val(name_image.name);
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
            }
        });
        var mockFile = { name: "Click en remove file", size: 12345};
        myDropzone.emit("addedfile", mockFile);
        $(".progress.progress-striped.active").addClass("hide");
        if($("#user-image").val()==""){
            image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
        }else{
            image_load = "/dash/assets/images/users/thumbnail/"+$("#user-image").val();
        }
        myDropzone.emit("thumbnail", mockFile,image_load);
        var existingFileCount = 1; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
    }
    if($("#image-principal-dz").length>=1){
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#image-principal-dz', {
            url: "/post/uploadimagenote",
            uploadMultiple : false,
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 1 ,
            parallelUploads : 1,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            sending : function(file, xhr, formData){
                formData.append("position_image",2);
                formData.append("image_post",1);
                formData.append("post_id",$("#post_id").val());
                formData.append("uniqid-id-image",$("#uniqid-id-image").val());
            },
            success: function(file, response){
                var name_image = response;
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-image-principal").val(name_image.name);
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
            }
        });
    }
    if($("#reciboPago").length>=1){
        if($("#input-recibo-pago").val()==''){
            $("#btn-submit1111").prop("disabled",true);
        }
        else{
            $("#btn-submit1111").prop("disabled",false);
        }
        $("#fileReciboPago").on("change",function(){
            if($("#input-recibo-pago").val()==''){
                $("#btn-submit1111").prop("disabled",true);
            }
            else{
                $("#btn-submit1111").prop("disabled",false);
            }
        });
        $('#reciboPago').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                cuotid: {
                    validators: {
                        notEmpty: {
                            message: 'El tipo de asociado no puede estar vacio.'
                        }
                    }
                },
                reciboPago: {
                    validators: {
                        notEmpty: {
                            message: 'El archivo no puede estar vacio.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            $("#btn-submit").attr("disabled","disabled").val("Iniciando");
            $("#message-box-info").toggleClass("open");
            var values = {
                key: $('#key-security').attr('data-key'),
                value: $('#value-security').attr('data-value')
            };
            var data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/user/updaterecibocuota",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-success").toggleClass("open");
                        setTimeout(function(){
                            $("#message-box-success").removeClass("open");
                            window.location = "/perfil"
                        },3000);
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                    }
                },
                error : function(){
                    $("#message-box-info").removeClass("open");
                }
            });
        });

    }
    if($("#fileReciboPago").length>=1){
            Dropzone.autoDiscover = false;
            var workid = $(this).attr("data-workid");
            var myDropzone = new Dropzone('#fileReciboPago',  {
                url: "/user/uploadfile",
                uploadMultiple : false,
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 15, // MB
                maxFiles: 2 ,
                parallelUploads : 1,
                addRemoveLinks : true,
                dictResponseError: "No se puede subir esta archivo!",
                autoProcessQueue: true,
                thumbnailWidth: 138,
                thumbnailHeight: 120,
                previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

                resize: function(file) {
                    var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                        srcRatio = file.width / file.height;
                    if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                        info.trgHeight = this.options.thumbnailHeight;
                        info.trgWidth = info.trgHeight * srcRatio;
                        if (info.trgWidth > this.options.thumbnailWidth) {
                            info.trgWidth = this.options.thumbnailWidth;
                            info.trgHeight = info.trgWidth / srcRatio;
                        }
                    } else {
                        info.trgHeight = file.height;
                        info.trgWidth = file.width;
                    }
                    return info;
                },
                sending : function(file, xhr, formData){
                    formData.append("file",file);
                },
                success: function(file, response){
                    var name_image = response;
                    $(".dz-preview").addClass("dz-success");
                    $("div.progress").remove();
                    $("#input-recibo-pago").val(name_image.name);
                    if($("#input-recibo-pago").val()==''){
                        $("#btn-submit1111").prop("disabled",true);
                    }
                    else{
                        $("#btn-submit1111").prop("disabled",false);
                    }


                },
                removedfile: function(file) {
                    var name = file.previewElement.accessKey;
                     $.ajax({
                        url: "/user/deleterecibo",
                        type: "post",
                        data: { "clid":workid},
                        dataType:"json",
                        success : function(response){
                            if(response.message=="SUCCESS" && response.code==200){
                                $(file.previewElement).remove();
                                $("#input-recibo-pago").val("");
                                form.formValidation('revalidateField', 'input-recibo-pago');
                            }else{
                                alert("No se ha podido eliminar");
                            }
                        },
                        error : function(){
                            error();
                        }
                     });
                }
            });

            if ($("#input-recibo-pago").val() == "" || $("#input-recibo-pago").val() == "icon.png") {
            } else {
                var mockFile = { name: "Click en remove file", size: 12345 };
                myDropzone.emit("addedfile", mockFile);
                $(".progress.progress-striped.active").addClass("hide");
                image_load = "/front/src/images/registro/document.png";
                myDropzone.emit("thumbnail", mockFile, image_load);
                var existingFileCount = 1; // The number of files already uploaded
                myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
            }

            /*var mockFile = { name: "Click en remove file", size: 12345};
            myDropzone.emit("addedfile", mockFile);
            $(".progress.progress-striped.active").addClass("hide");
            if($("#input-recibo-pago").val()==""){
                image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
            }else{
                image_load = "/api/administration/registro/recibo/"+$("#input-recibo-pago").val();
            }
            myDropzone.emit("thumbnail", mockFile,image_load);
            var existingFileCount = 1; // The number of files already uploaded
            myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;*/
    }
    if($("#dropzone-note-edit").length>=1){
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#dropzone-note-edit', {
            url: "/work/uploadimagenote",
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 2,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fas fa-times-circle"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            success: function(file, response){
                var name_image = response;
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#input-image-principal").val(name_image.name);
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
            }
        });
        var mockFile = { name: "Click en remove file", size: 12345};
        myDropzone.emit("addedfile", mockFile);
        $(".progress.progress-striped.active").addClass("hide");
        if($("#input-image-principal").val()==""){
            image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
        }else{
            image_load = "/api/images/eventos/100x80/"+$("#input-image-principal").val();
        }
        myDropzone.emit("thumbnail", mockFile,image_load);
        var existingFileCount = 1; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
    }
});
function validateCkEditor(name_selector,form_validator){
    $("#"+name_selector).ckeditor().editor.on('change', function(e) {
        form_validator.formValidation('revalidateField', name_selector);
    });
}
function eliminarBeneficiario($id){
    $.ajax({
        url:"/ayudamutua/beneficiariosoperations",
        type:"post",
        data:{removeben:$id},
        dataType:"json",
        success:function(resp){
            $('#beneficiario_'+$id).remove();
        }
    })
}

function sumaPorcentaje($name){
    var input= $('input[name="'+$name+'"]');
    input.off("keyup");
    input.each(function(i){
        $(this).on("keyup",function(){
            let  suma =0;
            for(var i =0; i<input.length;i++){
                  suma+=  parseInt(input.eq(i).val());
            }
            if(suma>100){
               swal("La suma de los porcentajes no puede ser mayor que 100"); 
               $(this).val("0");
            }
        });
   });
}
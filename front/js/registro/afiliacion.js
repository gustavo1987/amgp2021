$(document).ready(function () {
    
    $( ".finishobservation" ).click(function() {
        var tc = $(this).closest("form");
        var stid = $(this).closest("form").find("input[name='stid']").val();
        button(1);
        $.ajax({
            type: "POST",
            url: "/observar",
            data: {stid: stid},
            dataType: "json",
            success: function (response) {
                /*if (response.code == 200) {
                     alert("hecho");
                } else {
                    alert("No se ha podido generar la url de pago111");
                }*/

                switch (response.code){
                    case "200": modal(1);button(2);
                        break;
                    case "300": button(2);modal(2);
                        break;
                    default :button(2);modal(1);
                        break;
                }
            },
            error: function () {
                button(2);
                modal(3);

            }, complete: function () {
                setTimeout(function () {
                    $("#form-success").addClass("hidden");
                    $("#form-warning").addClass("hidden");
                }, 2000);
            }
        });
      return false
    });

    var formSendDocument = $("#senddocumentincomplete");
    if (formSendDocument.length >= 1) {
        formSendDocument.on('success.form.fv', function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "/registro/senddocumentincomplete",
                data: $(this).serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.code == 200) {
                        window.location.href = response.url;
                    } else {
                        alert("No se ha podido generar la url de pago")
                    }
                },
                error: function () {

                }, complete: function () {
                    setTimeout(function () {
                        $("#form-success").addClass("hidden");
                        $("#form-warning").addClass("hidden");
                    }, 2000);
                }
            });
        });
    };

    $("#close-modal-incomplete-document").on("click", function(){
        $(".list-modal-incomplete").remove();
    });
    $("#deleteInscription").on("click", function () {
        clid = $("#stidd").val();

        $.ajax({
            type: "POST",
            url: "/registro/cancelall",
            data: { clid: clid },
            dataType: "json",
            success: function (response) {
                if (response.message == "SUCCESS" && response.code == 200) {
                    messages(1);
                } else {
                    messages(2);
                }
                backgroundActive(2);
                button(2);
            },
            error: function () {
                backgroundActive(2); button(2);
            }
        });

        alert("Hello! I am an alert box!!" + clid);
    });

    /* Inicio la vista de solo pago */

    var newClient2 = $("#newClient2");
    if (newClient2.length >= 1) {
        $("#others").on("change", function(){
            if($("input[name='pay']:checked").val()=="TRANSFER"){
                $('.actions > ul > li > a[href$="#finish"]').text("Datos de pago");
            }
            else{
                $('.actions > ul > li > a[href$="#finish"]').text("Pagar");
            }
        });
        var stid = $("#stid").val();
        uid = $("#uid").val();
        var cuotid = $("#cuotid").val();
        $('input:text,input:password, input:file, input[type=email], select, textarea').css("text-transform", "uppercase");

        setTimeout(function () {
            validateDate(newClient2);
        }, 500);
        setTimeout(function () {
            $('.actions > ul > li > a[href$="#next"]').attr('style', 'left: 83.5% !important; position: absolute; top: 0%;background-color: #51a457 !important;');
            $('.actions > ul > li > a[href$="#previous"]').addClass("anterior");
            $('.actions > ul > li > a[href$="#finish"]').addClass("finish");
        }, 500);
        newClient2
            .steps({
                headerTag: 'h2',
                bodyTag: 'section',
                labels: {
                    current: "paso actual:",
                    pagination: "Paginación",
                    finish: "Pagar",
                    next: "Guardar y continuar",
                    previous: "Ir a formulario",
                    loading: "Cargando ..."
                },
                onStepChanged: function (e, currentIndex, priorIndex) {
                    /* You don't need to care about it
                       It is for the specific demo*/
                    /*adjustIframeHeight();*/

                },
                /* Triggered when clicking the Previous/Next buttons*/
                onStepChanging: function (e, currentIndex, newIndex) {
                    $('.actions > ul > li > a[href$="#previous"]').addClass("anterior");
                    $('.actions > ul > li > a[href$="#next"]').addClass("saveandcontinue");
                    $('.actions > ul > li > a[href$="#finish"]').addClass("finish");
                    /*console.log({e:e,c:currentIndex,n:newIndex});*/
                    var fv = newClient2.data('formValidation'), // FormValidation instance
                        /* The current step container*/
                        $container = newClient2.find('section[data-step="' + currentIndex + '"]');

                    if (newIndex == 0) {
                        $('.actions > ul > li > a[href$="#next"]').attr('style', 'left: 83.5% !important; position: absolute; background-color: #51a457 !important;');

                        return true;
                    } else {
                        $('.actions > ul > li > a[href$="#next"]').attr('style', 'left: 83.5% !important; position: absolute; background-color: #51a457 !important;');
                    }

                    // Validate the container
                    fv.validateContainer($container);
                    $('.actions > ul > li > a[href$="#previous"]').addClass("anterior");
                    $('.actions > ul > li > a[href$="#next"]').addClass("saveandcontinue");

                    var isValidStep = fv.isValidContainer($container);
                    if (isValidStep === false || isValidStep === null) {
                        // Do not jump to the next step
                        return false;
                    }
                    return true;
                },
                // Triggered when clicking the Finish button
                onFinishing: function (e, currentIndex) {

                    var fv = newClient2.data('formValidation'),
                        $container = newClient2.find('section[data-step="' + currentIndex + '"]');

                    // Validate the last step container
                    fv.validateContainer($container);

                    var isValidStep = fv.isValidContainer($container);
                    if (isValidStep === false || isValidStep === null) {
                        return false;
                    }
                    return true;
                },
                onFinished: function (e, currentIndex) {
                    e.preventDefault();
                    //window.location.href = "/pagarinscripcion/" + cuotid;

                    //$("#message-box-info").toggleClass("open");
                    $.ajax({
                        type: "POST",
                        url: "/registro/savepayafiliacion",
                        data: $(this).serialize(),
                        dataType: "json",
                        success: function (response) {
                            if (response.code == 200) {
                                messages(1);
                                /*$("#userContinue").toggleClass("open");*/
                                $("a#url-si").attr("href",response.url);
                                $(".metodo-de-pago-elegido").text(response.metodo);
                                $("#userContinue").modal("show");
                                /*window.location.href = response.url;*/
                            } else if (response.code == 404) {
                                messages(2)
                            }
                        },
                        error: function () {
                            messages(3);
                        }
                    });
                }
            }).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                excluded: ':disabled',
                locale: 'es_ES',
                fields: {
                    phone: {
                        validators: {
                            callback: {
                                message: 'El teléfono es necesario y no puede estar vacio.',
                                callback: function (value, validator, $field) {
                                    var channel = newClient2.find('[name="input_required"]:checked').val();
                                    return (channel !== 'phone') ? true : (value !== '');
                                }
                            }
                        }
                    }
                }
            }).on('err.field.fv', function (e, data) {
                if (data.fv.getSubmitButton()) {
                    data.fv.disableSubmitButtons(false);
                }
            }).on('success.field.fv', function (e, data) {
                if (data.fv.getSubmitButton()) {
                    data.fv.disableSubmitButtons(false);
                }
            });
        $(".tutor").on("change", function (e) {
            var val = $(this).val();
            if (val == "SI") {
                $(".add_data_client[name='calle']").val($("#calle").val()).change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='numerointe']").val($("#numerointe").val()).change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='numeroexte']").val($("#numeroexte").val()).change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='colonia']").val($("#colonia").val()).change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='cp']").val($("#cp").val()).change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='municipio']").val($("#municipio").val()).change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='localidad']").val($("#localidad").val()).change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='estado']").val($("#estado").val()).change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='pais']").val($("#pais").val()).change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
            } else {
                $(".add_data_client[name='calle']").val("").change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='numerointe']").val("").change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='numeroexte']").val("").change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='colonia']").val("").change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='cp']").val("").change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='municipio']").val("").change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='localidad']").val("").change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='estado']").val("").change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='pais']").val("").change(function () {
                    newClient2.formValidation('revalidateField', $(this));
                }).trigger("change");
            }
        });
        $("#addOtherUser").click(function (e) {
            e.preventDefault();
            newClient2.data('formValidation').resetForm(true);
            resetForm(newClient2);
            $("#uid").val(uid);
            $('.radio_email').prop('checked', true);
            newClient2.formValidation('revalidateField', 'email');
            $('.radio_work').prop('checked', true);
            $('#scid.selectUP').select2('val', "");
            $('#crid.selectUP').select2('val', "");
            $('#hour_interest.selectUP').select2('val', "");
            if ($("#validateUid").length >= 1) {
                $('#uid.selectUP').select2('val', "");
            }
        });
        saveKeyupValue("keyup_Save", newClient2, stid);
        saveChangeValue("change_Save", newClient2, stid);
        saveChange2Value("change_Save2", newClient2, stid);
    }


    /* Finalizar la vista de solo pago */






    /* Delete value table */

    var delete_value_table = $(".delete_direction");
    if (delete_value_table.length >= 1) {
        var id = {};
        $(document.body).on("click", ".delete_direction", function () {
            id = $(this).attr('data-id');
            $("#delete_modal").modal("show");
        });
        $("#delete_modal").on('shown.bs.modal', function (e) {
            $(".value_name_delete").text("Registro ha eliminar");
        });
        $("#delete_modal").on('hidden.bs.modal', function (e) {
            id = {};
            $(".value_name_delete").text(null);
        });
        $("#confirm_delete").click(function () {
            button(1);
            $.ajax({
                type: "POST",
                url: "/registro/deletedirection",
                data: { value: id },
                dataType: "json",
                success: function (response) {
                    $("#delete_modal").modal("hide");
                    switch (response.code) {
                        case "200": messages(1); button(2); $("#" + id).remove();
                            break;
                        case "300": messages(2); button(2);
                            break;
                        default: messages(3); button(2);
                            break;
                    }

                },
                error: function () {
                    $("#delete_modal").modal("hide");
                    button(2);
                    messages(3);
                }
            });
        });
    }
    /* Delete value table */
    var delete_value_table_studio = $(".delete_studio");
    if (delete_value_table_studio.length >= 1) {
        var id = {};
        $(document.body).on("click", ".delete_studio", function () {
            id = $(this).attr('data-id');
            $("#delete_modal").modal("show");
        });
        $("#delete_modal").on('shown.bs.modal', function (e) {
            $(".value_name_delete").text("Registro ha eliminar");
        });
        $("#delete_modal").on('hidden.bs.modal', function (e) {
            id = {};
            $(".value_name_delete").text(null);
        });
        $("#confirm_delete").click(function () {
            button(1);
            $.ajax({
                type: "POST",
                url: "/registro/deletestudio",
                data: { value: id },
                dataType: "json",
                success: function (response) {
                    $("#delete_modal").modal("hide");
                    switch (response.code) {
                        case "200": messages(1); button(2); $("#" + id).remove(); $("#titulo"+ id).remove();$("#cedula"+ id).remove();$("#comprobante"+ id).remove();
                            break;
                        case "300": messages(2); button(2);
                            break;
                        default: messages(3); button(2);
                            break;
                    }
                },
                error: function () {
                    $("#delete_modal").modal("hide");
                    button(2);
                    messages(3);
                }
            });
        });
    }
    /* Delete value table */
    var delete_value_table_work = $(".delete_work");
    if (delete_value_table_work.length >= 1) {
        var id = {};
        $(document.body).on("click", ".delete_work", function () {
            id = $(this).attr('data-id');
            $("#delete_modal").modal("show");
        });
        $("#delete_modal").on('shown.bs.modal', function (e) {
            $(".value_name_delete").text("Registro ha eliminar");
        });
        $("#delete_modal").on('hidden.bs.modal', function (e) {
            id = {};
            $(".value_name_delete").text(null);
        });
        $("#confirm_delete").click(function () {
            button(1);
            $.ajax({
                type: "POST",
                url: "/registro/deletework",
                data: { value: id },
                dataType: "json",
                success: function (response) {
                    $("#delete_modal").modal("hide");
                    switch (response.code) {
                        case "200": messages(1); button(2); $("#" + id).remove();
                            break;
                        case "300": messages(2); button(2);
                            break;
                        default: messages(3); button(2);
                            break;
                    }

                },
                error: function () {
                    $("#delete_modal").modal("hide");
                    button(2);
                    messages(3);
                }
            });
        });
    }


    $("#modalReferencias").modal("show");
    var form = $("#referenciaForm");
    if (form.length >= 1) {
        form.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                /*referencia: {
                    validators: {
                        notEmpty: {
                            message: 'La referencia es necesaria.'
                        }
                    }
                },*/
                emailrefe: {
                    validators: {
                        emailAddress: {
                            message: 'El valor no es una dirección de correo electrónico válida'
                        },
                        notEmpty: {
                            message: 'El correo es necesaria.'
                        }
                    }
                },
                phonerefe: {
                    validators: {
                        notEmpty: {
                            message: 'El teléfono es necesaria.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function (e) {
            e.preventDefault();
            var send = $("#sendM");
            var message = send.attr("data-loading-text");
            send.text(message).attr("disabled", true);
            $.ajax({
                type: "POST",
                url: "/registro/savereferencia",
                data: $(this).serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.code == 200) {
                        send.text("Enviar mensaje").attr("disabled", false);
                        resetForm(form);
                        form.formValidation('resetForm', true);
                        $("#form-success").removeClass("hidden");
                        $('#modalReferencias').modal('toggle');
                    } else {
                        $("#form-warning").removeClass("hidden");
                        send.text("Intentar nuevamente").attr("disabled", false);
                    }
                },
                error: function () {

                }, complete: function () {
                    setTimeout(function () {
                        $("#form-success").addClass("hidden");
                        $("#form-warning").addClass("hidden");
                    }, 2000);
                }
            });
        });
    }
    var newClient = $("#newClient");
    if (newClient.length >= 1) {
        var stid = $("#stid").val();
        uid = $("#uid").val();
        var cuotid = $("#cuotid").val();
        $('input:text,input:password, input:file, input[type=email], select, textarea').css("text-transform", "uppercase");

        setTimeout(function () {
            validateDate(newClient);
        }, 500);
        setTimeout(function () {
            $('.actions > ul > li > a[href$="#previous"]').attr('style', 'display:none;');
            $('.actions > ul > li > a[href$="#next"]').attr('style', 'left: 83.5% !important; position: absolute; top: 0%; background-color: #51a457 !important;');
        }, 500);
        newClient
            .steps({
                headerTag: 'h2',
                bodyTag: 'section',
                labels: {
                    current: "paso actual:",
                    pagination: "Paginación",
                    finish: "Guardar y continuar",
                    next: "Guardar y continuar",
                    previous: "Anterior",
                    loading: "Cargando ..."
                },
                onStepChanged: function (e, currentIndex, priorIndex) {
                    // You don't need to care about it
                    // It is for the specific demo
                    //adjustIframeHeight();

                },
                // Triggered when clicking the Previous/Next buttons
                onStepChanging: function (e, currentIndex, newIndex) {
                    $('.actions > ul > li > a[href$="#previous"]').addClass("anterior");
                    $('.actions > ul > li > a[href$="#next"]').addClass("saveandcontinue");
                    $('.actions > ul > li > a[href$="#finish"]').addClass("finish");


                    /*console.log({e:e,c:currentIndex,n:newIndex});*/
                    var fv = newClient.data('formValidation'), // FormValidation instance
                        // The current step container
                        $container = newClient.find('section[data-step="' + currentIndex + '"]');
                    if (newIndex == 0) {
                        $('.actions > ul > li > a[href$="#next"]').attr('style', 'left: 83.5% !important; position: absolute; background-color: #51a457 !important;');
                        $('.actions > ul > li > a[href$="#previous"]').attr('style', 'display:none;');
                        return true;
                    } else {
                        $('.actions > ul > li > a[href$="#next"]').attr('style', 'left: 83.5% !important; position: absolute; background-color: #51a457 !important;');
                        $('.actions > ul > li > a[href$="#previous"]').attr('style', 'display:init;');
                    }
                    // Validate the container
                    fv.validateContainer($container);
                    $('.actions > ul > li > a[href$="#previous"]').addClass("anterior");
                    $('.actions > ul > li > a[href$="#next"]').addClass("saveandcontinue");

                    var isValidStep = fv.isValidContainer($container);
                    if (isValidStep === false || isValidStep === null) {
                        // Do not jump to the next step
                        return false;
                    }
                    return true;
                },
                // Triggered when clicking the Finish button
                onFinishing: function (e, currentIndex) {




                    var fv = newClient.data('formValidation'),
                        $container = newClient.find('section[data-step="' + currentIndex + '"]');


                    // Validate the last step container
                    fv.validateContainer($container);

                    var isValidStep = fv.isValidContainer($container);
                    if (isValidStep === false || isValidStep === null) {
                        return false;
                    }
                    return true;
                },
                onFinished: function (e, currentIndex) {
                    e.preventDefault();
                    clidAll = $("#stid").val();
                    cuotid = $("#cuotid").val();
                    $.ajax({
                        type:"post",
                        url:"/registro/validatedocument",
                        data: {"clidAll":clidAll,"cuotid":cuotid},
                        dataType: "JSON",
                        success : function(response){
                            if (response.code == 200) {
                                var modal = response.modalname;
                                $("#"+modal).modal("show");
                                var addDocument2 = $("#list-studios-client");
                                if(response.client.actaNacimiento == "" || response.client.actaNacimiento == null)
                                {
                                    /* $("#acta").removeClass("hidden");*/
                                    addDocument2.append("<li class='list-modal-incomplete'>Acta de nacimiento</li>");
                                }
                                if(response.client.cv == "" || response.client.cv == null){
                                    /*$("#cv").removeClass("hidden");*/
                                    addDocument2.append("<li class='list-modal-incomplete'>Currículum vitae</li>");
                                }
                                if(response.client.photo == "" || response.client.photo == null){
                                    /*$("#photo").removeClass("hidden");*/
                                    addDocument2.append("<li class='list-modal-incomplete'>Fotografía</li>");
                                }
                                var addDocument = $("#list-studios-client");
                                jQuery.each(response.document, function(i, val){
                                    addDocument.append("<li class='list-modal-incomplete'>"+val+"</li>");
                                });
                                $('#str').val(JSON.stringify(response.document));

                            } else {
                                /*alert("No se ha podido generar la url de pago")*/
                            }
                        },
                        error: function () {
                        }, complete: function (response) {
                            setTimeout(function () {
                                /*$("#form-success").addClass("hidden");
                                $("#form-warning").addClass("hidden");*/
                            }, 2000);
                        }
                    });
                    /*clid = $("#clidmodal").val();
                    $.ajax({
                        type: "POST",
                        url: "/registro/senddocumentincomplete",
                        data: { clid: clid },
                        dataType: "json",
                        success: function (response) {
                            if (response.code == 200) {
                            } else {
                            }
                        },
                        error: function () {

                        }, complete: function () {
                            setTimeout(function () {
                            }, 2000);
                        }
                    });*/
                }
            }).formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                excluded: ':disabled',
                locale: 'es_ES',
                fields: {
                    phone: {
                        validators: {
                            callback: {
                                message: 'El teléfono es necesario y no puede estar vacio.',
                                callback: function (value, validator, $field) {
                                    var channel = newClient.find('[name="input_required"]:checked').val();
                                    return (channel !== 'phone') ? true : (value !== '');
                                }
                            }
                        }
                    },
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'El nombre es necesario.'
                            }
                        }
                    },
                    lastname: {
                        validators: {
                            notEmpty: {
                                message: 'El apellido paterno es necesario.'
                            }
                        }
                    },
                    secondname: {
                        validators: {
                            notEmpty: {
                                message: 'El apellido materno es necesario.'
                            }
                        }
                    },
                    sex: {
                        validators: {
                            notEmpty: {
                                message: 'El sexo es necesario.'
                            }
                        }
                    },
                    comid: {
                        validators: {
                            notEmpty: {
                                message: 'La delegación es necesaria.'
                            }
                        }
                    },
                    phone: {
                        validators: {
                            notEmpty: {
                                message: 'El celular es necesario.'
                            }
                        }
                    },
                    nacionalidad: {
                        validators: {
                            notEmpty: {
                                message: 'La nacionalidad es necesaria.'
                            }
                        }
                    },
                    daybirthdate: {
                        validators: {
                            notEmpty: {
                                message: 'El día es necesario.'
                            }
                        }
                    },
                    mothbirthdate: {
                        validators: {
                            notEmpty: {
                                message: 'El mes es necesario.'
                            }
                        }
                    },
                    yearsbirthdate: {
                        validators: {
                            notEmpty: {
                                message: 'El año es necesario.'
                            }
                        }
                    },
                    titulo: {
                        validators: {
                            notEmpty: {
                                message: 'El titulo es necesario.'
                            }
                        }
                    },
                    especialidad: {
                        validators: {
                            notEmpty: {
                                message: 'La especialidad es necesaria.'
                            }
                        }
                    },
                    universidad: {
                        validators: {
                            notEmpty: {
                                message: 'La universidad es necesaria.'
                            }
                        }
                    },
                    carrera: {
                        validators: {
                            notEmpty: {
                                message: 'La carrera es necesaria.'
                            }
                        }
                    },
                    mesfinalstu: {
                        validators: {
                            notEmpty: {
                                message: 'El mes es necesario.'
                            }
                        }
                    },
                    annofinalstu: {
                        validators: {
                            notEmpty: {
                                message: 'El año es necesario.'
                            }
                        }
                    },
                    empresa: {
                        validators: {
                            notEmpty: {
                                message: 'La empresa es necesaria.'
                            }
                        }
                    },
                    puesto: {
                        validators: {
                            notEmpty: {
                                message: 'El puesto es necesario.'
                            }
                        }
                    },
                    actual: {
                        validators: {
                            notEmpty: {
                                message: 'Es necesario indicarlo.'
                            }
                        }
                    },
                    mes: {
                        validators: {
                            notEmpty: {
                                message: 'El mes es necesario.'
                            }
                        }
                    },
                    anno: {
                        validators: {
                            notEmpty: {
                                message: 'El año es necesario.'
                            }
                        }
                    },

                    mesfinal: {
                        validators: {
                            callback: {
                                message: 'El mes es necesario.',
                                callback: function(value, validator, $field) {
                                    var framework = $('#newClient').find('[name="actual"] option:selected').val();
                                    return (framework !== 'NO') ? true : (value !== '');
                                }
                            }
                        }
                    },
                    annofinal: {
                        validators: {
                            callback: {
                                message: 'El año es necesario.',
                                callback: function(value, validator, $field) {
                                    var framework = $('#newClient').find('[name="actual"] option:selected').val();
                                    return (framework !== 'NO') ? true : (value !== '');
                                }
                            }
                        }
                    },
                }
            }).on('change', '[name="actual"]', function(e) {
                $('#newClient').formValidation('revalidateField', 'mesfinal');
                $('#newClient').formValidation('revalidateField', 'annofinal');
            }).on('err.field.fv', function (e, data) {
                if (data.fv.getSubmitButton()) {
                    data.fv.disableSubmitButtons(false);
                }
            }).on('success.field.fv', function (e, data) {
                if (data.fv.getSubmitButton()) {
                    data.fv.disableSubmitButtons(false);
                }
            });
        $(".tutor").on("change", function (e) {
            var val = $(this).val();
            if (val == "SI") {
                $(".add_data_client[name='calle']").val($("#calle").val()).change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='numerointe']").val($("#numerointe").val()).change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='numeroexte']").val($("#numeroexte").val()).change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='colonia']").val($("#colonia").val()).change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='cp']").val($("#cp").val()).change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='municipio']").val($("#municipio").val()).change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='localidad']").val($("#localidad").val()).change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='estado']").val($("#estado").val()).change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='pais']").val($("#pais").val()).change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
            } else {
                $(".add_data_client[name='calle']").val("").change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='numerointe']").val("").change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='numeroexte']").val("").change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='colonia']").val("").change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='cp']").val("").change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='municipio']").val("").change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='localidad']").val("").change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='estado']").val("").change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
                $(".add_data_client[name='pais']").val("").change(function () {
                    newClient.formValidation('revalidateField', $(this));
                }).trigger("change");
            }
        });
        $("#addOtherUser").click(function (e) {
            e.preventDefault();
            newClient.data('formValidation').resetForm(true);
            resetForm(newClient);
            $("#uid").val(uid);
            $('.radio_email').prop('checked', true);
            newClient.formValidation('revalidateField', 'email');
            $('.radio_work').prop('checked', true);
            $('#scid.selectUP').select2('val', "");
            $('#crid.selectUP').select2('val', "");
            $('#hour_interest.selectUP').select2('val', "");
            if ($("#validateUid").length >= 1) {
                $('#uid.selectUP').select2('val', "");
            }
        });
        saveKeyupValue("keyup_Save", newClient, stid);
        saveChangeValue("change_Save", newClient, stid);
        saveChange2Value("change_Save2", newClient, stid);
    }








    if ($(".dropzone_file_titulo").length >= 1) {

        $(".dropzone_file_titulo").each(function (index) {
            id_v = $(this).attr("data-id");
            var workid = $(this).attr("data-workid");
            var estclid = $(this).attr("data-estclid");
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone('#' + id_v, {
                url: "/registro/uploadfile",
                uploadMultiple: false,
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 15, // MB
                maxFiles: 2,
                parallelUploads: 1,
                addRemoveLinks: true,
                dictResponseError: "No se puede subir esta archivo!",
                autoProcessQueue: true,
                thumbnailWidth: 138,
                thumbnailHeight: 120,

                resize: function (file) {
                    var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                        srcRatio = file.width / file.height;
                    if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                        info.trgHeight = this.options.thumbnailHeight;
                        info.trgWidth = info.trgHeight * srcRatio;
                        if (info.trgWidth > this.options.thumbnailWidth) {
                            info.trgWidth = this.options.thumbnailWidth;
                            info.trgHeight = info.trgWidth / srcRatio;
                        }
                    } else {
                        info.trgHeight = file.height;
                        info.trgWidth = file.width;
                    }
                    return info;
                },
                sending: function (file, xhr, formData) {
                    formData.append("workid", workid);
                },
                success: function (file, response) {
                    var name_image = response;
                    $(".dz-preview").addClass("dz-success");
                    $("div.progress").remove();
                },
                removedfile: function (file) {
                    var name = file.previewElement.accessKey;
                     $.ajax({
                        url: "/registro/deletetitulo",
                        type: "post",
                        data: { "clid":workid},
                        dataType:"json",
                        success : function(response){
                            if(response.message=="SUCCESS" && response.code==200){
                                $(file.previewElement).remove();
                                $("#input-image-principal-titulo"+workid).val("");
                                form.formValidation('revalidateField', 'input-image-principal-titulo'+workid);
                            }else{
                                alert("No se ha podido eliminar");
                            }
                        },
                        error : function(){
                            error();
                        }
                     });
                }
            });
            if ($("#input-image-principal-titulo" + workid).val() == "" || $("#input-image-principal-titulo" + workid).val() == "icon.png") {

            } else {
                var mockFile = { name: "", size: 0 };
                myDropzone.emit("addedfile", mockFile);
                $(".progress.progress-striped.active").addClass("hide");
                /*image_load = "/front/src/images/registro/"+$("#input-image-principal-titulo"+estclid).val();*/
                image_load = "/front/src/images/registro/document.png";
                myDropzone.emit("thumbnail", mockFile, image_load);
                var existingFileCount = 1; // The number of files already uploaded
                myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
            }

        });
        $("#addestudio").click(function (e) {
            e.preventDefault();
            clid = $("#stid").val();
            cuotid = $("#cuotid").val();
            $.ajax({
                type: "POST",
                url: "/registro/newestudio",
                data: { id: clid, cuotid: cuotid },
                dataType: "json",
                success: function (response) {
                    if (response.code == 200) {
                        var maxOffset = 73; // Change to whatever you want
                        var thisYear2 = (new Date()).getFullYear();
                        var minOffset = thisYear2-maxOffset;
                        var maxOffset2 = thisYear2;

                        var selectestudio = $('<select id="annofinalstu'+response.data.estclid+'" name="annofinalstu" class="form-control change_Save" data-type="5"  data-workid="'+response.data.estclid+'"><option value="">AÑO</option>');
                        for (var i = minOffset; i <= maxOffset2; i++) {
                            var year2 = i;
                            $('<option>', { value: year2, text: year2 }).appendTo(selectestudio);
                        }
                        if (response.cuotid == response.estudio.cuotid) {
                            $(".work2").append(
                                '<div class="workid2" id="'+response.data.estclid+'">'+
                                '<div class="panel panel-default">' +
                                '<div class="panel-heading studio">' +
                                '<h4 class="panel-title afiliacion">' +
                                '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' + response.data.estclid + '" aria-expanded="true" aria-controls="collapseOne">' +
                                '</a>' +
                                '</h4>' +
                                '<button data-id="' + response.data.estclid + '" type="button" class="btn btn-danger delete_studio" rel="tooltip" data-color-class="danger" data-animate="animated fadeIn" data-toggle="tooltip" data-original-title="Eliminar Formación Académica" data-placement="top"><i class="fa fa-times-circle direction-close" aria-hidden="true"></i></button>' +
                                '</div>' +
                                '<div id="collapse' + response.data.estclid + '" class="panel-collapse collapse in"  aria-expanded="true">' +
                                '<br>' +
                                '<div class="form-group">' +
                                '<div class="col-md-6">' +
                                '<select id="titulo' + response.data.estclid + '" name="titulo" class="form-control change_Save titulo" data-type="5" data-workid="' + response.data.estclid + '">' +
                                '<option  value="">SELECCIONE EL GRADO ACADÉMICO</option>' +
                                '<option value="LICENCIATURA">LICENCIATURA</option>' +
                                '<option value="MAESTRÍA">MAESTRÍA</option>' +
                                '<option value="DOCTORADO">DOCTORADO</option>' +
                                '</select>' +
                                '</div>' +
                                '<div class="col-md-6">' +
                                '<input type="text" class="form-control change_Save"  name="calificacion" id="calificacion' + response.data.estclid + '" placeholder="Calificación" value="" data-type="5" data-workid="' + response.data.estclid + '">' +
                                '</div>' +
                                '</div>' +
                                '<div class="form-group">' +
                                '<div class="col-md-6">' +
                                '<input type="text" class="form-control change_Save"  name="especialidad" id="especialidad' + response.data.estclid + '" placeholder="Semestre" value="" data-type="5" data-workid="' + response.data.estclid + '">' +
                                '</div>' +
                                '</div>' +
                                '<div class="form-group">' +
                                '<div class="col-md-6">' +
                                '<input type="text" class="form-control change_Save"  name="universidad" id="universidad' + response.data.estclid + '" placeholder="Universidad" value="" data-type="5" data-workid="' + response.data.estclid + '">' +
                                '</div>' +
                                '<div class="col-md-6">' +
                                '<input type="text" class="form-control change_Save"  name="carrera" id="carrera' + response.data.estclid + '" placeholder="Carrera" value="" data-type="5" data-workid="' + response.data.estclid + '">' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<br>'
                            );
                            $("#add-jquery-studio-document").append(
                                '<br>' +
                                '<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12"  id="comprobante'+response.data.estclid+'">' +
                                '<div class="margen">' +
                                '<label class="control-label title-document">Comprobante de estudio con promedio</label><br>' +
                                '<label class="control-label title-document2">Formato: PDF</label><br>' +
                                '<label class="control-label title-document2">Nombre del archivo con el siguiente formato:</label><br>' +
                                '<label class="control-label title-document2">APELLIDO_NOMBRE_TITULO</label>' +
                                '<div class="controls">' +
                                '<div id="filePublicaciones_titulo' + response.data.estclid + '" data-id="filePublicaciones_titulo' + response.data.estclid + '" data-workid="' + response.data.estclid + '" data-estclid="<?=$estudios->getEstclid()?>" class="dropzone dropzone_file_titulo dz-default dz-message">' +
                                '<div class="dz-default dz-message membresia">' +
                                '<h2 class="text-center membresia">' +
                                '<i class="fas fa-cloud-upload-alt" style="font-size: 62px;"></i><br/>' +
                                'Arrastra un archivo <br><i style="font-size: 14px;">o haz clic para seleccionar manualmente</i>' +
                                '<input type="hidden" name="document"  value="" data-workid="' + response.data.estclid + '" id="input-image-principal-titulo' + response.data.estclid + '">' +
                                '</h2>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<br>'
                            );
                        }
                        else {
                            $(".work2").append(
                                '<div class="workid2" id="' + response.data.estclid + '">' +
                                '<div class="panel panel-default">' +
                                '<div class="panel-heading studio">' +
                                '<h4 class="panel-title afiliacion">' +
                                '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' + response.data.estclid + '" aria-expanded="true" aria-controls="collapseOne">' +
                                '</a>' +
                                '</h4>' +
                                '<button data-id="' + response.data.estclid + '" type="button" class="btn btn-danger delete_studio" rel="tooltip" data-color-class="danger" data-animate="animated fadeIn" data-toggle="tooltip" data-original-title="Eliminar Formación Académica" data-placement="top"><i class="fa fa-times-circle direction-close" aria-hidden="true"></i></button>' +
                                '</div>' +
                                '<div id="collapse' + response.data.estclid + '" class="panel-collapse collapse in"  aria-expanded="true">' +
                                '<br>' +
                                '<div class="form-group">' +
                                '<div class="col-md-6">' +
                                '<select id="titulo' + response.data.estclid + '" name="titulo" class="form-control change_Save titulo" data-type="5" data-workid="' + response.data.estclid + '">' +
                                '<option value="">Seleccione el grado académico</option>' +
                                '<option value="LICENCIATURA">LICENCIATURA</option>' +
                                '<option value="MAESTRÍA">MAESTRÍA</option>' +
                                '<option value="DOCTORADO">DOCTORADO</option>' +
                                '</select>' +
                                '</div>' +
                                '<div class="col-md-6">' +
                                '<input type="text" class="form-control change_Save"  name="especialidad" id="especialidad<?=$estudios->getEstclid()?>" placeholder="Especialidad" value="" data-type="5" data-workid="' + response.data.estclid + '">' +
                                '</div>' +
                                '</div>' +
                                '<div class="form-group">' +
                                '<div class="col-md-6">' +
                                '<input type="text" class="form-control change_Save"  name="universidad" id="universidad' + response.data.estclid + '" placeholder="Universidad" value="" data-type="5" data-workid="' + response.data.estclid + '">' +
                                '</div>' +
                                '<div class="col-md-6">' +
                                '<input type="text" class="form-control change_Save"  name="carrera" id="carrera' + response.data.estclid + '" placeholder="Carrera" value="" data-type="5" data-workid="' + response.data.estclid + '">' +
                                '</div>' +
                                '</div>' +
                                '<div class="form-group">' +
                                '<label class="col-md-6 control-label">Fecha de Titulación </label>' +
                                '<div class="col-md-3">' +
                                '<select id="mesfinalstu' + response.data.estclid + '" name="mesfinalstu" class="form-control change_Save" data-type="5" data-workid="' + response.data.estclid + '">' +
                                '<option value="">MES</option>' +
                                '<option value="01">ENERO</option>' +
                                '<option value="02">FEBRERO</option>' +
                                '<option value="03">MARZO</option>' +
                                '<option value="04">ABRIL</option>' +
                                '<option value="05">MAYO</option>' +
                                '<option value="06">JUNIO</option>' +
                                '<option value="07">JULIO</option>' +
                                '<option value="08">AGOSTO</option>' +
                                '<option value="09">SEPTIEMBRE</option>' +
                                '<option value="10">OCTUBRE</option>' +
                                '<option value="11">NOVIEMBRE</option>' +
                                '<option value="12">DICIEMBRE</option>' +
                                '</select>' +
                                '</div>' +
                                '<div class="col-md-3" id="annoselect2'+response.data.estclid+'">' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<br>'
                            );
                            selectestudio.appendTo('#annoselect2'+response.data.estclid);
                            $("#add-jquery-studio-document").append(
                                '<br>' +
                                '<div class="col-md-6 col-sm-6 col-xs-12" id="titulo'+response.data.estclid+'">' +
                                '<div class="margen">' +
                                '<label class="control-label title-document">Título de <span id="titulo-document-titulo'+response.data.estclid+'"></span></label><br>' +
                                '<label class="control-label title-document2">Formato: PDF</label><br>' +
                                '<label class="control-label title-document2">Nombre del archivo con el siguiente formato:</label><br>' +
                                '<label class="control-label title-document2">APELLIDO_NOMBRE_TITULO</label>' +
                                '<div class="controls">' +
                                '<div id="filePublicaciones_titulo'+response.data.estclid+'" data-id="filePublicaciones_titulo'+response.data.estclid+'" data-workid="'+response.data.estclid+'" class="dropzone dropzone_file_titulo dz-default dz-message">' +
                                '<div class="dz-default dz-message membresia">' +
                                '<h2 class="text-center membresia">' +
                                '<i class="fas fa-cloud-upload-alt" style="font-size: 62px;"></i><br/>' +
                                'Arrastra un archivo <br><i style="font-size: 14px;">o haz clic para seleccionar manualmente</i>' +
                                '<input type="hidden" name="document"  value="" data-workid="'+response.data.estclid+'" id="input-image-principal-titulo'+response.data.estclid+'">' +
                                '</h2>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-md-6 col-sm-6 col-xs-12" id="cedula'+response.data.estclid+'">' +
                                '<div class="margen">' +
                                '<label class="control-label title-document">Cédula de <span id="titulo-document-cedula'+response.data.estclid+'"></span></label><br>' +
                                '<label class="control-label title-document2">Formato: PDF</label><br>' +
                                '<label class="control-label title-document2">Nombre del archivo con el siguiente formato:</label><br>' +
                                '<label class="control-label title-document2">APELLIDO_NOMBRE_TITULO</label>' +
                                '<div class="controls">' +
                                '<div id="filePublicaciones_cedula'+response.data.estclid+'" data-id="filePublicaciones_cedula"'+response.data.estclid+'" data-workid="'+response.data.estclid+'"   class="dropzone dropzone_file_cedula dz-default dz-message">' +
                                '<div class="dz-default dz-message membresia">' +
                                '<h2 class="text-center membresia">' +
                                '<i class="fas fa-cloud-upload-alt" style="font-size: 62px;"></i><br/>' +
                                'Arrastra un archivo <br><i style="font-size: 14px;">o haz clic para seleccionar manualmente</i>' +
                                '<input type="hidden" name="cedula"  value="" data-workid="'+response.data.estclid+'" id="input-image-principal-cedula'+response.data.estclid+'">' +
                                '</h2>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<br>'

                            );
                            newDropzone($("#filePublicaciones_titulo"+response.data.estclid));
                            newDropzone2($("#filePublicaciones_cedula"+response.data.estclid));

                        }
                    } else if (response.code == 404) {
                        alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
                    }
                },
                error: function () {
                    alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
                }
            });
        });

        function newDropzone(id){
            id.each(function (index) {
                id_v = $(this).attr("data-id");
                var workid = $(this).attr("data-workid");
                var estclid = $(this).attr("data-estclid");
                Dropzone.autoDiscover = false;
                var myDropzone = new Dropzone('#' + id_v, {
                    url: "/registro/uploadfile",
                    uploadMultiple: false,
                    paramName: "file", // The name that will be used to transfer the file
                    acceptedFiles: ".pdf",
                    maxFilesize: 15, // MB
                    maxFiles: 1,
                    parallelUploads: 1,
                    addRemoveLinks: true,
                    dictResponseError: "No se puede subir esta archivo!",
                    autoProcessQueue: true,
                    thumbnailWidth: 138,
                    thumbnailHeight: 120,

                    resize: function (file) {
                        var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                            srcRatio = file.width / file.height;
                        if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                            info.trgHeight = this.options.thumbnailHeight;
                            info.trgWidth = info.trgHeight * srcRatio;
                            if (info.trgWidth > this.options.thumbnailWidth) {
                                info.trgWidth = this.options.thumbnailWidth;
                                info.trgHeight = info.trgWidth / srcRatio;
                            }
                        } else {
                            info.trgHeight = file.height;
                            info.trgWidth = file.width;
                        }
                        return info;
                    },
                    sending: function (file, xhr, formData) {
                        formData.append("workid", workid);
                    },
                    success: function (file, response) {
                        var name_image = response;
                        $(".dz-preview").addClass("dz-success");
                        $("div.progress").remove();
                    },
                    removedfile: function (file) {
                        $(file.previewElement).remove();
                    }
                });

                if ($("#input-image-principal-titulo" + estclid).val() == "" || $("#input-image-principal-titulo" + estclid).val() == "icon.png") {

                } else {

                }

            });
        }
        function newDropzone2(id){

            id.each(function (index) {
                id_v = $(this).attr("data-id");
                var workid = $(this).attr("data-workid");
                var estclid = $(this).attr("data-estclid");
                Dropzone.autoDiscover = false;
                var myDropzone = new Dropzone('#' + id_v, {
                    url: "/registro/uploadfilecedula",
                    uploadMultiple: false,
                    paramName: "file", // The name that will be used to transfer the file
                    maxFilesize: 15, // MB
                    maxFiles: 1,
                    acceptedFiles: ".pdf",
                    parallelUploads: 1,
                    addRemoveLinks: true,
                    dictResponseError: "No se puede subir esta archivo!",
                    autoProcessQueue: true,
                    thumbnailWidth: 138,
                    thumbnailHeight: 120,

                    resize: function (file) {
                        var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                            srcRatio = file.width / file.height;
                        if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                            info.trgHeight = this.options.thumbnailHeight;
                            info.trgWidth = info.trgHeight * srcRatio;
                            if (info.trgWidth > this.options.thumbnailWidth) {
                                info.trgWidth = this.options.thumbnailWidth;
                                info.trgHeight = info.trgWidth / srcRatio;
                            }
                        } else {
                            info.trgHeight = file.height;
                            info.trgWidth = file.width;
                        }
                        return info;
                    },
                    sending: function (file, xhr, formData) {
                        formData.append("workid", workid);

                    },
                    success: function (file, response) {
                        var name_image = response;
                        $(".dz-preview").addClass("dz-success");
                        $("div.progress").remove();

                    },
                    removedfile: function (file) {
                        /*$(file.previewElement).remove();*/

                        var name = file.previewElement.accessKey;
                        $.ajax({
                           url: "/registro/deletecedula",
                           type: "post",
                           data: { "clid":workid},
                           dataType:"json",
                           success : function(response){
                               if(response.message=="SUCCESS" && response.code==200){
                                   $(file.previewElement).remove();
                                   $("#input-image-principal-cedula"+estclid).val("");
                                   form.formValidation('revalidateField', 'input-image-principal-cedula'+estclid);
                               }else{
                                   alert("No se ha podido eliminar");
                               }
                           },
                           error : function(){
                               error();
                           }
                        });
                    }
                });
                if ($("#input-image-principal-cedula" + estclid).val() == "" || $("#input-image-principal-cedula" + estclid).val() == "icon.png") {
                } else {
                    var mockFile = { name: "", size: 0 };
                    myDropzone.emit("addedfile", mockFile);
                    $(".progress.progress-striped.active").addClass("hide");
                    /*image_load = "/front/src/images/registro/"+$("#input-image-principal-cedula"+estclid).val();*/
                    image_load = "/front/src/images/registro/document.png";
                    myDropzone.emit("thumbnail", mockFile, image_load);
                    var existingFileCount = 1; // The number of files already uploaded
                    myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
                }
            });
        }
    }
    if ($(".dropzone_file_cedula").length >= 1) {

        $(".dropzone_file_cedula").each(function (index) {
            var name_document = "";
            id_v = $(this).attr("data-id");
            var workid = $(this).attr("data-workid");
            var estclid = $(this).attr("data-estclid");
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone('#' + id_v, {
                url: "/registro/uploadfilecedula",
                uploadMultiple: false,
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 15, // MB
                maxFiles: 1,
                acceptedFiles: ".pdf",
                parallelUploads: 1,
                addRemoveLinks: true,
                dictResponseError: "No se puede subir esta archivo!",
                autoProcessQueue: true,
                thumbnailWidth: 138,
                thumbnailHeight: 120,

                resize: function (file) {
                    var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                        srcRatio = file.width / file.height;
                    if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                        info.trgHeight = this.options.thumbnailHeight;
                        info.trgWidth = info.trgHeight * srcRatio;
                        if (info.trgWidth > this.options.thumbnailWidth) {
                            info.trgWidth = this.options.thumbnailWidth;
                            info.trgHeight = info.trgWidth / srcRatio;
                        }
                    } else {
                        info.trgHeight = file.height;
                        info.trgWidth = file.width;
                    }
                    return info;
                },
                sending: function (file, xhr, formData) {
                    formData.append("workid", workid);

                },
                success: function (file, response) {
                    name_document = response;
                    $(".dz-preview").addClass("dz-success");
                    $("div.progress").remove();

                },
                removedfile: function (file) {
                    var name = file.previewElement.accessKey;
                     $.ajax({
                        url: "/registro/deletecedula",
                        type: "post",
                        data: { "clid":workid},
                        dataType:"json",
                        success : function(response){
                            if(response.message=="SUCCESS" && response.code==200){
                                $(file.previewElement).remove();
                                $("#input-image-principal-cedula"+estclid).val("");
                                form.formValidation('revalidateField', 'input-image-principal-cedula'+estclid);
                            }else{
                                alert("No se ha podido eliminar");
                            }
                        },
                        error : function(){
                            error();
                        }
                     });
                }
            });
            if ($("#input-image-principal-cedula"+estclid).val() == "" || $("#input-image-principal-cedula" + estclid).val() == "icon.png") {

            } else {

                var mockFile = { name: "", size: 0 };
                myDropzone.emit("addedfile", mockFile);
                $(".progress.progress-striped.active").addClass("hide");
                /*image_load = "/front/src/images/registro/"+$("#input-image-principal-cedula"+estclid).val();*/
                image_load = "/front/src/images/registro/document.png";
                myDropzone.emit("thumbnail", mockFile, image_load);
                var existingFileCount = 1; // The number of files already uploaded
                myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
            }
        });

    }
    if ($(".dropzone_file_acta").length >= 1) {
        $(".dropzone_file_acta").each(function (index) {
            id_v = $(this).attr("data-id");
            var workid = $(this).attr("data-workid");
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone('#' + id_v, {
                url: "/registro/uploadfileacta",
                uploadMultiple: false,
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 15, // MB
                maxFiles: 1,
                acceptedFiles: ".pdf",
                parallelUploads: 1,
                addRemoveLinks: true,
                dictResponseError: "No se puede subir esta archivo!",
                autoProcessQueue: true,
                thumbnailWidth: 138,
                thumbnailHeight: 120,
                sending: function (file, xhr, formData) {
                    formData.append("workid", workid);
                },
                success: function (file, response) {
                    var name_image = response;
                    $(".dz-preview").addClass("dz-success");
                    $("div.progress").remove();
                },
                removedfile: function (file) {
                    var name = file.previewElement.accessKey;
                     $.ajax({
                        url: "/registro/deleteacta",
                        type: "post",
                        data: { "clid":workid},
                        dataType:"json",
                        success : function(response){
                            if(response.message=="SUCCESS" && response.code==200){
                                $(file.previewElement).remove();
                                $("#input-image-principal").val("");
                                form.formValidation('revalidateField', 'input-image-principal');
                            }else{
                                alert("No se ha podido eliminar");
                            }
                        },
                        error : function(){
                            error();
                        }
                     });
                }
            });
            if ($("#input-image-principal").val() == "" || $("#input-image-principal").val() == "icon.png") {
            } else {
                var mockFile = { name: "Click en remove file", size: 12345 };
                myDropzone.emit("addedfile", mockFile);
                $(".progress.progress-striped.active").addClass("hide");
                image_load = "/front/src/images/registro/document.png";
                myDropzone.emit("thumbnail", mockFile, image_load);
                var existingFileCount = 1; // The number of files already uploaded
                myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
            }
        });
    }
    if ($(".dropzone_file_foto").length >= 1) {

        $(".dropzone_file_foto").each(function (index) {
            id_v = $(this).attr("data-id");
            var workid = $(this).attr("data-workid");
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone('#' + id_v, {
                url: "/registro/uploadfilephoto",
                uploadMultiple: false,
                acceptedFiles: "image/jpeg,image/jpg",
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 15, // MB
                maxFiles: 1,
                acceptedFiles: ".jpeg,.jpg",
                parallelUploads: 1,
                addRemoveLinks: true,
                dictResponseError: "No se puede subir esta archivo!",
                autoProcessQueue: true,
                thumbnailWidth: 138,
                thumbnailHeight: 120,

                resize: function (file) {
                    var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                        srcRatio = file.width / file.height;
                    if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                        info.trgHeight = this.options.thumbnailHeight;
                        info.trgWidth = info.trgHeight * srcRatio;
                        if (info.trgWidth > this.options.thumbnailWidth) {
                            info.trgWidth = this.options.thumbnailWidth;
                            info.trgHeight = info.trgWidth / srcRatio;
                        }
                    } else {
                        info.trgHeight = file.height;
                        info.trgWidth = file.width;
                    }
                    return info;
                },
                sending: function (file, xhr, formData) {
                    formData.append("workid", workid);
                },
                /*success: function(file, response){
                 var name_image = response;
                 $(".dz-preview").addClass("dz-success");
                 $("div.progress").remove();
                 },*/


                success: function (file, response) {
                    var name_image = response;
                    $("#input-document-foto").val(name_image.name);
                    $(".dz-preview").addClass("dz-success");
                    $("div.progress").remove();
                    file.previewElement.accessKey = name_image.name;
                    form.formValidation('revalidateField', 'input-document-foto');
                },
                /*removedfile: function(file) {
                 $(file.previewElement).remove();
                 }*/

                removedfile: function (file) {
                    /*$(file.previewElement).remove();*/
                    var name = file.previewElement.accessKey;
                     $.ajax({
                        url: "/registro/deletephoto",
                        type: "post",
                        data: { "clid":workid},
                        dataType:"json",
                        success : function(response){
                            if(response.message=="SUCCESS" && response.code==200){
                                $(file.previewElement).remove();
                                $("#input-document-cv").val("");
                                form.formValidation('revalidateField', 'input-document-cv');
                            }else{
                                alert("No se ha podido eliminar");
                            }
                        },
                        error : function(){
                            error();
                        }
                     });
                }
            });

            if ($("#input-document-foto").val() == "" || $("#input-document-foto").val() == "icon.png") {

            } else {
                var mockFile = { name: "", size: 12345 };
                myDropzone.emit("addedfile", mockFile);
                $(".progress.progress-striped.active").addClass("hide");
                image_load = "/front/src/images/registro/document.png";
                myDropzone.emit("thumbnail", mockFile, image_load);
                var existingFileCount = 1; // The number of files already uploaded
                myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
            }

        });


    }
    if ($(".dropzone_file_efectivo").length >= 1) {

        $(".dropzone_file_efectivo").each(function (index) {
            id_v = $(this).attr("data-id");
            var workid = $(this).attr("data-workid");
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone('#' + id_v, {
                url: "/registro/uploadfileefectivo",
                uploadMultiple: false,
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 15, // MB
                maxFiles: 1,
                acceptedFiles: ".pdf",
                parallelUploads: 1,
                addRemoveLinks: true,
                dictResponseError: "No se puede subir esta archivo!",
                autoProcessQueue: true,
                thumbnailWidth: 138,
                thumbnailHeight: 120,

                resize: function (file) {
                    var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                        srcRatio = file.width / file.height;
                    if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                        info.trgHeight = this.options.thumbnailHeight;
                        info.trgWidth = info.trgHeight * srcRatio;
                        if (info.trgWidth > this.options.thumbnailWidth) {
                            info.trgWidth = this.options.thumbnailWidth;
                            info.trgHeight = info.trgWidth / srcRatio;
                        }
                    } else {
                        info.trgHeight = file.height;
                        info.trgWidth = file.width;
                    }
                    return info;
                },
                sending: function (file, xhr, formData) {
                    formData.append("workid", workid);
                },
                /*success: function(file, response){
                 var name_image = response;
                 $(".dz-preview").addClass("dz-success");
                 $("div.progress").remove();
                 },*/


                success: function (file, response) {
                    var name_image = response;
                    $("#input-document-efectivo").val(name_image.name);
                    $(".dz-preview").addClass("dz-success");
                    $("div.progress").remove();
                    file.previewElement.accessKey = name_image.name;
                    form.formValidation('revalidateField', 'input-document-efectivo');
                },
                /*removedfile: function(file) {
                 $(file.previewElement).remove();
                 }*/

                removedfile: function (file) {
                    $(file.previewElement).remove();
                    /*var name = file.previewElement.accessKey;
                     $.ajax({
                     url: "/registro/deletecv",
                     type: "post",
                     data: { "clid":workid},
                     dataType:"json",
                     success : function(response){
                     if(response.message=="SUCCESS" && response.code==200){
                     $(file.previewElement).remove();
                     $("#input-document-cv").val("");
                     form.formValidation('revalidateField', 'input-document-cv');
                     }else{
                     alert("No se ha podido eliminar");
                     }
                     },
                     error : function(){
                     error();
                     }
                     });*/
                }
            });

            if ($("#input-document-efectivo").val() == "" || $("#input-document-efectivo").val() == "icon.png") {

            } else {
                var mockFile = { name: "", size: 12345 };
                myDropzone.emit("addedfile", mockFile);
                $(".progress.progress-striped.active").addClass("hide");
                image_load = "/front/src/images/registro/document.png";
                myDropzone.emit("thumbnail", mockFile, image_load);
                var existingFileCount = 1; // The number of files already uploaded
                myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
            }

        });


    }

    if ($(".dropzone_file_cv").length >= 1) {

        $(".dropzone_file_cv").each(function (index) {
            id_v = $(this).attr("data-id");
            var workid = $(this).attr("data-workid");
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone('#' + id_v, {
                url: "/registro/uploadfilecv",
                uploadMultiple: false,
                paramName: "file", // The name that will be used to transfer the file
                maxFilesize: 15, // MB
                maxFiles: 1,
                acceptedFiles: ".pdf",
                parallelUploads: 1,
                addRemoveLinks: true,
                dictResponseError: "No se puede subir esta archivo!",
                autoProcessQueue: true,
                thumbnailWidth: 138,
                thumbnailHeight: 120,

                /*resize: function(file) {
                    var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                        srcRatio = file.width / file.height;
                    if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                        info.trgHeight = this.options.thumbnailHeight;
                        info.trgWidth = info.trgHeight * srcRatio;
                        if (info.trgWidth > this.options.thumbnailWidth) {
                            info.trgWidth = this.options.thumbnailWidth;
                            info.trgHeight = info.trgWidth / srcRatio;
                        }
                    } else {
                        info.trgHeight = file.height;
                        info.trgWidth = file.width;
                    }
                    return info;
                },*/
                sending: function (file, xhr, formData) {
                    formData.append("workid", workid);
                },
                success: function (file, response) {
                    var name_image = response;
                    $(".dz-preview").addClass("dz-success");
                    $("div.progress").remove();
                },


                removedfile: function (file) {
                    /*$(file.previewElement).remove();*/
                    var name = file.previewElement.accessKey;
                     $.ajax({
                        url: "/registro/deletecv",
                        type: "post",
                        data: { "clid":workid},
                        dataType:"json",
                        success : function(response){
                            if(response.message=="SUCCESS" && response.code==200){
                                $(file.previewElement).remove();
                                $("#input-document-cv").val("");
                                form.formValidation('revalidateField', 'input-document-cv');
                            }else{
                                alert("No se ha podido eliminar");
                            }
                        },
                        error : function(){
                            error();
                        }
                     });
                }
            });

            if ($("#input-document-cv").val() == "" || $("#input-document-cv").val() == "icon.png") {

            } else {
                var mockFile = { name: "", size: 12345 };
                myDropzone.emit("addedfile", mockFile);
                $(".progress.progress-striped.active").addClass("hide");
                image_load = "/front/src/images/registro/document.png";
                myDropzone.emit("thumbnail", mockFile, image_load);
                var existingFileCount = 1; // The number of files already uploaded
                myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
            }

        });
    }
    $("#addwork").click(function (e) {
        e.preventDefault();
        clid = $("#stid").val();
        coutid = $("#coutid").val();
        $.ajax({
            type: "POST",
            url: "/registro/newwork",
            data: { id: clid },
            dataType: "json",
            success: function (response) {
                if (response.code == 200) {
                    var thisYear = (new Date()).getFullYear();
                    var select = $('<select id="anno' + response.data.workclid + '" name="anno" class="form-control keyup_Save" data-type="4"  data-workid="'+response.data.workclid+'"><option value="">AÑO</option>');
                    for (var i = thisYear; i >= thisYear-55; i--) {
                        var year = i;
                        $('<option>', { value: year, text: year }).appendTo(select);
                    }
                    var select2 = $('<select id="annofinal' + response.data.workclid + '" name="annofinal" class="form-control keyup_Save" data-type="4"  data-workid="'+response.data.workclid+'"><option value="">AÑO</option>');
                    for (var i = thisYear; i >= thisYear-55; i--) {
                        var year = i;
                        $('<option>', { value: year, text: year }).appendTo(select2);
                    }
                    $(".work").append(
                        '<br>' +
                        '<div class="workid" id="'+response.data.workclid+'">' +
                        '<div class="panel panel-default">' +
                        '<div class="panel-heading">' +

                        '<h4 class="panel-title afiliacion">' +
                        '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' + response.data.workclid + '" aria-expanded="true" aria-controls="collapseOne">' +
                        '</a>' +
                        '</h4>' +
                        '<button data-id="' + response.data.workclid + '" type="button" class="btn btn-danger delete_work" rel="tooltip" data-color-class="danger" data-animate="animated fadeIn" data-toggle="tooltip" data-original-title="Eliminar Trabajo" data-placement="top"><i class="fa fa-times-circle direction-close" aria-hidden="true"></i></button>' +
                        '</div>' +
                        '<div id="collapse' + response.data.workclid + '" class="panel-collapse collapse in" aria-expanded="true">' +
                        '<br>' +
                        '<div class="form-group">' +
                        '<div class="col-md-6">' +
                        '<input type="text" class="form-control keyup_Save"  name="empresa" data-workid="' + response.data.workclid + '" value="" placeholder="Empresa" data-type="4">' +
                        '</div>' +
                        '<div class="col-md-6">' +
                        '<input type="text" class="form-control keyup_Save"  name="puesto" data-workid="' + response.data.workclid + '" placeholder="Puesto" value="" data-type="4">' +
                        '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<div class="col-md-6">' +
                        '<select id="actual" name="actual" data-workid="' + response.data.workclid + '" class="form-control keyup_Save workchange" data-type="4">' +
                        '<option value="">¿Es su trabajo actual?</option>' +
                        '<option value="SI">SI</option>' +
                        '<option value="NO">NO</option>' +
                        '</select>' +
                        '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<label class="col-md-2 col-xs-12 col-lg-2 control-label">Fecha inicio </label>' +
                        '<div class="col-md-2 col-sm-6 col-xs-6 col-lg-2">' +
                        '<select name="mes" class="form-control keyup_Save" placeholder="Mes inicial" value="" data-type="4" data-workid="' + response.data.workclid + '">' +
                        '<option value="">MES</option>' +
                        '<option value="01">ENERO</option>' +
                        '<option value="02">FEBRERO</option>' +
                        '<option value="03">MARZO</option>' +
                        '<option value="04">ABRIL</option>' +
                        '<option value="05">MAYO</option>' +
                        '<option value="06">JUNIO</option>' +
                        '<option value="07">JULIO</option>' +
                        '<option value="08">AGOSTO</option>' +
                        '<option value="09">SEPTIEMBRE</option>' +
                        '<option value="10">OCTUBRE</option>' +
                        '<option value="11">NOVIEMBRE</option>' +
                        '<option value="12">DICIEMBRE</option>' +
                        '</select>' +
                        '</div>' +
                        '<div class="col-md-2 col-sm-6 col-xs-6 col-lg-2" id="annoselect'+response.data.workclid+'">' +
                        '</div>' +
                        '<label class="col-md-2 col-xs-12 col-lg-2 control-label hidden" id="label-date-end' + response.data.workclid + '">Fecha fin </label>' +
                        '<div class="col-md-2 col-sm-6 col-xs-6 col-lg-2 hidden" id="moth-date-end' + response.data.workclid + '">' +
                        '<select id="mesfinal' + response.data.workclid + '" name="mesfinal" class="form-control keyup_Save" data-type="4" data-workid="' + response.data.workclid + '">' +
                        '<option value="">MES</option>' +
                        '<option value="01">ENERO</option>' +
                        '<option value="02">FEBRERO</option>' +
                        '<option value="03">MARZO</option>' +
                        '<option value="04">ABRIL</option>' +
                        '<option value="05">MAYO</option>' +
                        '<option value="06">JUNIO</option>' +
                        '<option value="07">JULIO</option>' +
                        '<option value="08">AGOSTO</option>' +
                        '<option value="09">SEPTIEMBRE</option>' +
                        '<option value="10">OCTUBRE</option>' +
                        '<option value="11">NOVIEMBRE</option>' +
                        '<option value="12">DICIEMBRE</option>' +
                        '</select>' +
                        '</div>' +
                        '<div class="col-md-2 col-sm-6 col-xs-6 col-lg-2 hidden" id="annoselect3' + response.data.workclid + '">' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                    select2.appendTo('#annoselect3' + response.data.workclid);
                    select.appendTo('#annoselect'+response.data.workclid);
                } else if (response.code == 404) {
                    alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
                }
            },
            error: function () {
                alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
            }
        });
    });

    var factura_form = $("#newClient");
    /*createDatePicker(1,factura_form,"date");*/

    if (factura_form.length >= 1) {
        factura_form.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            locale: 'es_ES',
            fields: {
              cuotid: {
                  validators: {
                      notEmpty: {
                          message: 'El tipo de asociado no puede estar vacio.'
                      }
                  }
              }
            }
        }).on('success.form.fv', function (e) {
            jQuery("#mail-c").removeClass("hide");
            jQuery("#background-contact").modal({ backdrop: 'static', keyboard: false, show: true });
            var form = jQuery(this);
            var code;
            jQuery.ajax({
                type: "POST",
                data: jQuery(this).serialize(),
                url: "/registro/savepay",
                dataType: "json",
                success: function (response) {
                    code = response.code;
                    switch (code) {
                        case 200:
                            window.location.href = response.url;
                            break;
                        case 404:
                            jQuery("#dg-c").removeClass("hide").addClass("in");
                            jQuery("#error-contact").removeClass("hide");
                            break;
                    };
                },
                error: function () {
                    console.log("error11");
                },
                complete: function () {
                    setTimeout(function () {
                        switch (code) {
                            case 200:
                                resetForm(form);
                                break;
                            case 404:
                                jQuery("#dg-c").removeClass("in").addClass("hide");
                                break;
                        }
                        jQuery("#background-contact").modal("hide");
                        jQuery(".message-background").addClass("hide");
                        jQuery("#wm").removeClass("hide");
                    }, 3000);
                }
            });
            return false;
        });
    }
    $(document.body).on('change',"#newClient .workchange", function (e) {
        e.preventDefault();
        valor = $(this).val();
        id = $(this).attr("data-workid");
        label = $("#label-date-end" + id);
        moth = $("#moth-date-end" + id);
        year = $("#annoselect3" + id);
        if (valor == "NO") {
            label.removeClass("hidden");
            moth.removeClass("hidden");
            year.removeClass("hidden");
        }
        else {
            label.addClass("hidden");
            moth.addClass("hidden");
            year.addClass("hidden");
        }
    });

    $(document.body).on('change',"#newClient .titulo", function (e) {
        e.preventDefault();
        valor = $(this).val();
        id = $(this).attr("data-workid");
        addtitle = $("#titulo-document-titulo" + id).text(valor);
        addtitle2 = $("#titulo-document-cedula" + id).text(valor);
    });

    $("#adddirection").click(function (e) {
        e.preventDefault();
        clid = $("#stid").val();
        $.ajax({
            type: "POST",
            url: "/registro/newdirection",
            data: { id: clid },
            dataType: "json",
            success: function (response) {
                if (response.code == 200) {
                    $(".directions2").append(
                        '<br>' +

                        '<div class="direction2"  id="'+response.data.dirclid+'">' +

                        '<div class="panel panel-default">' +
                        '<div class="panel-heading">' +
                        '<h4 class="panel-title afiliacion">' +
                        '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse' + response.data.dirclid + '" aria-expanded="true" aria-controls="collapse' + response.data.dirclid + '">' +
                        '</a>' +
                        '</h4>' +
                        '<button data-id="'+response.data.dirclid +'" type="button" class="btn btn-danger delete_direction" rel="tooltip" data-color-class="danger" data-animate="animated fadeIn" data-toggle="tooltip" data-original-title="Eliminar Dirección" data-placement="top"><i class="fa fa-times-circle direction-close" aria-hidden="true"></i></button>' +
                        '</div>' +
                        '<div id="collapse' + response.data.dirclid + '" class="panel-collapse collapse in" aria-expanded="true">' +
                        '<div class="form-group">' +
                        '<div class="col-md-6">' +
                        '<select id="type" name="type" class="form-control change_Save2" data-type="3" data-workid="' + response.data.dirclid + '">' +
                        '<option  value="">Seleccione el tipo de domicilio</option>' +
                        '<option value="PARTICULAR">PARTICULAR</option>' +
                        '<option value="OFICINA">OFICINA</option>' +
                        '<option value="OTRO">OTRO</option>' +
                        '</select>' +
                        '</div>' +
                        '<div class="col-md-6">' +
                        '<input type="text" class="form-control change_Save2"  name="calle" id="calle" placeholder="Calle" value="" data-type="3" data-workid="' + response.data.dirclid + '">' +
                        '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<div class="col-md-6">' +
                        '<input type="text" class="form-control change_Save2"  name="numerointe" id="numerointe" placeholder="Número interior" value="" data-type="3" data-workid="' + response.data.dirclid + '">' +
                        '</div>' +
                        '<div class="col-md-6">' +
                        '<input type="text" class="form-control change_Save2"  name="numeroexte" id="numeroexte" placeholder="Número Exterior" value="" data-type="3" data-workid="' + response.data.dirclid + '">' +
                        '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<div class="col-md-6">' +
                        '<input type="text" class="form-control change_Save2"  name="colonia" id="colonia" placeholder="Colonia" value="" data-type="3" data-workid="' + response.data.dirclid + '">' +
                        '</div>' +
                        '<div class="col-md-6">' +
                        '<input type="text" class="form-control change_Save2"  name="cp" id="cp" placeholder="Código Postal" value="" data-type="3" data-workid="' + response.data.dirclid + '">' +
                        '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<div class="col-md-6">' +
                        '<input type="text" class="form-control change_Save2"  name="municipio" id="municipio" placeholder="Municipio" value="" data-type="3" data-workid="' + response.data.dirclid + '">' +
                        '</div>' +
                        '<div class="col-md-6">' +
                        '<input type="text" class="form-control change_Save2"  name="localidad" id="localidad" placeholder="Ciudad" value="" data-type="3" data-workid="' + response.data.dirclid + '">' +
                        '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                        '<div class="col-md-6">' +
                        '<input type="text" class="form-control change_Save2"  name="estado" id="estado" placeholder="Estado" value="" data-type="3" data-workid="' + response.data.dirclid + '">' +
                        '</div>' +
                        '<div class="col-md-6">' +
                        '<input type="text" class="form-control change_Save2"  name="pais" id="pais" placeholder="País" value="" data-type="3" data-workid="' + response.data.dirclid + '">' +
                        '</div>' +

                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );
                } else if (response.code == 404) {
                    alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
                }
            },
            error: function () {
                alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
            }
        });
    });

    /* Pago transferencia */
    $("#others").click(function () {
        $("div.form-group.transfer").addClass("height-onlypay animated fadeInUp");
        $("div.form-group.banco").removeClass("height-onlypay animated fadeInUp");
        $("div.form-group.pay").removeClass("height-onlypay animated fadeInUp");
        var table2 = $("table#table_cost_paypal");
        table2.html("");
        var table = $("table#table_cost_banco");
        table.html("");

        $("div#details-others").removeClass("hidden")



        var table3 = $("table#table_cost_banco");
        var inscription = $("input[name=costo_inscription]").val();
        var cuotanombre = $("input[name=name_cuota]").val();
        var costocuota = $("input[name=costo_cuota]").val();
        var id = parseInt(inscription) + parseInt(costocuota);
        table3.html("<thead class='animated infinite fadeInUp' ><tr class='blanc'><th class='border-table'>Concepto</th><th class='border-table blacn'>Monto</th></tr></thead>" +
            "<tbody class='animated infinite fadeInUp'>" +
            "<tr><td class='border-table'><span class='title-left'>  Inscripción </span></td><th class='mont'>$" + inscription + "</th></tr>" +
            "<tr><td class='border-table'><span class='title-left'>   Cuota Anual 2018; Asociado " + cuotanombre + " </span></td><th class='mont'>$" + costocuota + "</th></tr>" +
            "<tr><td class='border-table'><span class='title-right'> Total </span></td><th class='mont'>$" + id + "</th></tr>" +
            "</tbody>"
        );
    });



    /* Pago banco */
    $("#banco").click(function () {


        if ("#banco:checked") {
            $("div.form-group.banco").addClass("height-onlypay animated fadeInUp");
            $("div.form-group.transfer").removeClass("height-onlypay animated fadeInUp");
            $("div.form-group.pay").removeClass("height-onlypay animated fadeInUp");
            $("div#details-others").addClass("hidden");
            var table2 = $("table#table_cost_paypal");
            table2.html("");
            var table = $("table#table_cost_banco");
            var inscription = $("input[name=costo_inscription]").val();
            var cuotanombre = $("input[name=name_cuota]").val();
            var costocuota = $("input[name=costo_cuota]").val();
            var id = parseInt(inscription) + parseInt(costocuota);
            table.html("<thead class='animated infinite fadeInUp' ><tr><th class='blanc'>Concepto</th><th class='blanc'>Monto</th></tr></thead>" +
                "<tbody class='animated infinite fadeInUp'>" +
                "<tr><td class='border-table'><span class='title-left'>  Inscripción </span></td><th class='mont'>$" + inscription + "</th></tr>" +
                "<tr><td class='border-table'><span class='title-left'>   Cuota Anual 2018; Asociado " + cuotanombre + " </span></td><th class='mont'>$" + costocuota + "</th></tr>" +
                "<tr><td class='border-table'><span class='title-right'> Total </span></td><th class='mont'>$" + id + "</th></tr>" +
                "</tbody>"
            );
        }
        else {
            table.html("");
        }
    });
    /* Pago paypal */
    $("#paypal").click(function () {

        if ("#paypal:checked") {
            $("div.form-group.pay").addClass("height-onlypay animated fadeInUp");
            $("div.form-group.transfer").removeClass("height-onlypay animated fadeInUp");
            $("div.form-group.banco").removeClass("height-onlypay animated fadeInUp");
            $("div#details-others").addClass("hidden");
            var table2 = $("table#table_cost_banco");
            table2.html("");
            var table = $("table#table_cost_paypal");
            var inscription = $("input[name=costo_inscription]").val();
            var cuotanombre = $("input[name=name_cuota]").val();
            var costocuota = $("input[name=costo_cuota]").val();
            var id = parseInt(inscription) + parseInt(costocuota);
            var comisionPaypal = (((id * 0.034) + 4) * 1.16);

            subsidioAMGP = (comisionPaypal / 2) * (-1);
            total = comisionPaypal + id + subsidioAMGP;
            table.html("<thead class='animated infinite fadeInUp'><tr><th class='blanc'>Concepto</th><th class='blanc'>Monto</th></tr></thead>" +
                "<tbody class='animated infinite fadeInUp'>" +
                "<tr><td class='border-table'><span class='title-left'>  Inscripción </span></td><th class='mont'>$" + inscription + "</th></tr>" +
                "<tr><td class='border-table'><span class='title-left'>   Cuotas Anualidad 2018; Asociado " + cuotanombre + " </span></td><th class='mont'>$" + costocuota + "</th></tr>" +
                "<tr><td class='border-table'><span class='title-right'> Subtotal </span></td><th class='mont'>$" + id + "</th></tr>" +
                "<tr><td class='border-table'><span class='title-left'> Comisión Paypal </span></td><th class='mont'>$" + number_format(comisionPaypal, "2", ".", ",") + "</th></tr>" +
                "<tr><td class='border-table'><span class='title-left'> Subsidio AMGP pago PAYPAL (50%) </span></td><th class='mont'>$" + number_format(subsidioAMGP, "2", ".", ",") + "</th></tr>" +
                "<tr><td class='border-table'><span class='title-right'> Total </span></td><th class='mont'>$" + number_format(total, "2", ".", ",") + "</th></tr>" +
                "</tbody>"
            );
        }
        else {
            table.html("");
        }
    });
    function number_format(number, decimals, decPoint, thousandsSep) {

        number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
        var n = !isFinite(+number) ? 0 : +number
        var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
        var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
        var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
        var s = ''

        var toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec)
            return '' + (Math.round(n * k) / k)
                .toFixed(prec)
        }

        // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || ''
            s[1] += new Array(prec - s[1].length + 1).join('0')
        }

        return s.join(dec)
    }

});
function saveKeyupValue(selector, form, id) {
    $(document.body).on("change", ".work ." + selector, function (e) {
        e.preventDefault();
        var name = $(this).attr("name");
        var val = $(this).val();
        var work = $(this).attr("data-workid");
        var type = $(this).attr("data-type");
        $.ajax({
            type: "POST",
            url: "/registro/save",
            data: { name: name, id: id, work: work, val: val.toUpperCase(), type: type },
            dataType: "json",
            success: function (response) {
                if (response.code == 200) {
                    //alertMessage("success","Guardado correctamente");
                    //console.log("Guardado correctamente");
                } else if (response.code == 404) {
                    alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
                }
            },
            error: function () {
                alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
            }
        });
    });
}
function saveChange2Value(selector, form, id) {
    $("." + selector).on("change", function (e) {
        e.preventDefault();
        var name = $(this).attr("name");
        var val = $(this).val();
        var work = $(this).attr("data-workid");
        var type = $(this).attr("data-type");
        if (name == "cyid" || name == "scid" || name == "crid" || name == "hour_interest" || name == "mpid" || name == "mpidT" || name == "mpidR1" || name == "mpidR2" || name == "uid") if (val) val = val[0];

        $.ajax({
            type: "POST",
            url: "/registro/save",
            data: { name: name, work: work, id: id, val: val, type: type },
            dataType: "json",
            success: function (response) {
                if (response.code == 200) {
                    //alertMessage("success","Guardado correctamente");
                    //console.log("Guardado correctamente");
                } else if (response.code == 404) {
                    alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
                }
            },
            error: function () {
                alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
            }
        });
    });
}
function saveChangeValue(selector, form, id) {
    $(document.body).on("change", ".work2 ." + selector, function (e) {
        /*$("."+selector).on("change",function (e) {*/
        e.preventDefault();
        var name = $(this).attr("name");
        var val = $(this).val();
        var work = $(this).attr("data-workid");
        var type = $(this).attr("data-type");
        if (name == "cyid" || name == "scid" || name == "crid" || name == "hour_interest" || name == "mpid" || name == "mpidT" || name == "mpidR1" || name == "mpidR2" || name == "uid") if (val) val = val[0];

        $.ajax({
            type: "POST",
            url: "/registro/save",
            data: { name: name, id: id, work: work, val: val, type: type },
            dataType: "json",
            success: function (response) {
                if (response.code == 200) {
                    //alertMessage("success","Guardado correctamente");
                    //console.log("Guardado correctamente");
                } else if (response.code == 404) {
                    alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
                }
            },
            error: function () {
                alertMessage("warning", "Ha ocurrido un error, intente nuevamnete por favor");
            }
        });
    });
}
function validateDate(newClient) {
    $('#birthdate').datepicker({
        format: 'dd/mm/yyyy',
        language: "es",
        autoclose: true
    });

    $('.getDatepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: "es",
        autoclose: true
    });
    $('.begindatework').datepicker({
        format: "yyyy-mm",
        startView: "months",
        minViewMode: "months"
    });
    $('.begindateestudio').datepicker({
        format: "yyyy-mm",
        startView: "months",
        minViewMode: "months"
    });
    $('.enddatework').datepicker({
        format: "yyyy-mm",
        startView: "months",
        minViewMode: "months"
    });
    $('.enddateestudio').datepicker({
        format: "yyyy-mm",
        startView: "months",
        minViewMode: "months"
    });
    (function ($) {
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Hoy",
            clear: "Borrar",
            weekStart: 1,
            format: "dd/mm/yyyy"
        };
    }(jQuery));
}
function resetForm($form) {
    $form.find('input:text, input:password, input:file, input[type=email], select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}
function button(status) {
    var btn = $(".btn-submit");
    var btn_message_principal = btn.data("value");
    var btn_message = btn.data("message");
    var loading_fa = $("#loading_fa");
    switch (status) {
        case 1: btn.attr("disabled", "disabled").val(btn_message);
            loading_fa.removeClass("hidden");
            break;
        case 2: btn.removeAttr("disabled").removeClass("disabled").val(btn_message_principal);
            loading_fa.addClass("hidden");
            break;
    }
}
function messages(status) {
    switch (status) {
        case 1: $("#warning_alert_message").removeClass("hidden").addClass("in");
            setTimeout(function () { $("#warning_alert_message").removeClass("in").addClass("hidden"); }, 2000);
            break;
        case 2: $("#error_alert_message").removeClass("hidden").addClass("in");
            setTimeout(function () { $("#error_alert_message").removeClass("in").addClass("hidden"); }, 2000);
            break;
    }
}
/* Functions */
function background(status){
    var background = $("#background_load");
    switch (status){
        case 1 : background.removeClass("hidden");
            break;
        case 2 : background.addClass("hidden");
            break;
    }
}
function modal(status){
    switch(status){
        case 1 : $("#success_message").modal("show");
            break;
        case 2 : $("#warning_message").modal("show");
            break;
        case 3 : $("#danger_message").modal("show");
            break;
    }
}


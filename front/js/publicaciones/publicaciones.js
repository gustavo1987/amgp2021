var categoria_2,autor_2,titulo_2;
$(document).ready(function() {
    

    let timerInterval
    Swal.fire({
    title: 'Cargando datos',
    html: 'Procesando',
    //timer: 2000,
    timerProgressBar: true,
    onBeforeOpen: () => {
        Swal.showLoading()
        timerInterval = setInterval(() => {
        const content = Swal.getContent()
        if (content) {
            const b = content.querySelector('b')
            if (b) {
            b.textContent = Swal.getTimerLeft()
            }
        }
        }, 100)
    },
    onClose: () => {
        clearInterval(timerInterval)
    }
    }).then((result) => {
    /* Read more about handling dismissals below */
    if (result.dismiss === Swal.DismissReason.timer) {
        console.log('I was closed by the timer')
    }
    })
    $.ajax("/categoriaspublicaciones",{
        type:'GET',     
    })  
    .done(function(response){


        var datos=response.data;
        var id=$("#contenedor-publicaciones").attr("data-id");
        var micategoria="";

        var categorias=`<option value="">Categorias</option>`;
        datos.forEach(function(item,index){
  
            categorias+=`
                <option value="${item.name}">${item.name}</option>
            `;
        });
        $("#filtro-categoria").html(categorias);
        if(window.location.pathname!="/publicaciones")
        {
            switch(id) {
                case "1":
                //micategoria="Libretos guías"
                crear_datatable_con_filtros("Libretos guías","","");
                break;
                case "2":
                    //micategoria="Boletines"
                    crear_datatable_con_filtros("Boletines","","");
                break;
                case "3":
                    //micategoria="Resumen de simposios"
                    crear_datatable_con_filtros("Resumen de simposios","","");
                break;
                case "4":
                    //micategoria="Gacetas"
                    crear_datatable_con_filtros("Gacetas","","");
                break;
                default:
                // code block
            }
        }
        else
        {
            crear_datatable_con_filtros(categoria,"","");
        }
        //Swal.close();
        //crear_datatable_con_filtros(categoria,"","");
    });
        let fecha_inicio=``;
        let fecha_final=``;

        var categoria=``;
        var autor=``;
        var titulo=``;
        //if(window.location.pathname=="/publicaciones")
        //{
           
            $('#bFiltrar').on("click",function(){
 
                if(window.location.pathname=="/publicaciones")
                {
                    categoria=$("#filtro-categoria").val();
                    autor=$("#filtro-autor").val();
                    titulo=$("#filtro-titulo").val();
                    
                    categoria_2=categoria;
                    //autor_2=autor;
                    //titulo_2=titulo;
                    crear_datatable_con_filtros(categoria,autor,titulo);
                }
                else
                {
                    autor_2=$("#filtro-autor").val();
                    titulo_2=$("#filtro-titulo").val();
                    var id=$("#contenedor-publicaciones").attr("data-id");
                   
                    switch(id) {
                        case "1":
                        //micategoria="Libretos guías"
                        crear_datatable_con_filtros("Libretos guías",autor_2,titulo_2);
                        break;
                        case "2":
                            //micategoria="Boletines"
                            crear_datatable_con_filtros("Boletines",autor_2,titulo_2);
                        break;
                        case "3":
                            //micategoria="Resumen de simposios"
                            crear_datatable_con_filtros("Resumen de simposios",autor_2,titulo_2);
                        break;
                        case "4":
                            //micategoria="Gacetas"
                            crear_datatable_con_filtros("Gacetas",autor_2,titulo_2);
                        break;
                        default:
                        // code block
                    }
                }
            });
        


    var factura_form = $("#paypublication");
    /*createDatePicker(1,factura_form,"date");*/
    if(factura_form.length>=1) {
        factura_form.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            locale: 'es_ES',
            fields: {}
        }).on('success.form.fv', function (e) {
            jQuery("#mail-c").removeClass("hide");
            jQuery("#background-contact").modal({backdrop: 'static', keyboard: false, show: true});
            var form = jQuery(this);
            var code;
            jQuery.ajax({
                type: "POST",
                data: jQuery(this).serialize(),
                url: "/registro/savepay",
                dataType: "json",
                success: function (response) {
                    code = response.code;
                    switch (code) {
                        case 200:
                            window.location.href = response.url;
                            break;
                        case 404:
                            jQuery("#dg-c").removeClass("hide").addClass("in");
                            jQuery("#error-contact").removeClass("hide");
                            break;
                    };
                },
                error: function () {

                },
                complete: function () {
                    setTimeout(function () {
                        switch (code) {
                            case 200:
                                resetForm(form);
                                break;
                            case 404:
                                jQuery("#dg-c").removeClass("in").addClass("hide");
                                break;
                        }
                        jQuery("#background-contact").modal("hide");
                        jQuery(".message-background").addClass("hide");
                        jQuery("#wm").removeClass("hide");
                    }, 3000);
                }
            });
            return false;
        });
    }

});
function crear_datatable_con_filtros(categoria,autor,titulo)
{

    $.ajax("/revisarsesion",{
        type:'GET',
    })  
    .done(function(response){
        var datos=response.message;

        if(datos[0]!="no sesion")
        {

            var descarga=datos[1];
            var login_active=datos[0];
            $("#i-descargas-disponibles").html("Descargas libres disponibles : "+datos[2])
        }

        var b=` <table id="tabla-publicaciones" class="table" style="width:100%">
                <thead>
                    <tr>
                        <th>Categoria3</th>
                        <th>Titulo</th>
                        <th>Autores</th>
                        <th>Fecha</th>
                        <th>Vol / Num</th>
                        <th>Costo</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot></tfoot></table>`;
        $("#contenedor-publicaciones").html(b);
                //var ids=[],total_pagar=0,precios=[],cantidades=[];
        var mitb_detalles= $('#tabla-publicaciones').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'pdf', 'print'
                        ],
                        searching:true,
                        processing: true,
//                        responsive: true,
                        ordering:  false,
                        pagingType: "numbers",
                        language: {
                            "decimal": "",
                            "emptyTable": "No hay información",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                            //"infoEmpty": "Mostrando 5600 de 0 de 0 Entradas",
                            "infoEmpty": "",
                            //"infoFiltered": "(Filtrado de _MAX_ total entradas)",
                            "infoFiltered": "",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "Mostrar _MENU_ Entradas",
                            "loadingRecords": "Cargando...",
                            "processing": "Procesando...",
                            "search": "Buscar:",
                            "zeroRecords": "Sin resultados encontrados",
                            "paginate": {
                                "first": "Primero",
                                "last": "Ultimo",
                                "next": "Siguiente",
                                "previous": "Anterior"
                            },
                            "search": "",
                            "searchPlaceholder": "Buscar publicación",
                            "sLengthMenu": "_MENU_items"
                        },
                        //serverSide: true,
                        //ajax:`/consulta?fecha_inicio=${fecha_inicio}&fecha_final=${fecha_final}`,
                        ajax:`/consulta?categoria=${categoria}&autor=${autor}&titulo=${titulo}`,
                        columnDefs:[
                            {"targets":0, "data":function(data,type,full,meta)
                                {
                                    return data.categoria;
                                }
                            },
                            {"targets":1, "data":function(data,type,full,meta)
                                {
                                    return data.name;
                                }
                            },
                            {"targets":2, "data":function(data,type,full,meta)
                                {
                                    return data.autor;
                                }
                            },
                            {"targets":3, "data":function(data,type,full,meta)
                                {
                                    var a=data.date_creation;
                                    //total_pagar=total_pagar+parseFloat(data); 
                                    //precios.push(data);
                                    return `${a}`;
                                }
                            },
                            {"targets":4, "data":function(data,type,full,meta)
                                {
                                    return data.vol+' / '+data.num;
                                }
                            },
                            {"targets":5, "data":function(data,type,full,meta)
                                {
                                    
                                    //total_pagar=total_pagar+parseFloat(data);   
                                    var a,b,c;
                                    if(data.free==0)
                                    {
                                        if(data.price!=null)
                                        {
                                            a=`$ ${(parseFloat(data.price).toFixed(2))}`;
                                        }
                                        else
                                        {
                                            a=`$ 0`;
                                        }
                                    }
                                    else
                                    {
                                        a="Gratis";
                                    }
                                    if(data.free==0)
                                    {
                                        if(data.priceasociados!=null)
                                        {
                                            b=`$ ${(parseFloat(data.priceasociados).toFixed(2))}`;
                                        }
                                        else
                                        {
                                            b=`$ 0`;
                                        }
                                    }
                                    else
                                    {
                                        b="Gratis";
                                    }
                                    if(data.free==0)
                                    {
                                        if(data.priceestudiante!=null)
                                        {
                                            c=`$ ${(parseFloat(data.priceestudiante).toFixed(2))}`;
                                        }
                                        else
                                        {
                                            c=`$ 0`;   
                                        }
                                    }
                                    else
                                    {
                                        c="Gratis";
                                    }

                                    var d=`
                                        <p>U: ${a}</p>
                                        <p>AP: ${b}</p>
                                        <p>AE: ${c}</p>
                                    `;                                  
                                    return d;                                    
                                }
                            },
                            {"targets":6, "data":function(data,type,full,meta)
                                {
                                    
                                    var parametros={descarga:descarga,sesion:login_active,free:data.free}
                                    var opciones=`
                                        <a href="/api/administration/publicaciones/portada/${data.portada}" 
                                        class="btn btn-default btn-rounded btn-xs" 
                                        target="_black"><span class="fa fa-eye"></span></a>`;
                                        if(parametros.sesion=="si")
                                        {
                                            if(parametros.descarga=="descarga dis")
                                            {                                            
                                                if(parametros.free==1)
                                                {

                                                    opciones+=`<button id="descarga_${data.publid}>"
                                                    data-free="${parametros.free}" 
                                                    data-id="${data.publid}>" 
                                                    data-name="${data.name}>" 
                                                    type="button" 
                                                    class="delete_value_table btn btn-info btn-xs" 
                                                    rel="tooltip" data-color-class="danger" 
                                                    data-animate="animated fadeIn" 
                                                    data-toggle="tooltip" 
                                                    data-original-title="Eliminar posición" 
                                                    data-placement="top"
                                                    onclick='downloadFile("${data.publid}")'><i class="fas fa-cloud-download-alt"></i></button>`;
                                                }
                                                else
                                                {
                                                    opciones+=`<button id="descarga_${data.publid}>" 
                                                    data-id="${data.publid}>" 
                                                    data-name="${data.name}>" 
                                                    type="button" 
                                                    class="delete_value_table btn btn-info btn-xs" 
                                                    rel="tooltip" data-color-class="danger" 
                                                    data-animate="animated fadeIn" 
                                                    data-toggle="tooltip" 
                                                    data-original-title="Eliminar posición" 
                                                    data-placement="top"
                                                    onclick='downloadFile("${data.publid}")'><i class="fas fa-cloud-download-alt"></i></button>
                                                    
                                                    <button id="carro_${data.publid}" class="btn btn-success btn-rounded btn-xs"
                                                    onclick='carrito("${data.publid}")'><span class="fa fa-shopping-cart"></span></button>
                                                    `;
                                                }
                                            }
                                            else
                                            {
                                                if(parametros.free==1)
                                                {

                                                    opciones+=`<button id="descarga_${data.publid}>"
                                                    data-free="${parametros.free}" 
                                                    data-id="${data.publid}>" 
                                                    data-name="${data.name}>" 
                                                    type="button" 
                                                    class="delete_value_table btn btn-info btn-xs" 
                                                    rel="tooltip" data-color-class="danger" 
                                                    data-animate="animated fadeIn" 
                                                    data-toggle="tooltip" 
                                                    data-original-title="Eliminar posición" 
                                                    data-placement="top"
                                                    onclick='downloadFile("${data.publid}")'><i class="fas fa-cloud-download-alt"></i></button>`;
                                                }
                                                else
                                                {
                                                    opciones+=`
                                                    <button id="carro_${data.publid}" class="btn btn-success btn-rounded btn-xs"
                                                    onclick='carrito("${data.publid}")'><span class="fa fa-shopping-cart"></span></button>
                                                    `;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if(parametros.free==1)
                                                {

                                                    opciones+=`<button id="descarga_${data.publid}>"
                                                    data-free="${parametros.free}" 
                                                    data-id="${data.publid}>" 
                                                    data-name="${data.name}>" 
                                                    type="button" 
                                                    class="delete_value_table btn btn-info btn-xs" 
                                                    rel="tooltip" data-color-class="danger" 
                                                    data-animate="animated fadeIn" 
                                                    data-toggle="tooltip" 
                                                    data-original-title="Eliminar posición" 
                                                    data-placement="top"
                                                    onclick='openmodal()'><i class="fas fa-cloud-download-alt"></i></button>`;
                                                }
                                                else
                                                {
                                                    opciones+=`<button id="descarga_${data.publid}>"
                                                    data-free="${parametros.free}" 
                                                    data-id="${data.publid}>" 
                                                    data-name="${data.name}>" 
                                                    type="button" 
                                                    class="delete_value_table btn btn-info btn-xs" 
                                                    rel="tooltip" data-color-class="danger" 
                                                    data-animate="animated fadeIn" 
                                                    data-toggle="tooltip" 
                                                    data-original-title="Eliminar posición" 
                                                    data-placement="top"
                                                    onclick='openmodal()'><i class="fas fa-cloud-download-alt"></i></button>
                                                    
                                                    <button id="carro_${data.publid}" class="btn btn-success btn-rounded btn-xs"
                                                    onclick='openmodal()'><span class="fa fa-shopping-cart"></span></button>
                                                    `;
                                                }
                                        }
                                        /*if(descarga=="descarga dis")
                                        {
                                            if(login_active=="si")
                                            {
                                                //opciones+=`onclick='downloadFile("${data.publid}")'><i class="fas fa-cloud-download-alt"></i></button>`;
                                                if(data.free==0)
                                                {
                                                    opciones+=`
                                                        <button id="carro_${data.publid}" class="btn btn-success btn-rounded btn-xs"`;
                                                    if(login_active=="si")
                                                    {
                                                        opciones+=`onclick='carrito("${data.publid}")'><span class="fa fa-shopping-cart"></span></button>`;
                                                                    
                                                    }     
                                                    else
                                                    {
                                                        opciones+=`onclick='openmodal()'><span class="fa fa-shopping-cart"></span></button>`;
                                                    }
                                                }
                                                else
                                                {
                                                    opciones+=`<button id="descarga_${data.publid}>" 
                                                data-id="${data.publid}>" 
                                                data-name="${data.name}>" 
                                                type="button" 
                                                class="delete_value_table btn btn-info btn-xs" 
                                                rel="tooltip" data-color-class="danger" 
                                                data-animate="animated fadeIn" 
                                                data-toggle="tooltip" 
                                                data-original-title="Eliminar posición" 
                                                data-placement="top"`;
                                                    opciones+=`onclick='downloadFile("${data.publid}")'><i class="fas fa-cloud-download-alt"></i></button>`;
                                                }
                                            }     
                                            else
                                            {
                                                opciones+=`onclick='openmodal()'><i class="fas fa-cloud-download-alt"></i></button>`;
                                            }
                                        }
                                        else
                                        {
                                            if(data.free==0)
                                            {
                                                opciones+=`
                                                    <button id="carro_${data.publid}" class="btn btn-success btn-rounded btn-xs"`;
                                                if(login_active=="si")
                                                {
                                                    opciones+=`onclick='carrito("${data.publid}")'><span class="fa fa-shopping-cart"></span></button>`;
                                                                
                                                }     
                                                else
                                                {
                                                    opciones+=`onclick='openmodal()'><span class="fa fa-shopping-cart"></span></button>`;
                                                }
                                            }
                                        }*/

                                        /*if(data.free==0)
                                        {
                                            opciones+=`
                                                <button id="carro_${data.publid}" class="btn btn-success btn-rounded btn-xs"`;
                                            if(login_active=="si")
                                            {
                                                opciones+=`onclick='carrito("${data.publid}")'><span class="fa fa-shopping-cart"></span></button>`;
                                                            
                                            }     
                                            else
                                            {
                                                opciones+=`onclick='openmodal()'><span class="fa fa-shopping-cart"></span></button>`;
                                            }
                                        }*/
                                // <?=$web?"onclick='downloadFile(".$publis->getPublid().")'":"onclick='openmodal()'"?>><i class="fas fa-cloud-download-alt"></i></button>                      
                                    return opciones;                                  
                                }
                            }
                        ],
                        "drawCallback": function( settings ) {
                            Swal.close();
                            //eventos_tabla_estudios();
                        }
        }); 
    });
}
function resetForm($form) {
    $form.find('input:text, input:password, input:file, input[type=email], select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}

var articulosCarro = [];
var match = document.cookie.match(new RegExp('(^| )' + "carrito" + '=([^;]+)'));
if (match){
    var arreglo =match[2];
    var json = JSON.parse(decodeURIComponent(arreglo));
    for(var i=0;i<json.length;i++){
        articulosCarro.push(json[i]["publid"]);
    }
}
function carrito($id){
    articulosCarro.push($id);
    $('#carro_'+$id).attr("disabled","true");
    $.ajax({
        url:"/agregarcarrito",
        type:"post",
        data:{"public[]":articulosCarro},
        dataType:"json",
        success:function(resp){
          $('#cantidadCarrito').html(" "+resp.cant);
          swal("Se a agregado este producto al carrito de compra");
        }
    })
}
function downloadFile($file){
  var id = {};
  var name = {};
  var type = {};
  id = $('#descarga_'+$file).attr('data-id');
  name = $('#descarga_'+$file).attr('data-name');
  type = $('#descarga_'+$file).attr('data-type');
  $("#delete_modal").modal("show");
  var dispobibles = $("#descargas-usuario").val();
  //alert(dispobibles);
  $("#delete_modal").on('shown.bs.modal', function (e) {
      $(".value_name_delete").text(dispobibles);
  });
  $("#delete_modal").on('hidden.bs.modal', function (e) {
      id = {};
      name_value = {};
      $(".value_name_delete").text(null);
  });
  $("#confirm_delete").off("click");
  $("#confirm_delete").click(function(){
      $.ajax({
          type : "POST",
          url : "/publicaciones/download",
          data : {value:id,type:type},
          dataType : "json",
          success : function(response){
   

              var id=$("#contenedor-publicaciones").attr("data-id");
              if(window.location.pathname!="/publicaciones")
              {

                  //autor=$("#filtro-autor").val();
                  //titulo=$("#filtro-titulo").val();
                  autor_2=$("#filtro-autor").val();
                  titulo_2=$("#filtro-titulo").val();
                  switch(id) {
                      case "1":
                      //micategoria="Libretos guías"
                      crear_datatable_con_filtros("Libretos guías",autor_2,titulo_2);
                      break;
                      case "2":
                          //micategoria="Boletines"
                          crear_datatable_con_filtros("Boletines",autor_2,titulo_2);
                      break;
                      case "3":
                          //micategoria="Resumen de simposios"
                          crear_datatable_con_filtros("Resumen de simposios",autor_2,titulo_2);
                      break;
                      case "4":
                          //micategoria="Gacetas"
                          crear_datatable_con_filtros("Gacetas",autor_2,titulo_2);
                      break;
                      default:
                      // code block
                  }
              }
              else
              {
                  crear_datatable_con_filtros("","","");
              }
              //var n1 = document.getElementById('descargas-usuario').val();
              var n1=$("#descargas-usuario").val();
              var suma = parseInt(n1) + 1;
              $("#descargas-usuario").val(suma);
              $("#delete_modal").modal("hide");
              switch (response.code){
                  case 200:
                      window.open(response.liga, '_blank');
                      //swal("LLeva #"+response.descargas+" descargas este año, tu pdf esta encriptado la contraseña para abrirlo es tu correo");
                      swal("LLeva #"+response.descargas+" descargas este año");
                      break;
                  case 300:
                      window.location.href=response.liga;
                      break;
                  default :
              }
              
          },
          error : function(){
              $("#delete_modal").modal("hide");
          }
      });
  });

}
function openmodal(){
  jQuery("#iniciosesionmodal").modal("show");
}

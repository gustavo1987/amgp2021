$(document).ready(function(){
    var zero=0,one=0,two=0,three=0,four=0,five= 0,six=0,seven = 0;
    if($("#formAfiliarse").length>=1){
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            number = $(this).data("number");
            if(one==0 && number==1){
                var form_images = $('#formDirectionAfiliarse');
                form_images.formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    // This option will not ignore invisible fields which belong to inactive panels
                    excluded: ':disabled',
                    fields: {
                        'prid[]': {
                            validators: {
                                notEmpty: {
                                    message: 'Este campo es necesario y no puede estar vacío.'
                                }
                            }
                        },'imgid[]': {
                            validators: {
                                notEmpty: {
                                    message: 'Este campo es necesario y no puede estar vacío.'
                                }
                            }
                        },'addid[]': {
                            validators: {
                                notEmpty: {
                                    message: 'Este campo es necesario y no puede estar vacío.'
                                }
                            }
                        }
                    }
                }).on('success.form.fv', function(e) {
                    e.preventDefault();
                    backgroundActive(1);button(1);
                    values = {type:"images","comid":comid};
                    data = $(this).serialize()+"&"+jQuery.param(values);
                    $.ajax({
                        type : "POST",
                        url : "/panel/company/update",
                        data : data,
                        dataType : "json",
                        success : function(response){
                            if(response.message=="SUCCESS" && response.code==200) {
                                messages(1);
                            }else{
                                messages(2);
                            }
                            backgroundActive(2);
                            button(2);
                        },
                        error : function(){
                            backgroundActive(2);button(2);
                        }
                    });
                });
                one++;
            }
        }),
        initSelectM();
        (function($){
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                today: "Hoy",
                clear: "Borrar",
                weekStart: 1,
                format: "dd/mm/yyyy"
            };
        }(jQuery));
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language : "es",
            autoclose:true
        })
            .on('changeDate', function(e) {
                // Revalidate the date field
                $('#formAfiliarse').formValidation('revalidateField', 'dateP');
            });
        $('#formAfiliarse').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El nombre es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 2,
                            max: 100,
                            message: 'El titulo debe ser mayor de 2 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                lastname: {
                    validators: {
                        notEmpty: {
                            message: 'El apellido paterno es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 2,
                            max: 45,
                            message: 'El apellido paterno  debe ser mayor de 2 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                secondname: {
                    validators: {
                        notEmpty: {
                            message: 'El apellido materno es necesario y no puede estar vacío.'
                        },
                        stringLength: {
                            min: 2,
                            max: 45,
                            message: 'El apellido materno  debe ser mayor de 2 y menor de 100 caracteres de longitud.'
                        }
                    }
                },
                sexo: {
                    validators: {
                        notEmpty: {
                            message: 'El sexo es campo necesario y no puede estar vacío.'
                        }
                    }
                },
                delegacion: {
                    validators: {
                        notEmpty: {
                            message: 'El status no puede estar vacio.'
                        }
                    }
                },
                annoasociado: {
                    validators: {
                        notEmpty: {
                            message: 'El año de asociación no puede estar vacio.'
                        }
                    }
                },
                fecha: {
                    validators: {
                        notEmpty: {
                            message: 'La fecha no puede estar vacio.'
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: 'Su numero de celular es necesario y no puede estar vacío.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Es necesaria la dirección de correo electrónico y esta no puede estar vacía'
                        },
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/panel/user/validateemail',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            button(1);
            values = {type:"principal","clid":clid,"coutid":coutid};
            data = $(this).serialize()+"&"+jQuery.param(values);
            $.ajax({
                type : "POST",
                url : "/index/saveafiliacion",
                data : data,
                dataType : "json",
                success : function(response){
                    if(response.message=="SUCCESS" && response.code==200){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-success").toggleClass("open");
                        $('#step2').tab('show');
                    }else if(response.code==404){
                        $("#message-box-info").removeClass("open");
                        $("#message-box-warning").toggleClass("open");
                    }
                },
                error : function(){
                    $("#message-box-info").removeClass("open");
                }
            });
        });
    }
    if($("#dropzone-user-edit").length>=1){
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone('#dropzone-user-edit', {
            url: "/panel/news/uploadimage",
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            maxFiles: 2,
            addRemoveLinks : true,
            dictResponseError: "No se puede subir esta imagen!",
            autoProcessQueue: true,
            thumbnailWidth: 138,
            thumbnailHeight: 120,
            previewTemplate: '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-filename"><span data-dz-name style:"font-weight: bold;"></span></div><div class="dz-size">File size: <span data-dz-size></span></div><div class="dz-thumbnail-wrapper"><div class="dz-thumbnail"><img data-dz-thumbnail><span class="dz-nopreview">No preview</span><div class="dz-success-mark"><i class="fa fa-check-circle-o"></i></div><div class="dz-error-mark"><i class="fa fa-times-circle-o"></i></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div></div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" data-dz-uploadprogress></div></div></div>',

            resize: function(file) {
                var info = { srcX: 0, srcY: 0, srcWidth: file.width, srcHeight: file.height },
                    srcRatio = file.width / file.height;
                if (file.height > this.options.thumbnailHeight || file.width > this.options.thumbnailWidth) {
                    info.trgHeight = this.options.thumbnailHeight;
                    info.trgWidth = info.trgHeight * srcRatio;
                    if (info.trgWidth > this.options.thumbnailWidth) {
                        info.trgWidth = this.options.thumbnailWidth;
                        info.trgHeight = info.trgWidth / srcRatio;
                    }
                } else {
                    info.trgHeight = file.height;
                    info.trgWidth = file.width;
                }
                return info;
            },
            success: function(file, response){
                var name_image = response;
                $(".dz-preview").addClass("dz-success");
                $("div.progress").remove();
                $("#user-image").val(name_image.name);
            },
            removedfile: function(file) {
                $(file.previewElement).remove();
            }
        });
        var mockFile = { name: "Click en remove file", size: 12345};
        myDropzone.emit("addedfile", mockFile);
        $(".progress.progress-striped.active").addClass("hide");
        if($("#user-image").val()==""){
            image_load = "/dash/assets/images/users/thumbnail/no-image.jpg";
        }else{
            image_load = "/dash/assets/images/users/thumbnail/"+$("#user-image").val();
        }
        myDropzone.emit("thumbnail", mockFile,image_load);
        var existingFileCount = 1; // The number of files already uploaded
        myDropzone.options.maxFiles = myDropzone.options.maxFiles - existingFileCount;
    }
});


/* Functions */
function initSelectM(){
    $(".selectM").select2({});
}
function initSelectU(){
    $(".selectU").select2({
        maximumSelectionLength:1
    });
}

$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        if($('#formAfiliarse').formValidation()){};
        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}


//according menu

$(document).ready(function()
{
    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');

    //Set The Accordion Content Width
    var contentwidth = $('.accordion-header').width();
    $('.accordion-content').css({});

    //Open The First Accordion Section When Page Loads
    $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
    $('.accordion-content').first().slideDown().toggleClass('open-content');

    // The Accordion Effect
    $('.accordion-header').click(function () {
        if($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }

        else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
    });

    return false;
});
function button(status){
    var btn = $(".btn-submit");
    var btn_message_principal = btn.data("value");
    var btn_message = btn.data("message");
    var loading_fa = $("#loading_fa");
    switch (status){
        case 1 :  btn.attr("disabled","disabled").val(btn_message);
            loading_fa.removeClass("hidden");
            break;
        case 2 :  btn.removeAttr("disabled").removeClass("disabled").val(btn_message_principal);
            loading_fa.addClass("hidden");
            break;
    }
}

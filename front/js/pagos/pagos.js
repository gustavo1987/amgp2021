jQuery(document).ready( function() {

    /* */
    $(".disable-ayuda").on('change',function(){
        /*console.log($("select[name=cuota]").val());
        console.log($("select[name=year]").val());*/
        if($("select[name=cuota]").val()!=='Seleccione el tipo de asociado' && $("select[name=year]").val()!=='Año'){
            var disabled = $(this).val() == 'true' ? true : false;
            $('input[name=ayudamutua]').attr('disabled', disabled);
            $('input[name=anualidad]').attr('disabled', disabled);
            $('input[name=payment]').attr('disabled', disabled);
        }
    });

    var table = $("table#table_cost tbody");
    $("select[name=cuota]").on("change",function() {
        $("input[name=anualidad]").prop('checked', false);
        $("input[name=ayudamutua]").prop('checked', false);
        $("input[name=payment]").prop('checked', false);
        var cuota = $("select[name=cuota] option:selected").attr("data-type");
            jQuery.ajax({
                type : "POST",
                data : {id:cuota},
                url : "/pagos/paymentdescription",
                dataType : "json",
                success : function(response){
                    code = response.code;
                    namecuota = response.cuotaname;
                    id= response.cuotaid;

                    if(response.code==200){
                        table.html("");
                        $(".ayudamutua").prop( "checked", false );
                        $("#buy").val(id)
                    }else if(response.code==404){
                        alertMessage("warning","Ha ocurrido un error, intente nuevamnete por favor");
                    }
                },
                error : function(){
                    console.log("error11");
                }
            });
    });
    $("select[name=year]").on("change",function() {
        $("input[name=anualidad]").prop('checked', false);
        $("input[name=ayudamutua]").prop('checked', false);
        $("input[name=payment]").prop('checked', false);
        table.html("");
        $(".ayudamutua").prop( "checked", false );
        var cuota = $("select[name=year] option:selected").attr("data-type");
            jQuery.ajax({
                type : "POST",
                data : {id:cuota},
                url : "/pagos/paymentdescription",
                dataType : "json",
                success : function(response){
                    code = response.code;
                    namecuota = response.cuotaname;
                    id= response.cuotaid;

                    if(response.code==200){
                        table.html("");
                        $(".ayudamutua").prop( "checked", false );
                        $("#buy").val(id)
                    }else if(response.code==404){
                        alertMessage("warning","Ha ocurrido un error, intente nuevamnete por favor");
                    }
                },
                error : function(){
                    console.log("error11");
                }
            });
    });

    $("select[name=cuota]").on("change",function() {

                    table.html("");
                    $(".ayudamutua").prop( "checked", false );

    });



    /* Vista de mensualidades */
    $("input[name=payment]").click(function(){

        var cuota = $("select[name=cuota] option:selected").attr("data-type");
        var anonacimiento = $("select[name=year] option:selected").val();
        var ayudaM = $("input[name=ayudamutua]:checked").val();
        var fecha = new Date();
        var ano = fecha.getFullYear();
        var edad = ano-anonacimiento;
        var costoayuda=0;
        var comisionPaypal = 0;
        var subsidioAMGP = 0;
        var subtotal2 = 0;
        var meses = $(this).val();
        var mespagotabla = "";
        var f = new Date();

        /* Rango de edades */
        var rango = "";
        if(edad>30&&edad<35){
            if((f.getMonth() +1)==12){
                costoayuda=7865;
            }
            else if ((f.getMonth() +1)==1){
                costoayuda=7940;
            }
            else if((f.getMonth() +1)>=2){
                costoayuda=8015;
            }

            rango="30-34 años";
        }
        else if(edad<30){
            if((f.getMonth() +1)==12){
                costoayuda=5670;
                mespagotabla="Diciembre "+f.getFullYear();
            }
            else if ((f.getMonth() +1)==1){
                costoayuda=5724;
                mespagotabla="Enero "+f.getFullYear();
            }
            else if((f.getMonth() +1)>=2){
                costoayuda=5788;
                mespagotabla="Febrero "+f.getFullYear();
            }
            rango="Menor de 30 años";
            
        }
        else if(edad>34){
            if((f.getMonth() +1)==12){
                costoayuda=10080;
                mespagotabla="Diciembre "+f.getFullYear();
            }
            else if ((f.getMonth() +1)==1){
                costoayuda=10176;
                mespagotabla="Enero "+f.getFullYear();
            }
            else if((f.getMonth() +1)>=2){
                costoayuda=10272;
                mespagotabla="Febrero "+f.getFullYear();
            }
            rango="Mayor de 35 años";
        }
        /* Fin rango de edades */
        var value = $(this).val();

        if(value=="SI"){

            $.ajax({
                type : "POST",
                /*data : {id:cuota, value:ayudaM, mes:value},*/
                data : {id:cuota, value:value, mes:value},
                url : "/pagos/paymentdescriptionayuda",
                dataType : "json",
                success : function(response){
                    code = response.code;
                    namecuota = response.cuotaname;
                    id= parseInt(response.cuotaid);
                    ayuda = parseInt(response.costayuda);

                    if(response.code==200){
                        if(($("#ayudamutua").is(':checked')==true) && ($("#anualidad").is(':checked')==true)){
                            costo = id+costoayuda;
                            comisionPaypal = (((costo*0.034)+4)*1.16);
                            subsidioAMGP = (comisionPaypal/2)*(-1);
                            subtotal2 = costo+comisionPaypal;
                            pitarsubtotalcomision= subtotal2+subsidioAMGP
                            comisionmeses = ((subtotal2*0.0455)*1.16);
                            total = subtotal2+comisionmeses+subsidioAMGP;
                            mensualidades = total/3;
                            $("#buy").val(total);
                            $("#tresmeses").val(1);
                            table.html("<tr><td><span class='title-left'>   Cuotas Anualidad 2018; Asociado "+namecuota+" </span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango "+rango+"; Pago en "+mespagotabla+" </span></td><th class='mont'>$ "+number_format(costoayuda,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ "+number_format(costo,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ "+number_format(pitarsubtotalcomision,"2",".",",")+"</th></tr>"+

                                "<tr><th class='mensualidades'><span class='title-center'>Pago a 3 Meses</th><th class='mensualidades'></th></tr>"+
                                "<tr><td><span class='title-left'> Comisión PayPal por pago a 3 meses </span></td><th class='mont'>$ "+number_format(comisionmeses,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'> Total </span></td><th class='mont'>$ "+number_format(total,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'> 3 mensualidades de </span></td><th class='mont'>$ "+number_format(mensualidades,"2",".",",")+"</th></tr>"
                            );
                        }
                        else if(($("#ayudamutua").is(':checked')==false)&&($("#anualidad").is(':checked')==true)){
                            comisionPaypal = (((id*0.034)+4)*1.16);
                            subsidioAMGP = (comisionPaypal/2)*(-1);
                            subtotal2= id+comisionPaypal+subsidioAMGP;
                            pitarsubtotalcomision= subtotal2+subsidioAMGP
                            comisionmeses = ((subtotal2*0.0455)*1.16);
                            total = subtotal2+comisionmeses+subsidioAMGP;
                            mensualidades = total/3;
                            $("#buy").val(total);
                            $("#tresmeses").val(1);
                            table.html("<tr><td> <span class='title-left'>  Cuotas Anualidad 2018; Asociado "+namecuota+"</span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'>   Subtotal 1 </span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-left'>   Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-left'>   Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'>   Total </span></td><th class='mont'>$ "+number_format(pitarsubtotalcomision,"2",".",",")+"</th></tr>"+

                                "<tr><th class='mensualidades'><span class='title-center'>Pago a 3 Meses</th><th class='mensualidades'></th></tr>"+
                                "<tr><td><span class='title-left'> Comisión PayPal por pago a 3 meses </span></td><th class='mont'>$ "+number_format(comisionmeses,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'> Total </span></td><th class='mont'>$ "+number_format(total,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'> 3 mensualidades de </span></td><th class='mont'>$ "+number_format(mensualidades,"2",".",",")+"</th></tr>"
                            );
                        }
                        else if(($("#ayudamutua").is(':checked')==false)&&($("#anualidad").is(':checked')==false)){
                            console.log("4");
                            table.html("");
                        }
                    }else if(response.code==404){
                        alertMessage("warning","Ha ocurrido un error, intente nuevamnete por favor");
                    }
                },
                error : function(){
                    console.log("error11");
                }
            });
        }


        else {
            console.log("6");

            jQuery.ajax({
                type: "POST",
                /*data: {id:cuota, value:ayudaM, mes:value},*/
                data: {id:cuota, value:value, mes:value},
                url: "/pagos/paymentdescriptionayuda",
                dataType: "json",
                success: function (response) {
                    code = response.code;
                    namecuota = response.cuotaname;
                    id = parseInt(response.cuotaid);
                    ayuda = parseInt(response.costayuda);

                    if (response.code == 200) {
                        if (($("#ayudamutua").is(':checked') == true) && $("#anualidad").is(':checked') == true) {
                            costo = id + costoayuda;
                            comisionPaypal = (((costo * 0.034) + 4) * 1.16);
                            subsidioAMGP = (comisionPaypal / 2) * (-1);
                            subtotal2 = costo + comisionPaypal + subsidioAMGP;
                            $("#buy").val(subtotal2);
                            $("#tresmeses").val(0);
                            table.html("<tr><td><span class='title-left'>   Cuotas Anualidad 2018; Asociado " + namecuota + " </span></td><th class='mont'>$ " + number_format(id, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango " + rango + "; Pago en "+mespagotabla+" </span></td><th class='mont'>$ " + number_format(costoayuda, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ " + number_format(costo, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ " + number_format(comisionPaypal, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ " + number_format(subsidioAMGP, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ " + number_format(subtotal2, "2", ".", ",") + "</th></tr>"
                            );
                        }
                        else if (($("#ayudamutua").is(':checked') == false) && $("#anualidad").is(':checked') == true) {
                            console.log("7");
                            costo = id;
                            comisionPaypal = (((costo * 0.034) + 4) * 1.16);
                            subsidioAMGP = (comisionPaypal / 2) * (-1);
                            subtotal2 = costo + comisionPaypal + subsidioAMGP;
                            $("#buy").val(subtotal2);
                            $("#tresmeses").val(0);
                            table.html("<tr><td><span class='title-left'>  Cuotas Anualidad 2018; Asociado " + namecuota + " </span></td><th>$ " + number_format(id, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-right'> Subtotal 1 </span></td><th class='mont'>$ " + number_format(costo, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-left'> Comisiones por pago con PayPal</td><th class='mont'>$ " + number_format(comisionPaypal, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-left'> Subsidio AMGP pago PayPal(50%)</td><th class='mont'>$ " + number_format(subsidioAMGP, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-right'> Total </span></td><th class='mont'>$ " + number_format(subtotal2, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-right'> Total </span></td><th class='mont'>$ " + number_format(subtotal2, "2", ".", ",") + "</th></tr>"
                            );
                        }
                        else if (($("#ayudamutua").is(':checked') == false) && ($("#anualidad").is(':checked') == false)) {
                            console.log("8");
                            table.html("");
                        }
                    }
                },
                error: function () {
                    console.log("error11");
                }
            });
        }

    });



    /* *** Cuando le dan click al check box de "¿Qué deseo pagar? click en anualidad **** */
    $("input[name=anualidad]").click(function(){
        var cuota = $("select[name=cuota] option:selected").attr("data-type");
        var anonacimiento = $("select[name=year] option:selected").val();
        var fecha = new Date();
        var ano = fecha.getFullYear();
        var edad = ano-anonacimiento;
        var costoayuda=0;
        var comisionPaypal = 0;
        var subsidioAMGP = 0;
        var subtotal2 = 0;
        var meses = $('input[name=payment]:checked').val();
        var mespagotabla = "";
        var f = new Date();

        /* Rango de edades */
        var rango = "";
        if(edad>30&&edad<35){
            if((f.getMonth() +1)==12){
                costoayuda=7865;
                mespagotabla="Diciembre "+f.getFullYear();
            }
            else if ((f.getMonth() +1)==1){
                costoayuda=7940;
                mespagotabla="Enero "+f.getFullYear();
            }
            else if((f.getMonth() +1)>=2){
                costoayuda=8015;
                mespagotabla="Febrero "+f.getFullYear();
            }

            rango="30-34 años";
        }
        else if(edad<30){
            if((f.getMonth() +1)==12){
                costoayuda=5670;
                mespagotabla="Diciembre "+f.getFullYear();
            }
            else if ((f.getMonth() +1)==1){
                costoayuda=5724;
                mespagotabla="Enero "+f.getFullYear();
            }
            else if((f.getMonth() +1)>=2){
                costoayuda=5788;
                mespagotabla="Febrero "+f.getFullYear();
            }
            rango="Menor de 30 años";
        }
        else if(edad>34){
            if((f.getMonth() +1)==12){
                costoayuda=10080;
                mespagotabla="Diciembre "+f.getFullYear();
            }
            else if ((f.getMonth() +1)==1){
                costoayuda=10176;
                mespagotabla="Enero "+f.getFullYear();
            }
            else if((f.getMonth() +1)>=2){
                costoayuda=10272;
                mespagotabla="Febrero "+f.getFullYear();
            }
            rango="Mayor de 35 años";
        }
        /* Fin rango de edades */
        var value = $(this).val();
        if( $(this).is(':checked') ){
            jQuery.ajax({
                type : "POST",
                data : {id:cuota, value:value},
                url : "/pagos/paymentdescriptionayuda",
                dataType : "json",
                success : function(response){
                    code = response.code;
                    namecuota = response.cuotaname;
                    id= parseInt(response.cuotaid);
                    ayuda = parseInt(response.costayuda);

                    if(response.code==200){
                        if($("#ayudamutua").is(':checked')==true){
                            if(meses=="SI"){
                                costo = id+costoayuda;
                                comisionPaypal = (((costo*0.034)+4)*1.16);
                                subsidioAMGP = (comisionPaypal/2)*(-1);
                                subtotal2 = costo+comisionPaypal+subsidioAMGP;



                                pitarsubtotalcomision= subtotal2+subsidioAMGP
                                comisionmeses = ((subtotal2*0.0455)*1.16);
                                total = subtotal2+comisionmeses+subsidioAMGP;
                                mensualidades = total/3;



                                $("#buy").val(subtotal2);
                                $("#tresmeses").val(1);
                                table.html("<tr><td><span class='title-left'>   Cuotas Anualidad 2018; Asociado "+namecuota+" </span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango "+rango+"; Pago en "+mespagotabla+" </span></td><th class='mont'>$ "+number_format(costoayuda,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ "+number_format(costo,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ "+number_format(pitarsubtotalcomision,"2",".",",")+"</th></tr>"+

                                    "<tr><th class='mensualidades'><span class='title-center'>Pago a 3 Meses</th><th class='mensualidades'></th></tr>"+
                                    "<tr><td><span class='title-left'> Comisión PayPal por pago a 3 meses </span></td><th>$ "+number_format(comisionmeses,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> Total </span></td><th>$ "+number_format(total,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> 3 mensualidades de </span></td><th>$ "+number_format(mensualidades,"2",".",",")+"</th></tr>"
                                );
                            }
                            else if(typeof meses==("NO"||'undefined')){
                                costo = id+costoayuda;
                                comisionPaypal = (((costo*0.034)+4)*1.16);
                                subsidioAMGP = (comisionPaypal/2)*(-1);
                                subtotal2 = costo+comisionPaypal+subsidioAMGP;
                                $("#buy").val(subtotal2);
                                $("#tresmeses").val(0);
                                table.html("<tr><td><span class='title-left'>   Cuotas Anualidad 2018; Asociado "+namecuota+" </span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango "+rango+"; Pago en "+mespagotabla+" </span></td><th class='mont'>$ "+number_format(costoayuda,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ "+number_format(costo,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ "+number_format(subtotal2,"2",".",",")+"</th></tr>"
                                );
                            }
                            else{
                                costo = id+costoayuda;
                                comisionPaypal = (((costo*0.034)+4)*1.16);
                                subsidioAMGP = (comisionPaypal/2)*(-1);
                                subtotal2 = costo+comisionPaypal+subsidioAMGP;
                                $("#buy").val(subtotal2);
                                $("#tresmeses").val(0);
                                table.html("<tr><td><span class='title-left'>   Cuotas Anualidad 2018; Asociado "+namecuota+" </span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango "+rango+"; Pago en "+mespagotabla+" </span></td><th class='mont'>$ "+number_format(costoayuda,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ "+number_format(costo,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ "+number_format(subtotal2,"2",".",",")+"</th></tr>"
                                );
                            }

                        }
                        else if($("#ayudamutua").is(':checked')==false){
                            if(meses=="SI"){
                                comisionPaypal = (((id*0.034)+4)*1.16);
                                subsidioAMGP = (comisionPaypal/2)*(-1);
                                subtotal2= id+comisionPaypal+subsidioAMGP;
                                pitarsubtotalcomision= subtotal2+subsidioAMGP
                                comisionmeses = ((subtotal2*0.0455)*1.16);
                                total = subtotal2+comisionmeses+subsidioAMGP;
                                mensualidades = total/3;
                                $("#buy").val(total);
                                $("#tresmeses").val(1);
                                table.html("<tr><td> <span class='title-left'>  Cuotas Anualidad 2018; Asociado "+namecuota+"</span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>   Subtotal 1 </span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>   Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>   Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>   Total </span></td><th class='mont'>$ "+number_format(pitarsubtotalcomision,"2",".",",")+"</th></tr>"+

                                    "<tr><th class='mensualidades'><span class='title-center'>Pago a 3 Meses</th><th class='mensualidades'></th></tr>"+
                                    "<tr><td><span class='title-left'> Comisión PayPal por pago a 3 meses </span></td><th class='mont'>$ "+number_format(comisionmeses,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> Total </span></td><th class='mont'>$ "+number_format(total,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> 3 mensualidades de </span></td><th class='mont'>$ "+number_format(mensualidades,"2",".",",")+"</th></tr>"
                                );
                            }
                            else if (meses==("NO"||"undefined")){
                                comisionPaypal = (((id*0.034)+4)*1.16);
                                subsidioAMGP = (comisionPaypal/2)*(-1);
                                subtotal2= id+comisionPaypal+subsidioAMGP;
                                $("#buy").val(subtotal2);
                                $("#tresmeses").val(0);
                                table.html("<tr><td> <span class='title-left'>  Cuotas Anualidad 2018; Asociado "+namecuota+"</span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>   Subtotal 1 </span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>   Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>   Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>   Total </span></td><th class='mont'>$ "+number_format(subtotal2,"2",".",",")+"</th></tr>"
                                );
                            }
                            else{
                                comisionPaypal = (((id*0.034)+4)*1.16);
                                subsidioAMGP = (comisionPaypal/2)*(-1);
                                subtotal2= id+comisionPaypal+subsidioAMGP;
                                $("#buy").val(subtotal2);
                                $("#tresmeses").val(0);
                                table.html("<tr><td> <span class='title-left'>  Cuotas Anualidad 2018; Asociado "+namecuota+"</span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>   Subtotal 1 </span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>   Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>   Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>   Total </span></td><th class='mont'>$ "+number_format(subtotal2,"2",".",",")+"</th></tr>"
                                );
                            }

                        }

                    }else if(response.code==404){
                        alertMessage("warning","Ha ocurrido un error, intente nuevamnete por favor");
                    }
                },
                error : function(){
                    console.log("error11");
                }
            });

        }
        else{
            jQuery.ajax({
                type : "POST",
                data : {id:cuota, value:value},
                url : "/pagos/paymentdescriptionayuda",
                dataType : "json",
                success : function(response){
                    code = response.code;
                    namecuota = response.cuotaname;
                    id= parseInt(response.cuotaid);

                    if(response.code==200){
                        if ($("#ayudamutua").is(':checked')==true) {

                            if(meses=="SI"){
                                costo = costoayuda;
                                comisionPaypal = (((costo*0.034)+4)*1.16);
                                subsidioAMGP = (comisionPaypal/2)*(-1);
                                subtotal2 = costo+comisionPaypal+subsidioAMGP;

                                pitarsubtotalcomision= subtotal2+subsidioAMGP
                                comisionmeses = ((subtotal2*0.0455)*1.16);
                                total = subtotal2+comisionmeses+subsidioAMGP;
                                mensualidades = total/3;
                                $("#buy").val(total)
                                $("#tresmeses").val(1);
                                table.html("<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango "+rango+"; Pago en "+mespagotabla+" </span></td><th class='mont'>$ "+number_format(costoayuda,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ "+number_format(costo,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ "+number_format(pitarsubtotalcomision,"2",".",",")+"</th></tr>"+

                                    "<tr><th class='mensualidades'><span class='title-center'>Pago a 3 Meses</th><th class='mensualidades'></th></tr>"+
                                    "<tr><td><span class='title-left'> Comisión PayPal por pago a 3 meses </span></td><th>$ "+number_format(comisionmeses,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> Total </span></td><th>$ "+number_format(total,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> 3 mensualidades de </span></td><th>$ "+number_format(mensualidades,"2",".",",")+"</th></tr>"
                                );
                            }
                            else if (meses=("NO"||"undefined")){
                                costo = costoayuda;
                                comisionPaypal = (((costo*0.034)+4)*1.16);
                                subsidioAMGP = (comisionPaypal/2)*(-1);
                                subtotal2 = costo+comisionPaypal+subsidioAMGP;
                                $("#buy").val(subtotal2);
                                $("#tresmeses").val(0);
                                table.html("<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango "+rango+"; Pago en "+mespagotabla+" </span></td><th class='mont'>$ "+number_format(costoayuda,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ "+number_format(costo,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ "+number_format(subtotal2,"2",".",",")+"</th></tr>"
                                );
                            }

                        }
                        else if($("#ayudamutua").is(':checked')==false){
                            table.html("");
                        }


                    }else if(response.code==404){
                        alertMessage("warning","Ha ocurrido un error, intente nuevamnete por favor");
                    }
                },
                error : function(){
                    console.log("error11");
                }
            });

        }


    });
    /* *** Fin cuando le dan click al check box de "¿Qué deseo pagar? click en anualidad **** */

    /* *** Cuando le dan click al check box de "¿Qué deseo pagar? click en ayuda mutua **** */
    $("input[name=ayudamutua]").click(function(){
        var cuota = $("select[name=cuota] option:selected").attr("data-type");
        var anonacimiento = $("select[name=year] option:selected").val();
        var fecha = new Date();
        var ano = fecha.getFullYear();
        var edad = ano-anonacimiento;
        var costoayuda=0;
        var comisionPaypal = 0;
        var subsidioAMGP = 0;
        var subtotal2 = 0;
        var meses = $('input[name=payment]:checked').val();
        var mespagotabla = "";
        var f = new Date();
        /* Rango de edades */
        var rango = "";
        if(edad>30&&edad<35){
            if((f.getMonth() +1)==12){
                costoayuda=7865;
                mespagotabla="Diciembre "+f.getFullYear();
            }
            else if ((f.getMonth() +1)==1){
                costoayuda=7940;
                mespagotabla="Enero "+f.getFullYear();
            }
            else if((f.getMonth() +1)>=2){
                costoayuda=8015;
                mespagotabla="Febrero "+f.getFullYear();
            }

            rango="30-34 años";
        }
        else if(edad<30){
            if((f.getMonth() +1)==12){
                costoayuda=5670;
                mespagotabla="Diciembre "+f.getFullYear();
            }
            else if ((f.getMonth() +1)==1){
                costoayuda=5724;
                mespagotabla="Enero "+f.getFullYear();
            }
            else if((f.getMonth() +1)>=2){
                costoayuda=5788;
                mespagotabla="Febrero "+f.getFullYear();
            }
            rango="Menor de 30 años";
        }
        else if(edad>34){
            if((f.getMonth() +1)==12){
                costoayuda=10080;
                mespagotabla="Diciembre "+f.getFullYear();
            }
            else if ((f.getMonth() +1)==1){
                costoayuda=10176;
                mespagotabla="Enero "+f.getFullYear();
            }
            else if((f.getMonth() +1)>=2){
                costoayuda=10272;
                mespagotabla="Febrero "+f.getFullYear();
            }
            rango="Mayor de 35 años";
        }
        /* Fin rango de edades */



        var value = $(this).val();
        if( $(this).is(':checked') ){
            jQuery.ajax({
                type : "POST",
                data : {id:cuota, value:value},
                url : "/pagos/paymentdescriptionayuda",
                dataType : "json",
                success : function(response){
                    code = response.code;
                    namecuota = response.cuotaname;
                    id= parseInt(response.cuotaid);
                    ayuda = parseInt(response.costayuda);

                    if(response.code==200){
                        if ($("#anualidad").is(':checked')==true) {

                            if(meses=="SI"){
                                costo = id + costoayuda;
                                comisionPaypal = (((costo * 0.034) + 4) * 1.16);
                                subsidioAMGP = (comisionPaypal / 2) * (-1);
                                subtotal2 = costo + comisionPaypal + subsidioAMGP;

                                pitarsubtotalcomision= subtotal2+subsidioAMGP
                                comisionmeses = ((subtotal2*0.0455)*1.16);
                                total = subtotal2+comisionmeses+subsidioAMGP;
                                mensualidades = total/3;
                                $("#buy").val(total);
                                $("#tresmeses").val(1);
                                table.html("<tr><td><span class='title-left'>   Cuotas Anualidad 2018; Asociado " + namecuota + " </span></td><th class='mont'>$ " + number_format(id, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango " + rango + "; Pago en "+mespagotabla+" </span></td><th class='mont'>$ " + number_format(costoayuda, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ " + number_format(costo, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ " + number_format(comisionPaypal, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ " + number_format(subsidioAMGP, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ " + number_format(pitarsubtotalcomision, "2", ".", ",") + "</th></tr>"+

                                    "<tr><th class='mensualidades'><span class='title-center'>Pago a 3 Meses</th><th class='mensualidades'></th></tr>"+
                                    "<tr><td><span class='title-left'> Comisión PayPal por pago a 3 meses </span></td><th>$ "+number_format(comisionmeses,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> Total </span></td><th>$ "+number_format(total,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> 3 mensualidades de </span></td><th>$ "+number_format(mensualidades,"2",".",",")+"</th></tr>"
                                );
                            }
                            else if (meses=("NO"||"undefined")){
                                costo = id + costoayuda;
                                comisionPaypal = (((costo * 0.034) + 4) * 1.16);
                                subsidioAMGP = (comisionPaypal / 2) * (-1);
                                subtotal2 = costo + comisionPaypal + subsidioAMGP;
                                $("#buy").val(subtotal2);
                                $("#tresmeses").val(0);
                                table.html("<tr><td><span class='title-left'>   Cuotas Anualidad 2018; Asociado " + namecuota + " </span></td><th class='mont'>$ " + number_format(id, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango " + rango + "; Pago en "+mespagotabla+" </span></td><th class='mont'>$ " + number_format(costoayuda, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ " + number_format(costo, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ " + number_format(comisionPaypal, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ " + number_format(subsidioAMGP, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ " + number_format(subtotal2, "2", ".", ",") + "</th></tr>"
                                );
                            }

                        }
                        else if($("#anualidad").is(':checked')==false){
                            if(meses=="SI"){
                                costo = costoayuda;
                                comisionPaypal = (((costo*0.034)+4)*1.16);
                                subsidioAMGP = (comisionPaypal/2)*(-1);
                                subtotal2 = costo+comisionPaypal+subsidioAMGP;

                                pitarsubtotalcomision= subtotal2+subsidioAMGP
                                comisionmeses = ((subtotal2*0.0455)*1.16);
                                total = subtotal2+comisionmeses+subsidioAMGP;
                                mensualidades = total/3;
                                $("#buy").val(total);
                                $("#tresmeses").val(1);
                                table.html("<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango "+rango+"; Pago en "+mespagotabla+" </span></td><th class='mont'>$ "+number_format(costoayuda,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ "+number_format(costo,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ "+number_format(pitarsubtotalcomision,"2",".",",")+"</th></tr>"+

                                    "<tr><th class='mensualidades'><span class='title-center'>Pago a 3 Meses</th><th class='mensualidades'></th></tr>"+
                                    "<tr><td><span class='title-left'> Comisión PayPal por pago a 3 meses </span></td><th>$ "+number_format(comisionmeses,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> Total </span></td><th>$ "+number_format(total,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> 3 mensualidades de </span></td><th>$ "+number_format(mensualidades,"2",".",",")+"</th></tr>"
                                );
                            }
                            else if (meses=("NO"||"undefined")){
                                costo = costoayuda;
                                comisionPaypal = (((costo*0.034)+4)*1.16);
                                subsidioAMGP = (comisionPaypal/2)*(-1);
                                subtotal2 = costo+comisionPaypal+subsidioAMGP;
                                $("#buy").val(subtotal2);
                                $("#tresmeses").val(0);
                                table.html("<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango "+rango+"; Pago en "+mespagotabla+" </span></td><th class='mont'>$ "+number_format(costoayuda,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ "+number_format(costo,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ "+number_format(subtotal2,"2",".",",")+"</th></tr>"
                                );
                            }
                        }
                    }else if(response.code==404){
                        alertMessage("warning","Ha ocurrido un error, intente nuevamnete por favor");
                    }
                },
                error : function(){
                    console.log("error11");
                }
            });
        } else {
            jQuery.ajax({
                type : "POST",
                data : {id:cuota, value:value},
                url : "/pagos/paymentdescriptionayuda",
                dataType : "json",
                success : function(response){
                    code = response.code;
                    namecuota = response.cuotaname;
                    id= parseInt(response.cuotaid);

                    if(response.code==200){
                        if ($("#anualidad").is(':checked')==true) {

                            if(meses=="SI"){
                                costo = id;
                                comisionPaypal = (((costo * 0.034) + 4) * 1.16);
                                subsidioAMGP = (comisionPaypal / 2) * (-1);
                                subtotal2 = costo + comisionPaypal + subsidioAMGP;


                                pitarsubtotalcomision= subtotal2+subsidioAMGP
                                comisionmeses = ((subtotal2*0.0455)*1.16);
                                total = subtotal2+comisionmeses+subsidioAMGP;
                                mensualidades = total/3;
                                $("#buy").val(total);
                                $("#tresmeses").val(1);
                                table.html("<tr><td><span class='title-left'>   Cuotas Anualidad 2018; Asociado " + namecuota + " </span></td><th class='mont'>$ " + number_format(id, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ " + number_format(costo, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ " + number_format(comisionPaypal, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ " + number_format(subsidioAMGP, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ " + number_format(pitarsubtotalcomision, "2", ".", ",") + "</th></tr>"+

                                    "<tr><th class='mensualidades'><span class='title-center'>Pago a 3 Meses</th><th class='mensualidades'></th></tr>"+
                                    "<tr><td><span class='title-left'> Comisión PayPal por pago a 3 meses </span></td><th>$ "+number_format(comisionmeses,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> Total </span></td><th>$ "+number_format(total,"2",".",",")+"</th></tr>"+
                                    "<tr><td><span class='title-right'> 3 mensualidades de </span></td><th>$ "+number_format(mensualidades,"2",".",",")+"</th></tr>"
                                );
                            }
                            else if (meses=("NO"||"undefined")){
                                costo = id;
                                comisionPaypal = (((costo * 0.034) + 4) * 1.16);
                                subsidioAMGP = (comisionPaypal / 2) * (-1);
                                subtotal2 = costo + comisionPaypal + subsidioAMGP;
                                $("#buy").val(subtotal2);
                                $("#tresmeses").val(0);
                                table.html("<tr><td><span class='title-left'>   Cuotas Anualidad 2018; Asociado " + namecuota + " </span></td><th class='mont'>$ " + number_format(id, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ " + number_format(costo, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ " + number_format(comisionPaypal, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ " + number_format(subsidioAMGP, "2", ".", ",") + "</th></tr>" +
                                    "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ " + number_format(subtotal2, "2", ".", ",") + "</th></tr>"
                                );
                            }
                        }
                        else if($("#anualidad").is(':checked')==false){
                            if(meses=="SI"){
                                table.html("");
                            }
                            else if (meses=("NO"||"undefined")){
                                table.html("");
                            }
                        }

                    }else if(response.code==404){
                        alertMessage("warning","Ha ocurrido un error, intente nuevamnete por favor");
                    }
                },
                error : function(){
                    console.log("error11");
                }
            });
        }



        /*jQuery.ajax({
            type : "POST",
            data : {id:cuota, value:value},
            url : "/pagos/paymentdescriptionayuda",
            dataType : "json",
            success : function(response){
                code = response.code;
                namecuota = response.cuotaname;
                id= parseInt(response.cuotaid);
                ayuda = parseInt(response.costayuda);

                if(response.code==200){
                    if(ayuda>0){
                        if ($("#ayudamutua").val("SI").is(':checked') && $("#anualidad").val("NO").is(':checked')) {
                            costo = id + costoayuda;
                            comisionPaypal = (((costo * 0.034) + 4) * 1.16);
                            subsidioAMGP = (comisionPaypal / 2) * (-1);
                            subtotal2 = costo + comisionPaypal + subsidioAMGP;
                            $("#buy").val(subtotal2)
                            table.html("<tr><td><span class='title-left'>   Cuotas Anualidad 2018; Asociado " + namecuota + " </span></td><th class='mont'>$ " + number_format(id, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango " + rango + "; Pago en "+mespagotabla+" </span></td><th class='mont'>$ " + number_format(costoayuda, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ " + number_format(costo, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ " + number_format(comisionPaypal, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ " + number_format(subsidioAMGP, "2", ".", ",") + "</th></tr>" +
                                "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ " + number_format(subtotal2, "2", ".", ",") + "</th></tr>"
                            );
                        }
                        else if($("#ayudamutua").is(':checked')&& !($("#anualidad").is(':checked'))){
                            costo = costoayuda;
                            comisionPaypal = (((costo*0.034)+4)*1.16);
                            subsidioAMGP = (comisionPaypal/2)*(-1);
                            subtotal2 = costo+comisionPaypal+subsidioAMGP;
                            $("#buy").val(subtotal2)
                            table.html("<tr><td><span class='title-left'>  Cuota Ayuda Mutua 2018; Rango "+rango+"; Pago en "+mespagotabla+" </span></td><th class='mont'>$ "+number_format(costoayuda,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'>    Subtotal 1 </span></td><th class='mont'>$ "+number_format(costo,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-left'>    Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-left'>    Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'>    Total </span></td><th class='mont'>$ "+number_format(subtotal2,"2",".",",")+"</th></tr>"
                            );
                        }
                        else if(!($("#ayudamutua").is(':checked'))&& ($("#anualidad").is(':checked'))){
                            comisionPaypal = (((id*0.034)+4)*1.16);
                            subsidioAMGP = (comisionPaypal/2)*(-1);
                            subtotal2= id+comisionPaypal+subsidioAMGP;
                            $("#buy").val(subtotal2)
                            table.html("<tr><td> <span class='title-left'>  Cuotas Anualidad 2018; Asociado "+namecuota+"</span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'>   Subtotal 1 </span></td><th>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-left'>   Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-left'>   Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                                "<tr><td><span class='title-right'>   Total </span></td><th class='mont'>$ "+number_format(subtotal2,"2",".",",")+"</th></tr>"
                            );
                        }
                    }
                    else{
                        comisionPaypal = (((id*0.034)+4)*1.16);
                        subsidioAMGP = (comisionPaypal/2)*(-1);
                        subtotal2= id+comisionPaypal+subsidioAMGP;
                        $("#buy").val(subtotal2)
                        table.html("<tr><td> <span class='title-left'>  Cuotas Anualidad 2018; Asociado "+namecuota+"</span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                        "<tr><td><span class='title-right'>   Subtotal 1 </span></td><th class='mont'>$ "+number_format(id,"2",".",",")+"</th></tr>"+
                        "<tr><td><span class='title-left'>   Comisiones por pago con PayPal </span></td><th class='mont'>$ "+number_format(comisionPaypal,"2",".",",")+"</th></tr>"+
                        "<tr><td><span class='title-left'>   Subsidio AMGP pago PayPal(50%) </span></td><th class='mont'>$ "+number_format(subsidioAMGP,"2",".",",")+"</th></tr>"+
                        "<tr><td><span class='title-right'>   Total </span></td><th class='mont'>$ "+number_format(subtotal2,"2",".",",")+"</th></tr>"
                        );
                    }

                }else if(response.code==404){
                    alertMessage("warning","Ha ocurrido un error, intente nuevamnete por favor");
                }
            },
            error : function(){
                console.log("error11");
            }
        });*/
    });
    var form = $("#paymentCuotaTemporalForm");
    if(form.length>=1){
        form.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El nombre es necesario.'
                        }
                    }
                },
                lastname: {
                    validators: {
                        notEmpty: {
                            message: 'El apellido paterno es necesario.'
                        }
                    }
                },
                secondname: {
                    validators: {
                        notEmpty: {
                            message: 'El apellido materno es necesario.'
                        }
                    }
                },
                rfc: {
                    validators: {
                        notEmpty: {
                            message: 'El RFC es necesario.'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Es necesaria la dirección de correo electrónico'
                        },
                        emailAddress: {
                            message: 'No contiene una dirección de correo electrónico váida.'
                        }
                    }
                },
                cuota: {
                    validators: {
                        notEmpty: {
                            message: 'El tipo de asociado es necesario.'
                        }
                    }
                },
                delegation: {
                    validators: {
                        notEmpty: {
                            message: 'La delegación es necesaria.'
                        }
                    }
                },
                day: {
                    validators: {
                        notEmpty: {
                            message: 'El día es necesario.'
                        }
                    }
                },
                mes: {
                    validators: {
                        notEmpty: {
                            message: 'El mes es necesario.'
                        }
                    }
                },
                year: {
                    validators: {
                        notEmpty: {
                            message: 'El año es necesario.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            e.preventDefault();
            var send = $("#sendM");
            var message = send.attr("data-loading-text");
            send.text(message).attr("disabled",true);
            $.ajax({
                type : "POST",
                url : "/pagos/guardar",
                data : $(this).serialize(),
                dataType : "json",
                success : function(response){
                    if(response.code==200){
                        window.location.href = response.url;
                    }else{
                        alert("No se ha podido generar la url de pago")
                    }
                },
                error : function(){

                },complete:function(){
                    setTimeout(function(){
                        $("#form-success").addClass("hidden");
                        $("#form-warning").addClass("hidden");
                    },2000);
                }
            });
        });
    }
});

function number_format (number, decimals, decPoint, thousandsSep) {
    
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
        var n = !isFinite(+number) ? 0 : +number
        var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
        var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
        var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
        var s = ''
    
        var toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec)
            return '' + (Math.round(n * k) / k)
                    .toFixed(prec)
        }
    
        // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || ''
            s[1] += new Array(prec - s[1].length + 1).join('0')
        }
    
        return s.join(dec)
    }
    
$(document).ready(function () {
  var newClient = $("#newClientEditPerfil");
  if (newClient.length >= 1) {
      var stid = $("#stid").val();
      uid = $("#uid").val();
      var cuotid = $("#cuotid").val();
      $('input:text,input:password, input:file, input[type=email], select, textarea').css("text-transform", "uppercase");
      setTimeout(function () {
          validateDate(newClient);
      }, 500);
      setTimeout(function () {
          $('.actions > ul > li > a[href$="#previous"]').attr('style', 'display:none;');
          $('.actions > ul > li > a[href$="#next"]').attr('style', 'left: 83.5% !important; position: absolute; top: 0%; background-color: #51a457 !important;');
      }, 500);
      newClient
          .steps({
              headerTag: 'h2',
              bodyTag: 'section',
              labels: {
                  current: "paso actual:",
                  pagination: "Paginación",
                  finish: "Guardar y continuar",
                  next: "Guardar y continuar",
                  previous: "Anterior",
                  loading: "Cargando ..."
              },
              onStepChanged: function (e, currentIndex, priorIndex) {
                  // You don't need to care about it
                  // It is for the specific demo
                  //adjustIframeHeight();
              },
              // Triggered when clicking the Previous/Next buttons
              onStepChanging: function (e, currentIndex, newIndex) {
                  $('.actions > ul > li > a[href$="#previous"]').addClass("anterior");
                  $('.actions > ul > li > a[href$="#next"]').addClass("saveandcontinue");
                  $('.actions > ul > li > a[href$="#finish"]').addClass("finish");
                  /*console.log({e:e,c:currentIndex,n:newIndex});*/
                  var fv = newClient.data('formValidation'), // FormValidation instance
                      // The current step container
                      $container = newClient.find('section[data-step="' + currentIndex + '"]');
                  if (newIndex == 0) {
                      $('.actions > ul > li > a[href$="#next"]').attr('style', 'left: 83.5% !important; position: absolute; background-color: #51a457 !important;');
                      $('.actions > ul > li > a[href$="#previous"]').attr('style', 'display:none;');
                      return true;
                  } else {
                      $('.actions > ul > li > a[href$="#next"]').attr('style', 'left: 83.5% !important; position: absolute; background-color: #51a457 !important;');
                      $('.actions > ul > li > a[href$="#previous"]').attr('style', 'display:init;');
                  }
                  // Validate the container
                  fv.validateContainer($container);
                  $('.actions > ul > li > a[href$="#previous"]').addClass("anterior");
                  $('.actions > ul > li > a[href$="#next"]').addClass("saveandcontinue");

                  var isValidStep = fv.isValidContainer($container);
                  if (isValidStep === false || isValidStep === null) {
                      // Do not jump to the next step
                      return false;
                  }
                  return true;
              },
              // Triggered when clicking the Finish button
              onFinishing: function (e, currentIndex) {
                  var fv = newClient.data('formValidation'),
                      $container = newClient.find('section[data-step="' + currentIndex + '"]');
                  // Validate the last step container
                  fv.validateContainer($container);

                  var isValidStep = fv.isValidContainer($container);
                  if (isValidStep === false || isValidStep === null) {
                      return false;
                  }
                  return true;
              },
              onFinished: function (e, currentIndex) {
                  e.preventDefault();
                  clidAll = $("#stid").val();
                  cuotid = $("#cuotid").val();
                  $.ajax({
                      type:"post",
                      url:"/registro/validatedocument",
                      data: {"clidAll":clidAll,"cuotid":cuotid},
                      dataType: "JSON",
                      success : function(response){
                          if (response.code == 200) {
                              var modal = response.modalname;
                              $("#"+modal).modal("show");
                              var addDocument2 = $("#list-studios-client");
                              if(response.client.actaNacimiento == "" || response.client.actaNacimiento == null)
                              {
                                  /* $("#acta").removeClass("hidden");*/
                                  addDocument2.append("<li class='list-modal-incomplete'>Acta de nacimiento</li>");
                              }
                              if(response.client.cv == "" || response.client.cv == null){
                                  /*$("#cv").removeClass("hidden");*/
                                  addDocument2.append("<li class='list-modal-incomplete'>Currículum vitae</li>");
                              }
                              if(response.client.photo == "" || response.client.photo == null){
                                  /*$("#photo").removeClass("hidden");*/
                                  addDocument2.append("<li class='list-modal-incomplete'>Fotografía</li>");
                              }
                              var addDocument = $("#list-studios-client");
                              jQuery.each(response.document, function(i, val){
                                  addDocument.append("<li class='list-modal-incomplete'>"+val+"</li>");
                              });
                              $('#str').val(JSON.stringify(response.document));

                          } else {
                              /*alert("No se ha podido generar la url de pago")*/
                          }
                      },
                      error: function () {
                      }, complete: function (response) {
                          setTimeout(function () {
                          }, 2000);
                      }
                  });
              }
          }).formValidation({
              framework: 'bootstrap',
              icon: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              excluded: ':disabled',
              locale: 'es_ES',
              fields: {
                  phone: {
                      validators: {
                          callback: {
                              message: 'El teléfono es necesario y no puede estar vacio.',
                              callback: function (value, validator, $field) {
                                  var channel = newClient.find('[name="input_required"]:checked').val();
                                  return (channel !== 'phone') ? true : (value !== '');
                              }
                          }
                      }
                  },
                  name: {
                      validators: {
                          notEmpty: {
                              message: 'El nombre es necesario.'
                          }
                      }
                  },
                  lastname: {
                      validators: {
                          notEmpty: {
                              message: 'El apellido paterno es necesario.'
                          }
                      }
                  },
                  secondname: {
                      validators: {
                          notEmpty: {
                              message: 'El apellido materno es necesario.'
                          }
                      }
                  },
                  sex: {
                      validators: {
                          notEmpty: {
                              message: 'El sexo es necesario.'
                          }
                      }
                  },
                  comid: {
                      validators: {
                          notEmpty: {
                              message: 'La delegación es necesaria.'
                          }
                      }
                  },
                  phone: {
                      validators: {
                          notEmpty: {
                              message: 'El celular es necesario.'
                          }
                      }
                  },
                  nacionalidad: {
                      validators: {
                          notEmpty: {
                              message: 'La nacionalidad es necesaria.'
                          }
                      }
                  },
                  daybirthdate: {
                      validators: {
                          notEmpty: {
                              message: 'El día es necesario.'
                          }
                      }
                  },
                  mothbirthdate: {
                      validators: {
                          notEmpty: {
                              message: 'El mes es necesario.'
                          }
                      }
                  },
                  yearsbirthdate: {
                      validators: {
                          notEmpty: {
                              message: 'El año es necesario.'
                          }
                      }
                  },
                  titulo: {
                      validators: {
                          notEmpty: {
                              message: 'El titulo es necesario.'
                          }
                      }
                  },
                  especialidad: {
                      validators: {
                          notEmpty: {
                              message: 'La especialidad es necesaria.'
                          }
                      }
                  },
                  universidad: {
                      validators: {
                          notEmpty: {
                              message: 'La universidad es necesaria.'
                          }
                      }
                  },
                  carrera: {
                      validators: {
                          notEmpty: {
                              message: 'La carrera es necesaria.'
                          }
                      }
                  },
                  mesfinalstu: {
                      validators: {
                          notEmpty: {
                              message: 'El mes es necesario.'
                          }
                      }
                  },
                  annofinalstu: {
                      validators: {
                          notEmpty: {
                              message: 'El año es necesario.'
                          }
                      }
                  },
                  empresa: {
                      validators: {
                          notEmpty: {
                              message: 'La empresa es necesaria.'
                          }
                      }
                  },
                  puesto: {
                      validators: {
                          notEmpty: {
                              message: 'El puesto es necesario.'
                          }
                      }
                  },
                  actual: {
                      validators: {
                          notEmpty: {
                              message: 'Es necesario indicarlo.'
                          }
                      }
                  },
                  mes: {
                      validators: {
                          notEmpty: {
                              message: 'El mes es necesario.'
                          }
                      }
                  },
                  anno: {
                      validators: {
                          notEmpty: {
                              message: 'El año es necesario.'
                          }
                      }
                  },
                  mesfinal: {
                      validators: {
                          callback: {
                              message: 'El mes es necesario.',
                              callback: function(value, validator, $field) {
                                  var framework = $('#newClientEditPerfil').find('[name="actual"] option:selected').val();
                                  return (framework !== 'NO') ? true : (value !== '');
                              }
                          }
                      }
                  },
                  annofinal: {
                      validators: {
                          callback: {
                              message: 'El año es necesario.',
                              callback: function(value, validator, $field) {
                                  var framework = $('#newClientEditPerfil').find('[name="actual"] option:selected').val();
                                  return (framework !== 'NO') ? true : (value !== '');
                              }
                          }
                      }
                  },
              }
          }).on('change', '[name="actual"]', function(e) {
              $('#newClientEditPerfil').formValidation('revalidateField', 'mesfinal');
              $('#newClientEditPerfil').formValidation('revalidateField', 'annofinal');
          }).on('err.field.fv', function (e, data) {
              if (data.fv.getSubmitButton()) {
                  data.fv.disableSubmitButtons(false);
              }
          }).on('success.field.fv', function (e, data) {
              if (data.fv.getSubmitButton()) {
                  data.fv.disableSubmitButtons(false);
              }
          });
      $(".tutor").on("change", function (e) {
          var val = $(this).val();
          if (val == "SI") {
              $(".add_data_client[name='calle']").val($("#calle").val()).change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='numerointe']").val($("#numerointe").val()).change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='numeroexte']").val($("#numeroexte").val()).change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='colonia']").val($("#colonia").val()).change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='cp']").val($("#cp").val()).change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='municipio']").val($("#municipio").val()).change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='localidad']").val($("#localidad").val()).change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='estado']").val($("#estado").val()).change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='pais']").val($("#pais").val()).change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
          } else {
              $(".add_data_client[name='calle']").val("").change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='numerointe']").val("").change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='numeroexte']").val("").change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='colonia']").val("").change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='cp']").val("").change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='municipio']").val("").change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='localidad']").val("").change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='estado']").val("").change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
              $(".add_data_client[name='pais']").val("").change(function () {
                  newClient.formValidation('revalidateField', $(this));
              }).trigger("change");
          }
      });
      $("#addOtherUser").click(function (e) {
          e.preventDefault();
          newClient.data('formValidation').resetForm(true);
          resetForm(newClient);
          $("#uid").val(uid);
          $('.radio_email').prop('checked', true);
          newClient.formValidation('revalidateField', 'email');
          $('.radio_work').prop('checked', true);
          $('#scid.selectUP').select2('val', "");
          $('#crid.selectUP').select2('val', "");
          $('#hour_interest.selectUP').select2('val', "");
          if ($("#validateUid").length >= 1) {
              $('#uid.selectUP').select2('val', "");
          }
      });
      /* saveKeyupValue("keyup_Save", newClient, stid);
       saveChangeValue("change_Save", newClient, stid);
       saveChange2Value("change_Save2", newClient, stid);*/
  }
});

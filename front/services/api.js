front.service("Api",["$http","$q",function($http,$q){
    this.api = function($key,$val,$type){
        var defer = $q.defer();
        $http.get("/api/v1/web/get/"+$key+"?type="+$type+"&val="+$val)
            .success(function(data){
                defer.resolve(data);
            }).error(function (data) {
                defer.reject(data);
            });
        return defer.promise;
    }
}]);
front.service("Services",["$http","$q",function($http,$q){
    this.services = function($key,$type){
        var defer = $q.defer();
        $http.get("/api/v1/web/get/"+$key+"?type="+$type).success(function(data){
            defer.resolve(data);
        }).error(function(data){
            defer.reject(data);
        });
        return defer.promise;
    }
}]);
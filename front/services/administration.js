front.service("Administration",["$http","$q", function ($http,$q) {
    this.administration = function($key){
        var defer = $q.defer();
        $http.get("/api/v1/web/get-administration/"+$key).success(function(data){
            defer.resolve(data);
        }).error(function(data){
            defer.reject(data);
        });
        return defer.promise;
    }
}]);
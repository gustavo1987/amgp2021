var front = angular.module("front",['ngRoute','angular-loading-bar','ngAnimate','ui.bootstrap','ngSanitize','bootstrapLightbox','videosharing-embed','ngMap']);

front.config(['$routeProvider','$locationProvider','cfpLoadingBarProvider',function($routeProvider,$locationProvider,cfpLoadingBarProvider){
    $routeProvider
        .when("/",{
            templateUrl:"/front/views/index/index.html",
            controller : "IndexController"
        }).when("/todas-las-empresas",{
            templateUrl:"/front/views/index/all_company.html",
            controller : "IndexController"
        }).when("/contacto",{
            templateUrl:"/front/views/index/contact.html",
            controller : "IndexController"
        })
        //Categories List
        .when('/:category',{
            templateUrl:"/front/views/api/category.html",
            controller:"ApiController"
        }).when('/:category/:subcategory',{
            templateUrl:"/front/views/api/subcategory.html",
            controller:"ApiController"
        }).when('/:category/:subcategory/:val',{
            templateUrl:"/front/views/api/list.html",
            controller:"ApiController"
        }).when('/:category/:subcategory/:val/:perm',{
            templateUrl:"/front/views/api/list.html",
            controller:"ApiController"
        })
        .when("/contactanos",{
            templateUrl:"/front/views/contact/contact.html",
            controller: "ContactController"
        })
        .otherwise({redirectTo:"/"});
    $locationProvider.html5Mode(true);
    cfpLoadingBarProvider.includeSpinner = true;
}]).directive('carouselP',function($timeout){
    return function($scope,$el,$attrs){
        setTimeout((function(){
            $el.owlCarousel({
                autoPlay: 3200,
                items : 2,
                navigation: false,
                itemsDesktopSmall : [1024,3],
                itemsTablet : [768,2],
                itemsMobile : [500,1],
                pagination: false
            })
        }),500)
    }
}).directive('carouselG',function($timeout){
    return function($scope,$el,$attrs){
        setTimeout((function(){
            $el.owlCarousel({
                items : 3,
                lazyLoad : true,
                navigation : true
            })
        }),500)
    }
}).directive('carouselT',function($timeout){
    return function($scope,$el,$attrs){
        setTimeout((function(){
            $el.owlCarousel({
                items : 1,
                autoPlay: 3200,
                navigation : false,
                autoHeight : true,
                slideSpeed : 400,
                singleItem: true,
                pagination : true
            })
        }),500)
    }
}).directive('carouselC',function($timeout){
    return function($scope,$el,$attrs){
        setTimeout((function(){
            $el.owlCarousel({
                autoPlay: 3200,
                items : 5,
                navigation: false,
                itemsDesktop : [1199,4],
                itemsDesktopSmall : [1024,4],
                itemsTablet : [768,3],
                itemsMobile : [500,2],
                pagination: true
            })
        }),500)
    }
}).filter('capitalize', function() {
    return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
}).filter('explode', function() {
    var result;
    return function(input) {
        result = input.split(',');
        if(result.length>1){
            result = result[0]+", "+result[1];
            return result;
        }
        return input;
    }
}).filter('explode_array', function() {
    var result;
    return function(input) {
        result = input.split(',');
        if(result.length>1){
            input = result;
        }
        return input;
    }
}).filter('num', function() {
    var result;
    return function(input) {
        result = input.split(":");
        return result[0];
    }
});
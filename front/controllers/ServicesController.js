front.controller("ServicesController",["$scope","$timeout","$http","Api",function($scope,$timeout,$http,Api){
    $scope.categories = function($key,$val,$type){
        var categories = Api.api($key,$val,$type);
        categories.then(function(data){
            $scope.c_result  = data.result;
        },function(error){
            console.log(error);
        })
    };
    var key = angular.element('#key').val();
    $scope.categories(key,null,"home");

    $scope.successSubscription = true;
    $scope.errorSubscription = true;
    $scope.sendSubscription = true;
    $scope.warningSubscription = true;
    $scope.subscriptionData  = {};
    $scope.formSubscription = function(isValid){
        var button  =  angular.element("#submitInput");
        if(isValid){
            $scope.subscriptionData['type'] = "subscription";
            button.attr("disabled",true);
            $scope.sendSubscription = false;
            $http({
                method:"POST",
                url :  "/api/v1/web/get/"+key,
                data : $.param($scope.subscriptionData),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function(data){
                $timeout(function(){$scope.sendSubscription = true;},350);
                if(data.code=="200"){
                    $scope.successSubscription = false;
                    $timeout(function(){$scope.successSubscription = true;},2000);
                    $scope.subscriptionData={};
                    $scope.subscription.$setPristine();
                }else if(data.code=="303"){
                    $scope.warningSubscription = false;
                    $timeout(function(){$scope.warningSubscription = true;},2000);
                    button.attr("disabled",false);
                }else{
                    $scope.errorSubscription = false;
                    $timeout(function(){$scope.errorSubscription = true;},2000);
                    button.attr("disabled",false);
                }
            }).finally(function(){});
        }
    };
}]);
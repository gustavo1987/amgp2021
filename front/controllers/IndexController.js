front.controller("IndexController",["$scope","$location","$http","$timeout","Administration","Api",function($scope,$location,$http,$timeout,Administration,Api){
    $scope.$location = $location;
    $scope.toggle = true;
    $scope.administration  = function($key){
        var admin = Administration.administration($key);
        admin.then(function(data){
            $scope.sliders = data.slider;
            $scope.advertising = data.advertising;
            $scope.companies = data.random;
        },function(error){
            console.log(error);
        })
    };

    $scope.api = function($key,$val,$type){
        var result = Api.api($key,$val,$type);
        result.then(function(data){
            $scope.result  = data.result;
            $scope.name = data.name;
            if($scope.$location.path()=="/")document.getElementById("search_focus").focus();
        },function(error){
            console.log(error);
        })
    };

    var key = angular.element('#key').val();
    switch ($scope.$location.path()){
        case "/": $scope.administration(key);
            break;
        case "/todas-las-empresas": $scope.api(key,"all","all_company"); $scope.sort=2;
                                    $scope.sortType = 'name'; $scope.sortReverse  = false;
            break;
        case "/contacto":
            $scope.successContact = true;
            $scope.errorContact = true;
            $scope.sendContact = true;
            $scope.warningContact = true;
            $scope.contactData = {};
            $scope.formContact = function(isValid){
                var button  =  angular.element("#submitContact");
              if(isValid){
                  $scope.contactData['type'] = "contact";
                  button.attr("disabled",true);
                  $scope.sendContact = false;
                  $http({
                      method:"POST",
                      url :  "/api/v1/web/get/"+key,
                      data : $.param($scope.contactData),
                      headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                  }).success(function(data){
                      $timeout(function(){$scope.sendContact = true;},350);
                      if(data.code=="200"){
                          $scope.successContact = false;
                          $timeout(function(){$scope.successContact = true;},2000);
                          $scope.contactData={};
                      }else if(data.code=="303"){
                          $scope.warningContact = false;
                          $timeout(function(){$scope.warningContact = true;},2000);
                          button.attr("disabled",false);
                      }else{
                          $scope.errorContact = false;
                          $timeout(function(){$scope.errorContact = true;},2000);
                          button.attr("disabled",false);
                      }
                  }).finally(function(){});
              }
            };
        break;
    }
    $scope.resetForm = function (formName)
    {formName.$setPristine();};

    $scope.search = function(){
        $scope.toggle = false;
        $scope.api(key,$scope.keywords,"search");
    }
}]);
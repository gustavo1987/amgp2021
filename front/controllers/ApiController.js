front.controller("ApiController",["$scope","$routeParams","$http","$location","$http","$timeout","Lightbox","Api",function($scope,$routeParams,$http,$location,$http,$timeout,Lightbox,Api){
    $scope.location = $location;

    $scope.category= $routeParams.category;
    $scope.subcategory= $routeParams.subcategory;
    $scope.val = $routeParams.val;
    $scope.perm = $routeParams.perm;
    $scope.promotions = [];
    $scope.additional = [];
    var key = angular.element('#key').val();
    $scope.api = function($key,$val,$type){
      var result = Api.api($key,$val,$type);
        result.then(function(data){
            $scope.result  = data.result;
            $scope.name = data.name;
            $scope.number_type = data.number_type;
            switch ($scope.number_type){
                case "2" : $scope.sort = 2; break;
                case "3" : $scope.sort = 1; break;
                case "4" :
                        $scope.direction = data.result.office.PRINCIPAL;
                        angular.forEach($scope.result.promotions, function(value, key) {
                                $scope.promotions.push({"url":value.images.original,"thumbnail":value.images.thumbnail});
                        });
                        if($scope.result.additional.length>=1){
                            angular.forEach($scope.result.additional, function(value, key) {
                                $scope.additional.push({"url":value.images.original,"thumbnail":value.images.thumbnail});
                            });
                        }
                    $scope.coordinates = [data.result.map.lat,data.result.map.long];
                    break
            }
        },function(error){
            console.log(error);
        })
    };
    $scope.formC = function($key,$permalink){
        $scope.successContact = true;
        $scope.errorContact = true;
        $scope.sendContact = true;
        $scope.warningContact = true;
        $scope.contactData = {};
        $scope.formContact = function(isValid){
            var button  =  angular.element("#submitContact");
            if(isValid){
                $scope.contactData['type'] = "contact";
                $scope.contactData['company'] = true;
                $scope.contactData['permalink'] = $permalink;
                button.attr("disabled",true);
                $scope.sendContact = false;
                $http({
                    method:"POST",
                    url :  "/api/v1/web/get/"+$key,
                    data : $.param($scope.contactData),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function(data){
                    $timeout(function(){$scope.sendContact = true;},350);
                    if(data.code=="200"){
                        $scope.successContact = false;
                        $timeout(function(){$scope.successContact = true;},2000);
                        $scope.contactData={};
                    }else if(data.code=="303"){
                        $scope.warningContact = false;
                        $timeout(function(){$scope.warningContact = true;},2000);
                        button.attr("disabled",false);
                    }else{
                        $scope.errorContact = false;
                        $timeout(function(){$scope.errorContact = true;},2000);
                        button.attr("disabled",false);
                    }
                }).finally(function(){});
            }
        };
    };
    if($scope.category && $scope.subcategory && $scope.val && $scope.perm && key){
        $scope.api(key,$scope.perm,"company");
        $scope.formC(key,$scope.perm);
    }else if($scope.category && $scope.subcategory && $scope.val && key){
        $scope.api(key,$scope.val,"company");
        $scope.formC(key,$scope.val);
    }else if($scope.category && $scope.subcategory && key){
        $scope.api(key,$scope.subcategory,"subcategory");
    }else if($scope.category && key){
        $scope.api(key,$scope.category,"category");
        $scope.sort = 1;
    }
    $scope.status = {
        openOne : true
    };
    if($scope.promotions.length>=1){

    }
    if($scope.additional.length>=1){

    }
    $scope.openLightboxPromotion = function (index) {
        Lightbox.openModal($scope.promotions, index);
    };
    $scope.openLightboxAdditional = function (index) {
        Lightbox.openModal($scope.additional, index);
    };
    $scope.map={
        zoom : 15,
        options : {
            scrollwheel: false
        },
        style:[{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"administrative","elementType":"labels","stylers":[{"saturation":"-100"}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"gamma":"0.75"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"lightness":"-37"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f9f9f9"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"saturation":"-100"},{"lightness":"40"},{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"labels.text.fill","stylers":[{"saturation":"-100"},{"lightness":"-37"}]},{"featureType":"landscape.natural","elementType":"labels.text.stroke","stylers":[{"saturation":"-100"},{"lightness":"100"},{"weight":"2"}]},{"featureType":"landscape.natural","elementType":"labels.icon","stylers":[{"saturation":"-100"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"saturation":"-100"},{"lightness":"80"}]},{"featureType":"poi","elementType":"labels","stylers":[{"saturation":"-100"},{"lightness":"0"}]},{"featureType":"poi.attraction","elementType":"geometry","stylers":[{"lightness":"-4"},{"saturation":"-100"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#c5dac6"},{"visibility":"on"},{"saturation":"-95"},{"lightness":"62"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":20}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":20}]},{"featureType":"road","elementType":"labels","stylers":[{"saturation":"-100"},{"gamma":"1.00"}]},{"featureType":"road","elementType":"labels.text","stylers":[{"gamma":"0.50"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"saturation":"-100"},{"gamma":"0.50"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#c5c6c6"},{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"-13"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"lightness":"0"},{"gamma":"1.09"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#e4d7c6"},{"saturation":"-100"},{"lightness":"47"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"lightness":"-12"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"saturation":"-100"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#fbfaf7"},{"lightness":"77"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"lightness":"-5"},{"saturation":"-100"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"saturation":"-100"},{"lightness":"-15"}]},{"featureType":"transit.station.airport","elementType":"geometry","stylers":[{"lightness":"47"},{"saturation":"-100"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#acbcc9"}]},{"featureType":"water","elementType":"geometry","stylers":[{"saturation":"53"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"lightness":"-42"},{"saturation":"17"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"lightness":"61"}]}]
    };
    $scope.sortType = 'name'; $scope.sortReverse  = false;
    $scope.resetForm = function(formName){
        formName.$setPristine()
    };
    $scope.validate_hour = function() {
        var date = new Date();
        var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        //time = hours + ":" + minutes + ":" + seconds;
        return parseInt(hours);
    };
    $scope.hour = $scope.validate_hour();

    $scope.format = function(val){
        $object = {"1":13,"2":14,"3":15,"4":16,"5":17,"6":18,"7":19,"8":20,"9":21,"10":22,"11":23,"12":0};
        hr = val.split(":");
        return parseInt($object[hr[0]]);
    }
}]);
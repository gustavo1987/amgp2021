$(document).ready(function() {
    $("#icon-closed").click(function(){
        $("#closed-payment").remove();
        $("#closed-payment").css({"display":"none"});
    });

    $(".list-group-item").click(function(){
      var imageUrl = $(this).attr("data-image");
        $('.testimonial_client_area3').css('background-image', 'url(' + imageUrl + ')');
    });
    $('.map-container')
        .click(function(){
            $(this).find('iframe').addClass('clicked')})
        .mouseleave(function(){
            $(this).find('iframe').removeClass('clicked')});

    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })

    //User edit

    var userEdit = $('#updateClient');
    if(userEdit.length>=1){
      userEdit.formValidation({
        framework:'bootstrap',
        icon:{
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        locale:'es_Es',
        excluded:":disabled",
        fields:{
          name:{
            validators:{
              notEmpty:{
                message:"Rellenar campo"
              }
            }
          },
          lastname:{
            validators:{
              notEmpty:{
                message:"Rellenar campo"
              }
            }
          },
          sex:{
            validators:{
              notEmpty:{
                message:"Rellenar campo"
              }
            }
          },
          daybirthdate:{
            validators:{
              notEmpty:{
                message:"Rellenar campo"
              }
            }
          },
          mothbirthdate:{
            validators:{
              notEmpty:{
                message:"Rellenar campo"
              }
            }
          },
          yearsbirthdate:{
            validators:{
              notEmpty:{
                message:"Rellenar campo"
              }
            }
          },
          rfc:{
            validators:{
              notEmpty:{
                message:"Rellenar campo"
              }
            }
          },
          curp:{
            validators:{
              notEmpty:{
                message:"Rellenar campo"
              }
            }
          }
        }
      }).on("success.form.fv",function(e){
        e.preventDefault();
        $.ajax({
          url:"/updatedatospersonales",
          type:"post",
          data:$(this).serialize(),
          success:function(resp){
            swal("Datos guardados correctamente");
          }
        });
      });
    }

    /* Suscription modal */
    var suscription_value_table = $(".suscription_value_table");
    if(suscription_value_table.length>=1){
        suscription_value_table.formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            locale: 'es_ES',
            excluded: ':disabled',
            fields: {
                email: {
                    validators: {
                        validators: {notEmpty: {}},
                        emailAddress: {
                            message: 'Este campo no contiene una dirección de correo electrónico válida.'
                        },
                        remote: {
                            message: 'Esta cuenta de correo electrónico ya existe, pruebe con otra',
                            url: '/session/validate-email',
                            data: {
                                type: 'email'
                            },
                            type: 'POST'
                        }
                    }
                }
            }
        });
        var mail = {};
        suscription_value_table.click(function () {
            mail = $(this).parent().parent().find("input.email").val();
            $("#delete_modal").modal("show");
        });
        $("#delete_modal").on('shown.bs.modal', function (e) {
            $(".value_name_delete").text(mail);
        });
        $("#delete_modal").on('hidden.bs.modal', function (e) {
            mail = {};
            $(".value_name_delete").text(null);
        });
        $("#confirm_delete").click(function(){
            $.ajax({
                type : "POST",
                url : "/index/suscription",
                data : {value:mail},
                dataType : "json",
                success : function(response){
                    $("#delete_modal").modal("hide");
                    switch (response.code){
                        case 200:

                            break;
                        case 300:

                            break;
                        default :
                            break;
                    }
                },
                error : function(){
                    $("#delete_modal").modal("hide");

                }
            });
        });
    }



    $(function () {
        $('.button-checkbox').each(function () {

            // Settings
            var $widget = $(this),
                $button = $widget.find('button'),
                $checkbox = $widget.find('input:checkbox'),
                color = $button.data('color'),
                settings = {
                    on: {
                        icon: 'glyphicon glyphicon-check'
                    },
                    off: {
                        icon: 'glyphicon glyphicon-unchecked'
                    }
                };

            // Event Handlers
            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });

            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');

                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $button.find('.state-icon')
                    .removeClass()
                    .addClass('state-icon ' + settings[$button.data('state')].icon);

                // Update the button's color
                if (isChecked) {
                    $button
                        .removeClass('btn-default')
                        .addClass('btn-' + color + ' active');
                }
                else {
                    $button
                        .removeClass('btn-' + color + ' active')
                        .addClass('btn-default');
                }
            }

            // Initialization
            function init() {

                updateDisplay();

                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    });

    $(document).ready(function() {

        //Added, set initial value.
        $("#amount").val(0);
        $("#amount4").val(0);
        $("#amount2").val(0);
        $("#amount3").val(0);
        $("#amount5").val(0);
        $("#amount6").val(0);
        $("#amount7").val(0);
        $("#amount8").val(0);
        $("#amount9").val(0);
        $("#amount10").val(0);
        $("#amount11").val(0);
        $("#amount-label").text(0);
        $("#amount2-label").text(0)
        $("#amount3-label").text(0);
        $("#amount4-label").text(0);
        $("#amount5-label").text(0);
        $("#amount6-label").text(0);
        $("#amount7-label").text(0);
        $("#amount8-label").text(0);
        $("#amount9-label").text(0);
        $("#amount10-label").text(0);
        $("#amount11-label").text(0);




        update();
    });

//changed. now with parameter
    function update(slider,val) {
        //changed. Now, directly take value from ui.value. if not set (initial, will use current value.)
        var $amount = slider == 1?val:$("#amount").val();
        var $amount2 = slider == 3?val:$("#amount2").val();
        var $amount3 = slider == 4?val:$("#amount3").val();
        var $amount4 = slider == 2?val:$("#amount4").val();
        var $amount5 = slider == 5?val:$("#amount5").val();
        var $amount6 = slider == 6?val:$("#amount6").val();
        var $amount7 = slider == 7?val:$("#amount7").val();
        var $amount8 = slider == 8?val:$("#amount8").val();
        var $amount9 = slider == 9?val:$("#amount9").val();
        var $amount10 = slider == 10?val:$("#amount10").val();
        var $amount11 = slider == 11?val:$("#amount11").val();


        /* commented
         $amount = $( "#slider" ).slider( "value" );
         $duration = $( "#slider2" ).slider( "value" );
         */

        $( "#amount" ).val($amount);
        $( "#amount-label" ).text($amount);
        $( "#amount2" ).val($amount2);
        $( "#amount2-label" ).text($amount2);
        $( "#amount3" ).val($amount3);
        $( "#amount3-label" ).text($amount3);
        $( "#amount4" ).val($amount4);
        $( "#amount4-label" ).text($amount4);
        $( "#amount5" ).val($amount5);
        $( "#amount5-label" ).text($amount5);
        $( "#amount6" ).val($amount6);
        $( "#amount6-label" ).text($amount6);
        $( "#amount7" ).val($amount7);
        $( "#amount7-label" ).text($amount7);
        $( "#amount8" ).val($amount8);
        $( "#amount8-label" ).text($amount8);
        $( "#amount9" ).val($amount9);
        $( "#amount9-label" ).text($amount9);
        $( "#amount10" ).val($amount10);
        $( "#amount10-label" ).text($amount10);
        $( "#amount11" ).val($amount11);
        $( "#amount11-label" ).text($amount11);
        $('#slider a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
        $('#slider2 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount4+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
        $('#slider3 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount2+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
        $('#slider4 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount3+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
        $('#slider5 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount5+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
        $('#slider6 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount6+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
        $('#slider7 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount7+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
        $('#slider8 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount8+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
        $('#slider9 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount9+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
        $('#slider10 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount10+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
        $('#slider11 a').html('<label><span class="glyphicon glyphicon-chevron-left"></span> '+$amount11+' <span class="glyphicon glyphicon-chevron-right"></span></label>');
    }

    $(document).ready(function()
    {


        $("#mostrarmodalSus").modal("show");
        setTimeout(function(){
            $("#mostrarmodalSus").modal('hide');
        }, 199000);
        $('.launch-modal').click(function(){
            $('#myModal').modal({
                backdrop: 'static'
            });
        });
        $('.launch-modal2').click(function(){
            $('#myModal2').modal({
                backdrop: 'static'
            });
        });
        var form = $("#contactForm");
        if(form.length>=1){
            form.formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                locale: 'es_ES',
                excluded: ':disabled',
                fields: {}
            }).on('success.form.fv', function(e) {
                e.preventDefault();
                var send = $("#sendM");
                var message = send.attr("data-loading-text");
                send.text(message).attr("disabled",true);
                $.ajax({
                    type : "POST",
                    url : "/index/message",
                    data : $(this).serialize(),
                    dataType : "json",
                    success : function(response){
                        if(response.code==200){
                            send.text("Enviar mensaje").attr("disabled",false);
                            resetForm(form);
                            form.formValidation('resetForm', true);
                            $("#form-success").removeClass("hidden");
                        }else{
                            $("#form-warning").removeClass("hidden");
                            send.text("Intentar nuevamente").attr("disabled",false);
                        }
                    },
                    error : function(){

                    },complete:function(){
                        setTimeout(function(){
                            $("#form-success").addClass("hidden");
                            $("#form-warning").addClass("hidden");
                        },2000);
                    }
                });
            });
        }
    });
    function resetForm($form) {
        $form.find('input:text, input:password, input:file, input[type=email], select, textarea').val('');
        $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
    }

    var tabsFn = (function() {

        function init() {
            setHeight();
        }

        function setHeight() {
            var $tabPane = $('.tab-pane'),
                tabsHeight = $('.nav-tabs').height();

            $tabPane.css({
                height: tabsHeight
            });
        }

        $(init);
    })();




});

function eliminarProducto($id){
    array = [];
    var match = document.cookie.match(new RegExp('(^| )' + "carrito" + '=([^;]+)'));
    if (match){
        var arreglo =match[2];
        var json = JSON.parse(decodeURIComponent(arreglo));
        for(var i=0;i<json.length;i++){
             array.push(parseInt(json[i]["publid"]));
        }
    }
    var posicion = array.indexOf($id);
    array.splice(posicion, 1);
    $.ajax({
        url:"/agregarcarrito",
        type:"post",
        data:{"public[]":array},
        dataType:"json",
        success:function(resp){
            $('#preciocarrito_'+$id).remove();
            $('#productocarrito_'+$id).remove();
            $('#precioTotal').text("$"+resp.precio+".00");
            $('#cantidadCarrito').html(" "+resp.cant);
        }
    })
    console.log(array);

}

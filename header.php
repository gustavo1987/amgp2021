<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/front/src/images/favicon.png">
    <meta charset="utf-8">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="revisit-after" content="7 days">
    <meta name="googlebot" content="follow, index, all">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Asociación Mexicana de Geologos Petroleros</title>
    <meta name="keywords" content="Asociación Mexicana de Geologos Petroleros" />
    <meta name="author" content="www.c-developersmx.com" />
    <meta name="copyright" content="2021 C-Developers MX Todos los derechos reservados." />
    <meta name="application-name" content="AMGP" />
    <meta name="robots" content="follow, index">
    <meta name="google" content="notranslate" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Kumar+One" rel="stylesheet">


    <!-- Cache Control  -->
    <!--meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" /-->
    <meta property="og:title" content="AMGP"/>
    <meta property="og:type" content="website" />
    <meta property="og:image" content="">
    <meta property="og:url" content="/"/>
    <meta property="og:description" content="Asociación Mexicana de Geologos Petroleros"/>
    <meta property="og:site_name" content="<?=$meta['url']?>"/>
    <meta property="og:image:width" content="800" />
    <meta property="og:image:height" content="600" />
    <!--Twitter-->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="Asociación Mexicana de Geologos Petroleros" />
    <meta name="twitter:description" content="Asociación Mexicana de Geologos Petroleros" />
    <meta name="twitter:image" content="" />
    <meta property="og:image:width" content="800" />
    <meta property="og:image:height" content="600" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/style.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/css/amgp.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/css/bootstrap.vertical-tabs.css">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/css/responsive.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/css/lightslider.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/css/slider/common.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/css/slider/demo.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/css/slider/style7.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/css/full-slider.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/css/gustavo.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/css/select2.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/front/src/css/bootstrap-timepicker.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <?php
      # Iniciando la variable de control que permitirá mostrar o no el modal
      $exibirModal = false;
      # Verificando si existe o no la cookie
      if(!isset($_COOKIE["mostrarModal"])){
        # Caso no exista la cookie entra aqui
        # Creamos la cookie con la duración que queramos
        //$expirar = 3600; // muestra cada 1 hora
        //$expirar = 10800; // muestra cada 3 horas
        //$expirar = 21600; //muestra cada 6 horas
        $expirar = 43200; //muestra cada 12 horas
        //$expirar = 86400;  // muestra cada 24 horas
        setcookie('mostrarModal', 'SI', (time() + $expirar)); // mostrará cada 12 horas.
        # Ahora nuestra variable de control pasará a tener el valor TRUE (Verdadero)
        $exibirModal = true;?>

      <?php }?>
    <!--[if IE]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <!--Plugin para comentarios por medio de facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.0&appId=294572587695577&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="fb-root"></div>
<header class="navbar navbar-default navbar-fixed-top navbar-inner">
    <div class="top_row row m0 header-social">
        <!--div class="container">
            <div class="row"-->
                <div class="col-sm-12 col-md-12 col-lg-12 p0 visible-xs">
                    <div class="navbar-header ">
                        <a href="/" class="logo">
                            <img src="<?php echo get_template_directory_uri(); ?>/front/src/images/logos/LOGO300x300.png" alt="" class="img-responsive logo">
                        </a>
                    </div>
                    <div class="main_menu_area">
                        <div class="mainmenu">
                            <nav>
                                <ul id="nav">
                                    <li class="current_page_item"><a href="/">Inicio</a>
                                    </li>
                                    <li ><a href="/acerca-de-amgp">Nosotros</a>
                                    </li>
                                    <li ><a href="/delegaciones">Delegaciones</a>
                                    </li>
                                    <li ><a href="/capitulos-estudiantiles">Capítulos Estudiantiles</a>
                                    </li>
                                    <li ><a href="/noticias">Noticias</a>
                                    </li>
                                    <li ><a href="/eventos">Eventos</a>
                                    </li>
                                    <li ><a href="/proximamente-publicaciones">Publicaciones</a>
                                    </li>
                                    <!-- <li><a href="/publicaciones">Publicaciones</a>
                                    </li> -->
                                    <li ><a href="/planes-registro">Membresia</a>
                                    </li>
                                    <li ><a href="/directorio">Directorio</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-lg-2 col-xs-12 logo-spectrum hidden-xs">
                    <div class="col-sm-10 col-md-10 p0">
                    <a href="/" class="logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/front/src/images/logos/LOGO300x300.png" alt="" class="img-responsive">
                    </a>
                    </div>
                    <div class="col-sm-2 col-md-2 p0">
                    <div class="top_quote hidden-md hidden-xs hidden-sm">
                        <h1 class="amgp">AMGP</h1>
                    </div>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 p0 hidden-xs">
                    <div class="col-sm-12 col-md-12 col-lg-12 p0 hidden-xs">

                        <div class="col-sm-4 col-md-5 p0">
                            <div class="top_quote hidden-sm">
                                <p class="nameheader">Asociación Mexicana de Geólogos Petroleros A C</p>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-7 p0">
                            <div class="social_reg_log_area">
                                <div class="col-sm-4 col-md-4 col-lg-4 col-xs-4">
                                    <div class="top_social_bar">
                                        
                                        <div class="col-sm-4 col-md-4 col-lg-4 col-xs-4">
                                            <p class="redes">Síguenos:</p>
                                        </div>
                                        <div class="col-sm-8 col-md-8 col-lg-8 col-xs-8">
                                            <a href="#"><i class="fab fa-facebook facebook coloramarillo" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fab fa-twitter-square twitter coloramarillo" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fab fa-linkedin linkedi coloramarillo" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fab fa-skype skype coloramarillo" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12 p0">
                        <div class="main_menu_area" style="margin-top: 1em !important;">
                            <div class="mainmenu">
                                <nav>
                                    <ul id="nav">
                                        <li class="current_page_item"><a href="/">Inicio</a>
                                        </li>
                                        <li ><a href="/acerca-de-amgp">Nosotros</a>
                                        </li>
                                        <li ><a href="/delegaciones">Delegaciones</a>
                                        </li>
                                        <li ><a href="/capitulos-estudiantiles">Capítulos  Estudiantiles</a>
                                        </li>
                                        <li ><a href="/noticias">Noticias</a>
                                        </li>
                                        <li ><a href="/eventos">Eventos</a>
                                        </li>
                                        <li ><a  href="/publicaciones">Publicaciones<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <ul class="sub-menu" style="width: 180px;">
                                                <li class="publi"><a href="/publicaciones/#">publi</a></li>
                                                <li class="publi gacetas"><a href="/publicaciones/4"><span class="gacetas">GACETAS</span></a>
                                                    <ul class="sub-menu" style="top: -3px;left: -76%;">
                                                        <li class="publi"><a href="/publicaciones/gacetas/nacional">Nacional</a></li>
                                                        <li class="publi"><span class="gacetas" style="padding-left: 0 !important;"><a href="#">Delegaciones</a></span>
                                                            <ul class="sub-menu sub-sub-menu" style="top: -3px;left: -107%;">
                                                                    <li class="publi"><a href="/delegaciones/dele/gacetas">gacetas</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="/directorio">Directorio</a>
                                        </li>
                                
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                <!--/div>
            </div-->
        </div>
    </div>
</header>
<?php /* Template Name: Events */ ?>
<?php get_header();?>
<section class="blog_area section_padding_100 eventos">
	<div class="container">
		<div class="row">
            <?php   $id=$_GET['delegacion'];
                    $category = get_cat_name($id);?>
			<div class="col-xs-12">
				<div class="section_heading text-center">
					<?php   
                        if($id){
		                        	$news=$post_list = get_posts( array(
				                        'post_status'   => 'publish',
				                        'orderby'       => 'date',
				                        'post_type'     => 'eventos',
                            			'category'      =>  $id,
				                        'order'         => 'DESC'
		                    		));

                        		}
                        else{
								$news=$post_list = get_posts( array(
			                        'post_status'   => 'publish',
			                        'orderby'       => 'date',
			                        'post_type'     => 'eventos',
			                        'order'         => 'DESC'
	                			));
                    		}
                    ?>
					<h3 class="titleevent">Eventos de <span>AMGP <?php if($id){ echo $category;} else{ echo "Nacional";}?></span></h3>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-9 col-sm-9 col-lg-9">
				<?php foreach($news as $value):?>

				<div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
						<div class="single_latest_news_area wow fadeInUp" data-wow-delay="0.2s">
							<div class="published_date2">
                                    <?php $title = substr($value->post_title, 0, 110); ?>
								<p class="date"><?=$title;?></p>
							</div>
							<div>
								<div class="single_latest_news_img_area">
									<img class="allnews" src="<?php echo  get_the_post_thumbnail_url($value->ID);?>" alt="">
								</div>
								<div class="single_latest_news_text_area">
									<div class="news_title text-center" >
										<a href="<?php echo get_permalink($value->ID); ?>" target="_blank">
											<?php $title = substr($value->post_title, 0, 110); ?>
											<h4 style="display: inline-block;vertical-align: middle!important;"><?=$title;?></h4>
										</a>
									</div>
									<div class="author_name_comments">
										<div class="comments">
											<i class="fas fa-map-marker color3" style="display: inline-block!important;padding-bottom: 1em;"></i>
											<p class="color3" style="display: inline-block!important; max-width: 300px"><?php $summary = substr($even->summary, 0, 200); ?>
										<?=$summary."";?></p>
										</div>
										<div class="comments">
											<i class="fa fa-eye color3" aria-hidden="true"></i>
											<p class="color3"><?=$even->visits;?></p>
										</div>
									</div>
									<div class="news_content">
										
									</div>
								</div>
							</div>
						</div>
				</div>
				<?php endforeach;?>
				<!--div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
					<h4 class="text-center no-events">Por el momento no hay eventos en AMGP</h4>
				</div-->
			</div>
			<div class="col-xs-12 col-md-3 col-lg-3 col-sm-3 testimonial_client_newview">
				<div class="col-xs-12 col-md-12 col-sm-12">
					<div class="event_sidebar">
						<div class="latest_post">
							<?php   $eventsold=$post_list = get_posts( array(
								'numberposts'      => 3,
		                        'post_status'   => 'publish',
		                        'orderby'       => 'date',
		                        'date_query'	=> array(
		                        	'after' =>  date('Y-m-d', strtotime('-10 days')) 
		                        ),
		                        'post_type'     => 'eventos',
		                        'order'         => 'DESC'
		                    ));
		                    ?>
							<h4 id="gradNews">Eventos <span>anteriores</span></h4>
							<?php foreach($eventsold as $mor):?>
								<div class="single_latest_post">
									<a href="<?php echo get_permalink($value->ID); ?>"><img src="<?php echo  get_the_post_thumbnail_url($value->ID);?>" alt=""></a>
									<a href="<?php echo get_permalink($value->ID); ?>">
										<?php $title2 = substr($value->post_title, 0, 110); ?>
										<h4 class="title-news"><?=$title2;?></h4>
									</a>
								</div>
							<?php endforeach;?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer();?>
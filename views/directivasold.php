<?php /* Template Name: Directivasold */ ?>
<?php get_header();?>
<div class="advisor section_padding_100_70 perfiles testimonial_client_area3dele">
    <div class="container ">
        <div class="row">
            <div class="col-lg-12 col-md=12 col-sm-12 col-xs-12 section_heading title-asociaciones ex">
                <div class="leftTitle col-md-4">
                </div>
                <div class="col-md-4">
                    <h3>Directivas<span> anteriores</span></h3>
                </div>
                <div class="rightTitle col-md-4">
                </div>
            </div>
            <div class="col-lg-12 col-md=12 col-sm-12 col-xs-12 bhoechie-tab-container directivas">
                <div class="container">
                    <ul class="nav nav-pills">
                        <?php   $directivaOld=$post_list = get_posts( array(
                            'post_status'   => 'private',
                            'orderby'       => 'date',
                            'post_type'     => 'directivas-nacionale',
                            'order'         => 'DESC'
                        ));
                        ?>
                        <?php foreach ($directivaOld as $key => $value): ?>
                        <?php $statusss=get_field("status_directiva",$value->ID);?>
                        <?php if($statusss=="INACTIVO"):?>
                                <li class="<?php if($key==0){echo "active";}?>"><a data-toggle="pill" href="#<?=$value->duracion?>"><?=$value->duracion?></a></li>
                            <?php endif;?>
                        <?php endforeach;?>
                    </ul>
                    <div class="tab-content">
                        <?php foreach ($directivaOld as $key2 => $value2):?>
                            <?php $statusss=get_field("status_directiva",$value2->ID);?>
                            <?php $duracion=get_field("duracion",$value2->ID);?>
                            <?php if($statusss=="INACTIVO"):?>
                                <div id="<?php echo $duracion;?>" class="tab-pane fade <?php if($key2==0){echo "active";}?> in">
                                    <?php $directivaaa=get_field("directiva_nacional_array",$value2->ID);?>
                                    <?php foreach($directivaaa as $key => $value3):?>
                                        <div class="col-xs-12 col-sm-6 col-md-3">
                                            <div class="single_advisor_profile  directiva wow fadeInUp" data-wow-delay="0.2s">
                                                <div class="advisor_thumb">
                                                    <img class="perfil" src="<?php if($value3['imagen']["url"]){ echo $value3['imagen']["url"];} else{ echo get_template_directory_uri().'/front/src/images/image-no.jpeg';}?>" alt="">
                                                </div>
                                                <div class="single_advisor_details_info">
                                                    <h5><?php echo $value3['nombre'];?></h5>
                                                    <p><?php echo $value3['cargo'];?></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            <?php endif;?>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer();?>
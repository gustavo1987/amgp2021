<?php get_header();?>
<section class="about_area home2 section_padding_100 delegation">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section_heading">
                <h3 class="text-center">Delegaciónnnn <span><?= get_the_title();?></span></h3>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<hr align="left" noshade="noshade" size="2" width="100%" />
<section class="welcome_area carousel slide" id="myCarousel" >
    <ol class="carousel-indicators">
        <?php $slider=get_field("sliders");?>
        <?php foreach($slider as $key => $value):?>
            <li data-target="#myCarousel" data-slide-to="<?=$key;?>" class="<?=$key==0?'active':''?>"></li>
        <?php endforeach;?>
    </ol>
    <div class="carousel-inner">
        <?php $count=1;  foreach($slider as $key => $value):?>
            <div class="item <?=$count==1?'active':''?>">
                <a href="<?php echo $value['enlace'];?>" target="_blank"><img class="img-responsive slider" src="<?php echo $value['imagen']['url'];?>" alt=""/></a>
            </div>
        <?php $count++;endforeach;?>
    </div>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</section>
<div class="clearfix"></div>
<hr align="left" noshade="noshade" size="2" width="100%" />
<section class="blog_area event-delegation" style="overflow:scroll;height:350px">
    <div class="container">
        <div class="row">
            <?php   
                    global $wp;
                    $porciones = explode("/", home_url( $wp->request ));
                    $urldele=$porciones[5];
                    if($urldele=="villahermosa"){$delegation=2;}
                    elseif($urldele=="coatzacoalcos"){$delegation=5;}
                    elseif($urldele=="ciudad-de-mexico"){$delegation=3;}
                    elseif($urldele=="ciudad-del-carmen"){$delegation=4;}
                    elseif($urldele=="paraiso-comalcalco"){$delegation=6;}
                    elseif($urldele=="poza-rica"){$delegation=7;}
                    elseif($urldele=="reynosa"){$delegation=8;}
                    elseif($urldele=="tampico"){$delegation=9;}
                    elseif($urldele=="veracruz"){$delegation=10;}
            ?>
            <div class="col-xs-6 col-md-3 col-sm-3 col-lg-3">
                <a href="/noticias/?delegacion=<?=$delegation;?>">
                    <div class="divisor">
                        <h3>Noticias</h3>
                        <p class="name-delegation"><?=$delegationAll->name;?></p>
                        <i class="fa fa-file-text" aria-hidden="true"></i>
                    </div>
                </a>
            </div>
            <div class="col-xs-6 col-md-3 col-sm-3 col-lg-3">
                <a href="/eventos/?delegacion=<?=$delegation;?>">
                    <div class="divisor">
                        <h3>Eventos</h3>
                        <p class="name-delegation"><?=$delegationAll->name;?></p>
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                    </div>
                </a>
            </div>
            <div class="col-xs-6 col-md-3 col-sm-3 col-lg-3">
                <a href="http://publicaciones.amgp.org/publicaciones/gacetas/<?=$delegationAll->permalink;?>/gacetas">
                    <div class="divisor">
                        <h3>Gacetas</h3>
                        <p class="name-delegation"><?=$urldele;?></p>
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                    </div>
                </a>
            </div>
            <div class="col-xs-6 col-md-3 col-sm-3 col-lg-3">
                <a href="/directorio/<?=$delegationAll->permalink;?>/directorio">
                    <div class="divisor">
                        <h3>Directorio</h3>
                        <p class="name-delegation"><?=$delegationAll->name;?></p>
                        <i class="fas fa-envelope" style="font-size: 120px;padding: 11px;color: #ecbb22;" aria-hidden="true"></i>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<hr align="left" noshade="noshade" size="2" width="100%" />
<section class="footer_area2">
    <div class="container">
        <div class="row footer">
            <div class="section_heading text-center">
                <h3><span class="title-big">OFICINA</span> Delegación <span><?= get_the_title();?></span></h3>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="footer_about_us wow fadeInUp" data-wow-delay="0.2s">
            		<?php $mapa=get_field("mapa");?>
                    <input class="hidden" id="mapaaa" value="<?php echo $mapa["lat"].",".$mapa["lng"];?>">
                    <style type="text/css">
                        #mapa { height: 400px; margin-bottom: 10px !important;}
                    </style>
                    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCYYyMtcLN3HroKg1NxSzZ-6bWv-xV3QoU"></script>
                    <script type="text/javascript">
                        function initialize() {
                            var marcadores = [
                                ['<a href="/delegaciones/<?= $delegationAll->permalink;?>"><?= $delegationAll->name;?></a>', <?= $delegationAll->lat;?>,<?= $delegationAll->long;?>,
                                    'title','Cd. del Carmen']
                            ];
                            var map = new google.maps.Map(document.getElementById('mapa'), {
                                zoom: 5,
                                center: new google.maps.LatLng(23.452595,-96.4994557),
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                scrollwheel: false
                            });
                            var infowindow = new google.maps.InfoWindow();
                            var marker, i;
                            for (i = 0; i < marcadores.length; i++) {
                                marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(document.getElementById('mapaaa').value),
                                    map: map
                                });
                                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                    return function() {
                                        infowindow.setContent(marcadores[i][0]);
                                        infowindow.open(map, marker);
                                    }
                                })(marker, i));
                            }
                        }
                        google.maps.event.addDomListener(window, 'load', initialize);
                    </script>
                    <div id="mapa"></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="row">
                    <div class="address-area">
                    	<?php $direction=get_field("calle");?>
                        <?php if($direction!=null):?>
                        <div class="single_part">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            <p class="white"><?php echo $direction;?></p>
                        </div>
                        <?php else:?>
                        <div class="single_part">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            <p class="white"></p>
                        </div>
                        <?php endif;?>

                    	<?php $RS=get_field("redes_sociales");?>
                        <?php if($RS!=null):?>

						<?php foreach($RS as $key => $value):?>
							<?php if($value["red_social"]=="Facebook"):?>
	                        <div class="single_part">
	                            <i class="fab fa-facebook" aria-hidden="true"></i>
	                            <a class="white" style="padding-top: 4%;"><?php echo $value["liga"];?></a>
	                        </div>
							<?php elseif($value["red_social"]=="Twitter"):?>
	                        <div class="single_part">
                                <i class="fab fa-twitter-square" aria-hidden="true"></i>
                                <a class="white" style="padding-top: 4%;"><?php echo $value["liga"];?></a>
                            </div>
							<?php elseif($value["red_social"]=="Linkedin"):?>
	                        <div class="single_part">
                                <i class="fab fa-linkedin-square" aria-hidden="true"></i>
                                <a class="white" style="padding-top: 4%;"><?php echo $value["liga"];?></a>
                            </div>
							<?php elseif($value["red_social"]=="Instagram"):?>
	                        <div class="single_part">
                                <i class="fab fa-instagram" aria-hidden="true"></i>
                                <a class="white" style="padding-top: 4%;"><?php echo $value["liga"];?></a>
                            </div>
							<?php elseif($value["red_social"]=="Youtube"):?>
	                        <div class="single_part">
                                <i class="fab fa-youtube-square" aria-hidden="true"></i>
                                <a class="white" style="padding-top: 4%;"><?php echo $value["liga"];?></a>
                            </div>
	                        <?php endif;?>
                        <?php endforeach;?>
                        <?php else:?>
                        <div class="single_part">
                            <i class="fab fa-facebook" aria-hidden="true"></i>
                            <a class="white"></a>
                        </div>
                        <div class="single_part">
                            <i class="fab fa-twitter-square" aria-hidden="true"></i>
                            <a class="white"></a>
                        </div>
                        <?php endif;?>
                    	<?php $phone=get_field("telefonos");?>
                        <?php if($phone!=null):?>
                            <div class="single_part">
						        <?php foreach($phone as $key => $value):?>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <a class="white" href="tel:<?php echo $value['telefono'];?>"><?php echo $value['telefono'];?></a>
                                <?php endforeach;?>
                            </div>
                        <?php else:?>
                        <div class="single_part">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <a class="white" href="tel:">Sin teléfono</a>
                        </div>
                        <?php endif;?>
                    	<?php $email=get_field("emails");?>
                        <?php if($email!=null):?>
                        <div class="single_part">
						    <?php foreach($email as $key => $value):?>
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <a class="white" href="<?php echo $value['email'];?>"><?php echo $value['email'];?></a>
                        	<?php endforeach;?>
                        </div>
                        <?php else:?>
                        <div class="single_part">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <a class="white" href=""></a>
                        </div>
                        <?php endif;?>
                    	<?php $day=get_field("dias");?>
                    	<?php $hour=get_field("horario_inicial");?>
                    	<?php $hour_end=get_field("horario_final");?>
                        <?php if(($day!=null||$hour!=null||$hour_end!=null)):?>
                        <div class="single_part">
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                            <?php $hour_open =date("H:i", strtotime($hour));?>
                            <?php $hour_closed =date("H:i", strtotime($hour_end));?>
                            <p class="white"><?php foreach($day as $key => $value):?><?php echo $value;?> <?php endforeach;?></p>
                            <p class="white"><?php echo $hour_open;?>-<?php echo $hour_closed;?></p>
                        </div>
                        <?php else:?>
                        <div class="single_part">
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                            <p class="white"></p>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<hr align="left" noshade="noshade" size="2" width="100%" />
<?php get_footer();?>
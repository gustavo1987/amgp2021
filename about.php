<?php /* Template Name: About */ ?>
<?php get_header();?>
<section class="home2 section_padding_60 about testimonial_client_area3dele">
	<div class="container about">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container about">
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bhoechie-tab-menu">
					<div class="list-group">
				        <?php $propie=get_field("propiedades"); ?>
				        <?php foreach ($propie as $key => $value): ?>
						<a href="#" class="list-group-item <?=$key==0?'active':''?> text-center">
							<?php echo $value['titulo'];?>
						</a>
						<?php endforeach;?>
						<a href="#" class="list-group-item text-center">
							Directiva Nacional
						</a>
						<a href="#" class="list-group-item text-center">
							Primera Directiva
						</a>
					</div>
				</div>
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab whiteback">
					
			        <?php $propie=get_field("propiedades"); ?>
			        <?php foreach ($propie as $key => $value): ?>
					<div class="bhoechie-tab-content <?=$key==0?'active':''?>  about">
						<section class="our_mission_vision_area section_padding_60 history">
							<div class="row">
								<div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
									<div class="about_us_text text-justify">
										<?php echo $value['contenido'];?>
									</div>
								</div>
							</div>
						</section>
					</div>
					<?php endforeach;?>
					<div class="bhoechie-tab-content about">
						<section class="our_mission_vision_area section_padding_60 directiva">
							<div class="section_heading title-asociaciones">
								<div class="leftTitle col-md-4">
								</div>
								<div class="col-md-4">
									<h3 class="title-directiva-nacional">Directiva <span>Nacional</span></h3>
								</div>
								<div class="rightTitle  col-md-4">
								</div>
							</div>
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="row">
										<?php   $directivaActual=$post_list = get_posts( array(
					                        'post_status'   => 'publish',
					                        'orderby'       => 'date',
					                        'post_type'     => 'directivas-nacionale',
					                        'order'         => 'DESC'
					                    ));
					                    ?>
					                    <!--?php print_r($directivaActual);?-->
								        <?php foreach ($directivaActual as $key => $value): ?>
								        <?php $statusss=get_field("status_directiva",$value->ID);?>
								        <?php if($statusss=="ACTIVO"):?>
								        <?php $directivaaa=get_field("directiva_nacional_array",$value->ID);?>
								        <?php foreach($directivaaa as $key => $value2):?>
											<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
												<div class="single_advisor_profile2 directiva">
													<div class="row">
														<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
															<img class="imgDirectiva" src="<?php if($value2['imagen']["url"]){ echo $value2['imagen']["url"];} else{ echo get_template_directory_uri().'/front/src/images/image-no.jpeg';}?> " alt="">
														</div>
														<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
															<div class="directivaDescription">
																<h5><?php echo $value2['nombre'];?></h5>
																<p class="descriptionDirectiva"><?php echo $value2['cargo'];?></p>
															</div>
														</div>
													</div>
												</div>
											</div>
										<?php endforeach;?>
										<?php endif;?>
										<?php endforeach;?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12">
								<button type="submit" class="btn btn-default more"><a href="/directivas-anteriores">Directivas anteriores</a></button>
							</div>
						</section>
					</div>
					<div class="bhoechie-tab-content about">
						<section class="our_mission_vision_area section_padding_60 primera">
							<div class="section_heading title-asociaciones">
								<div class="leftTitle col-md-4">
								</div>
								<div class="col-md-4">
									<h3>Primera <span>Directiva</span></h3>
								</div>
								<div class="rightTitle  col-md-4">
								</div>
							</div>
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<div class="carousel-inner" role="listbox">
									<div class="item active">
										<div class="row">
												<div class="col-xs-6 col-sm-6 col-md-4">
													<div class="single_advisor_profile2 directiva">
														<div class="row">
															<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																<img class="imgDirectiva" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/expresidentes/Manuel_Aguilar_1949-1951.png" alt="">
															</div>
															<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
																<div class="directivaDescription">
																	<h5>Manuel Rodríguez Aguilar</h5>
																	<p class="descriptionDirectiva">Presidente fundador</p>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-6 col-md-4">
													<div class="single_advisor_profile2 directiva">
														<div class="row">
															<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																<img class="imgDirectiva" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/expresidentes/Eduardo J. Guzmán 1963-1966.jpeg" alt="">
															</div>
															<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
																<div class="directivaDescription">
																	<h5>Eduardo J. Guzmán</h5>
																	<p class="descriptionDirectiva">Asociado fundador</p>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-6 col-md-4">
													<div class="single_advisor_profile2 directiva">
														<div class="row">
															<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																<img class="imgDirectiva" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/expresidentes/59fe1f0fcec0a_Manuel Alvarez y Alvarez.png" alt="">
															</div>
															<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
																<div class="directivaDescription">
																	<h5>Manuel Álvarez y Álvarez</h5>
																	<p class="descriptionDirectiva">Asociado fundador</p>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-6 col-md-4">
													<div class="single_advisor_profile2 directiva">
														<div class="row">
															<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																<img class="imgDirectiva" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/expresidentes/Guillermo Pardo Salas.png" alt="">
															</div>
															<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
																<div class="directivaDescription">
																	<h5>Guillermo Pardo Salas</h5>
																	<p class="descriptionDirectiva">Asociado fundador</p>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-6 col-sm-6 col-md-4">
													<div class="single_advisor_profile2 directiva">
														<div class="row">
															<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
																<img class="imgDirectiva" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/expresidentes/Antonio García Rojas.png" alt="">
															</div>
															<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
																<div class="directivaDescription">
																	<h5>Antonio García Rojas</h5>
																	<p class="descriptionDirectiva">Asociado fundador</p>
																</div>
															</div>
														</div>
													</div>
												</div>


										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="testimonial_client_area3dele section_padding_60 vision">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="about_us_thumb vision">
					<img class="mision-vision" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/597796f6d98ad_vision.png" alt="misión de la asociación mexicana de geologos petroleros">
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="about_us_text text-justify">
					<h3>Nuestra Misión</h3>
					<p>Seguir siendo reconocida nacional e internacionalmente como una asociación profesional, dinámica y por sus aportes técnico-científicos en el sector extractivo de la industria petrolera, promoviendo la presencia de las geociencias a lo largo de la cadena de valor.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="testimonial_client_area3dele section_padding_60 mision">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="about_us_text text-justify">
					<h3>Nuestra Visión</h3>
					<p>Seguir siendo reconocida nacional e internacionalmente como una asociación profesional, dinámica y por sus aportes técnico-científicos en el sector extractivo de la industria petrolera, promoviendo la presencia de las geociencias a lo largo de la cadena de valor.</p>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="about_us_thumb mision">
					<img class="mision-vision" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/597797012af19_about.png" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
<section class="cool_facts_area home2 section_padding_60">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-4">
				<div class="cool_fact_text wow fadeInUp" data-wow-delay="0.2s">
					<i class="fa fa-institution" aria-hidden="true"></i>
					<h3><span class="counter">9</span></h3>
					<h5>Delegaciones</h5>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4">
				<div class="cool_fact_text wow fadeInUp" data-wow-delay="0.4s">
					<i class="fa fa-users" aria-hidden="true"></i>
					<h3><span class="counter">1019</span></h3>
					<h5>Asociados</h5>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4">
				<div class="cool_fact_text wow fadeInUp" data-wow-delay="0.6s">
					<i class="fa fa-book" aria-hidden="true"></i>
					<h3><span class="counter">800</span></h3>
					<h5>Publicaciones</h5>
				</div>
			</div>
		</div>
	</div>
</section>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner" role="listbox">
		<div class="item active">
			<section class="our_advisor_area advisor section_padding_100_70about testimonial_client_area3dele">
				<div class="container">
					<div class="row">
						<div class="section_heading title-asociaciones">
							<div class="leftTitle col-md-4">
							</div>
							<div class="col-md-4">
								<h3>EX <span>PRESIDENTES</span></h3>
							</div>
							<div class="rightTitle  col-md-4">
							</div>
						</div>
				        <?php $expresi=get_field("expresidentes"); ?>
				        <?php foreach ($expresi as $key => $value): ?>
							<div class="col-xs-6 col-sm-6 col-md-2">
								<div class="single_advisor_profile expresidentes">
									<div class="advisor_thumb expresidentes">
										<img src="<?php echo $value['imagen']["url"];?>" alt="">
									</div>
									<div class="single_advisor_details_info">
										<h5><?php echo $value['nombre'];?></h5>
										<p><?php echo $value['fecha'];?></p>
									</div>
								</div>
							</div>
						<?php endforeach?>
						<div class="col-xs-12 more">
							<button type="submit" class="btn btn-default more2"><a href="/control/expresidentes">Ver Más</a></button>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
<div id="myCarousel2" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner" role="listbox">
		<div class="item active">
			<section class="our_advisor_area advisor section_padding_100_70about testimonial_client_area3dele">
				<div class="container">
					<div class="row">
						<div class="section_heading title-asociaciones">
							<div class="leftTitle col-md-4">
							</div>
							<div class="col-md-4">
								<h3>SOCIOS <span> FUNDADORES</span></h3>
							</div>
							<div class="rightTitle  col-md-4">
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Acevedo C. F.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Acosta E. R.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Aldredge R. F.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Alvarez Jr. M.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Arellano A. R. V.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Barclay S.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Barnetche G. A.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Bellicard J. C.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Benavides G. L.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Bennison A. P.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Browles, Jr. A. C.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Castillo T. C.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Castillón B. M.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Chapin T.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
							<div class="single_speciality sitios">
								<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
									<div class="single_speciality_icon">
										<a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
									</div>
								</div>
								<div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
									<div class="single_speciality_text sitios">
										<p class="sociosname">Coffin O. J.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 more">
							<button type="submit" class="btn btn-default more2"><a href="/socios-fundadores">Ver Más</a></button>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
<?php get_footer()?>
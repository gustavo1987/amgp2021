<?php /* Template Name: Home */ ?>
<?php get_header();?>

<section class="welcome_area carousel slide" style="padding-top: 113px;" id="myCarousel" >
    <ol class="carousel-indicators">
        <?php $slider=get_field("sliders"); ?>
        <?php foreach ($slider as $key => $value): ?>
        <li data-target="#myCarousel" data-slide-to="<?=$key;?>" class=" <?php if($key==0){ echo 'active';}?>"></li>
        <?php endforeach;?>
    </ol>
    <div class="carousel-inner">
        <?php $slider=get_field("sliders"); ?>
        <?php foreach ($slider as $key => $value): ?>
        <div class="item <?php if($key==0){ echo 'active';}?>">
            <?php if($value['tipo_enlace']=="SI");?><a href="<?php echo $value['enlace'];?>" target="_blank"><img class="img-responsive slider" src="<?php echo $value['imagen']["url"];?>" alt=""/><?php if($value['tipo_enlace']=="SI");?></a>
        </div>
        <?php endforeach;?>
    </div>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</section>
<div class="clearfix"></div>
<hr align="left" noshade="noshade" size="2" width="100%" />
<div class="partner_area section_padding_60_30 asociaciones">
    <div class="container">
        <div class="row">
            <div class="section_heading title-asociaciones">
                <div class="leftTitle col-md-4 col-xs-4 col-sm-4 col-lg-4">
                </div>
                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4">
                    <h3 class="title-beneficios">Otras <span>Asociaciones e Instituciones</span></h3>
                </div>
                <div class="rightTitle col-md-4 col-xs-4 col-sm-4 col-lg-4">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="partners_thumbs slide">
                    <?php $asociacion=get_field("asociaciones"); ?>
                    <?php foreach ($asociacion as $key => $value): ?>
                    <div class="single_partner_area">
                        <a href="<?php echo $value['pagina_web'];?>" target="_blank">
                            <div class="single_partner_thumb">
                                <img class="image-asociaciones" src="<?php echo $value['imagen']["url"];?>" alt="<?php echo $value['imagen']["alt"];?>">
                            </div>
                        </a>
                        <p class="nameAsociados"><?php echo $value['titulo'];?></p>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<hr align="left" noshade="noshade" size="2" width="100%" />
<section class="testimonial_client_area3dele home2 section_padding_60 inicio">
    <div class="container">
        <div class="row">
            <div class="section_heading text-center bene">
                <div class="leftTitle col-md-4 col-xs-4 col-sm-4 col-lg-4">
                </div>
                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4">
                    <h3 class="title-beneficios">Beneficios de ser <span> asociado AMGP</span></h3>
                </div>
                <div class="rightTitle  col-md-4 col-xs-4 col-sm-4 col-lg-4">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container inicio">
                <i class="fa fa-angle-up beneficios" aria-hidden="true"></i>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bhoechie-tab-menu" style="overflow-y: hidden;" id="wn2">
                    <div class="list-group" id="lyr2">
                        <?php $beneficios=get_field("beneficios"); ?>
                        <?php foreach ($beneficios as $key => $value): ?>
                        <a href="#" class="list-group-item <?php if($key==0){ echo 'active';}?> text-center" data-image="https://amgp.org/api/administration/slider/58d0631bba65d_58cc22ff746fa_58c39b9a3c31d_slider2.jpg"><?php echo $value['beneficio'];?></a>
                        <?php endforeach;?>
                    </div>
                </div>
                <i class="fa fa-angle-down beneficios" aria-hidden="true"></i>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab">
                    <?php foreach ($beneficios as $key => $value): ?>
                    <div class="bhoechie-tab-content <?php if($key==0){ echo 'active';}?>">
                        <section class="our_mission_vision_area">
                            <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                    <div class="about_us_text text-justify">
                                        <?php echo $value['contenido'];?>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>
<hr align="left" noshade="noshade" size="2" width="100%" />
<section class="blog_area section_padding_100_50 asociate">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3">
                <img class="imagen-asociado" src="<?php echo get_template_directory_uri(); ?>/front/src/images/parallax/monos.png">
            </div>
            <div class="col-xs-12 col-sm-9 col-md-9">
                <div class="row">
                    <img class="imagen-asociado" src="<?php echo get_template_directory_uri(); ?>/front/src/images/parallax/letras.png" alt=""/>
                <!--a href="" class="howinscription" data-toggle="modal" data-target="#myModal">¿Cómo me asocio?</a-->
                <button type="button" class="btn btn-warning more"><a href="/manteinscripcion">Leer más</a></button>
                </div>
            </div>
        </div>
    </div>
</section>
<hr align="left" noshade="noshade" size="2" width="100%" />
<section class="footer_area2">
    <div class="container">
        <div class="row footer">
            <div class="section_heading text-center">
                    <h3>Delegaciones de <span>AMGP</span></h3>
            </div>
            <!--div class="title section_heading fo_black">
                <h4 class=titledelegationindex>Delegaciones de <span>AMGP</span></h4>
            </div-->
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="footer_about_us wow fadeInUp" data-wow-delay="0.2s">
                    <?php   $delega=$post_list = get_posts( array(
                        'post_status'   => 'publish',
                        'orderby'       => 'date',
                        'post_type'     => 'delegaciones',
                        'order'         => 'DESC'
                    ));
                    ?>
                    <style type="text/css">
                        #mapa { height: 300px; margin-bottom: 10px !important;}
                    </style>
                    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCYYyMtcLN3HroKg1NxSzZ-6bWv-xV3QoU"></script>
                    <script type="text/javascript">
                        function initialize() {
                            var marcadores = [
                                <?php foreach($delega as $compa):?>
                                ['<a href="/delegaciones/<?=$compa->post_name?>"><?=$compa->post_title?></a>', <?=$compa->lat?>, <?=$compa->log?>,
                                    'title','Villahermosa'],
                                <?php endforeach;?>

                            ];
                            var map = new google.maps.Map(document.getElementById('mapa'), {
                                zoom: 5,
                                center: new google.maps.LatLng(23.452595,-96.4994557),
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                scrollwheel: false
                            });
                            var infowindow = new google.maps.InfoWindow();
                            var marker, i;
                            for (i = 0; i < marcadores.length; i++) {
                                marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(marcadores[i][1], marcadores[i][2]),
                                    map: map
                                });
                                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                    return function() {
                                        infowindow.setContent(marcadores[i][0]);
                                        infowindow.open(map, marker);
                                    }
                                })(marker, i));
                            }
                        }
                        google.maps.event.addDomListener(window, 'load', initialize);
                    </script>
                    <div id="mapa"></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="footer_contact_info wow fadeInUp" data-wow-delay="0.6s">
                <?php foreach($delega as $compa):?>  
                    <div class="footer_single_contact_info">
                        <i class="fa fa-globe" aria-hidden="true"></i>
                        <p><a class="white" href="/delegaciones/<?=$compa->post_name?>"><?=$compa->post_title?></a></p>
                    </div>
                <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</section>
<hr align="left" noshade="noshade" size="2" width="100%" />
<section class="testimonial_client_area home2 section_padding_60">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="testimonials_area">
                    <div class="section_heading interes text-center">
                            <h3>Sitios de <span>interes</span></h3>
                    </div>
                    <div class="row">
                            <?php $sitios=get_field("sitios"); ?>
                            <?php foreach ($sitios as $key => $value): ?>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                                <div class="single_speciality sitios">
                                    <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                        <div class="single_speciality_icon">
                                            <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo $value['imagen']["url"];?>" alt="<?php echo $value['imagen']["alt"];?>"></a>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                        <div class="single_speciality_text sitios">
                                            <h5><a class="namesitios" href="<?php echo $value['url'];?>" target="_blank"><?php echo $value['sitio'];?></a></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach;?>
                        <div class="col-xs-12 more">
                            <button type="submit" class="btn btn-default more2"><a href="/sitios-de-interes">Ver Más</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<hr align="left" noshade="noshade" size="2" width="100%" />
<section class="section_padding_60 gacetas">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="testimonials_area">
                    <div class="section_heading interes text-center">
                        <h3>Gaceta  <span>La Nacional</span></h3>
                    </div>
                    <div class="row">
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 wow fadeInUp">
                                <div class="single_speciality sitios">
                                   <div class="container mt-40">
                                        <div class="row mt-30">
                                            <?php $gacetas=get_field("gacetas"); ?>
                                            <?php foreach ($gacetas as $key => $value): ?>
                                            <div class="col-md-4 col-sm-6">
                                                <div class="box16">
                                                    <img src="<?php echo $value['portada']["url"];?>">
                                                    <div class="box-content">
                                                        <h3 class="title"><?php echo $value['titulo'];?></h3>
                                                        <ul class="social">
                                                            <li><a href="<?php echo $value["url"];?>"><i class="fa fa-eye"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="col-xs-12 more">
                            <button type="submit" class="btn btn-default more2"><a href="/publicaciones/gacetas/nacional">Ver Más</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<hr align="left" noshade="noshade" size="2" width="100%" />
<?php get_footer()?>
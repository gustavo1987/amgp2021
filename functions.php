<?php
	function wpb_custom_new_menu() {
	 	register_nav_menu('my-custom-menu',__( 'My Custom Menu' ));
	}
	add_action( 'init', 'wpb_custom_new_menu' );
	add_theme_support( 'post-thumbnails' );

	add_filter('wpcf7_form_elements', function($content)
	{     $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/>/i', '', $content);
	return $content; });




add_filter( 'wp_terms_checklist_args', function( $args ) {
    global $pagenow;
    if ( is_admin() && 'post-new.php' === $pagenow && isset( $_GET['cat'] ) ) {
        $cat = array();
        if ( is_array( $_GET['cat'] ) ) {
            $cat = array_filter( $_GET['cat'], 'absint' );
        } else {
            $cat = (array) absint( $_GET['cat'] );
        }
        $args['selected_cats'] = $cat;
    }
    return $args;
} );

/**
 * Create a new post link with preselected category
 *
 * @param int|array $cats Preselected category id
 * @return void
 */
function op_create_selected_cat_post_link( $cats, $text = 'Create a new post' ) {
    if ( ! is_user_logged_in() || ! current_user_can( 'edit_posts' ) ) {
        return;
    }

    $url = admin_url( add_query_arg(
        array(
            'cat' => $cats,
        ),
        'post-new.php'
    ) );

    echo '<a href="' . esc_url( $url ) . '">' . esc_html( $text ) . '</a>';
}
<?php get_header();?>
<section class="single-blog-post-area section_padding_100 news">
    <div class="container new">
        <div class="row">
            <div class="col-xs-12 col-md-9 col-sm-9">
                <div class="singl-blog-post">
                    <figure>
                        <div class="singl-blog-status-bar title-news wow fadeInUp" data-wow-delay="0.2s">
                            <span>
                                <h1><?= get_the_title();?></h1>
                            </span>
                        </div>
                        <div class="singl-blog-status-bar wow fadeInUp" data-wow-delay="0.2s">
                            <span>
                                <i class="fas fa-user"></i>
                                <a href=""><?=$events->summary;?></a>
                            </span>
                            <br>
                            <span>
                                <i class="fa fa-clock-o block2"></i>
                                <a href="">Fecha de publicación: <?=$date2=date("d",strtotime($events->date_public));?> <?php $date2=date("m",strtotime($events->date_public)); echo $meses[$date2];?> <?=$date2=date("Y",strtotime($events->date_public));?></a>
                            </span>
                            <span>
                                <i class="fas fa-folder-open"></i>
                                <a href=""><?=$delegation->name;?></a>
                            </span>
                            <span>
                                <i class="fa fa-eye "></i>
                                <a href=""><?=$events->visits;?> leídas</a>
                            </span>
                        </div>

                        <div class="singl-blog-details text-justify wow fadeInUp" data-wow-delay="0.2s">
                            <?= get_the_content();?>
                        </div>
                    </figure>
                </div>
                <!--div class="fb-comments" data-href="http://amgp.org/" data-width="100%" data-numposts="5"></div-->
            </div>
            <div class="col-xs-12 col-md-3 col-sm-3 testimonial_client_newview">
                <div class="event_sidebar">
                    <div class="latest_post">
                        <h4 id="gradNews">Noticias más <span>leídas</span></h4>
                        <?php foreach($more as $mor):?>
                            <div class="single_latest_post">
                                <a href="/noticias/<?=$mor->permalink;?>/<?=$mor->pid;?>"><img src="/api/images/eventos/800x600/<?=$mor->image;?>" alt=""></a>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>
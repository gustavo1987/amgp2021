<?php /* Template Name: Delegation */ ?>
<?php get_header();?>
<section class="gallery_area column clearfix section_padding_100 testimonial_client_area3dele">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section_heading text-center">

                    <?php   $delegationAll=$post_list = get_posts( array(
                        'post_status'   => 'publish',
                        'orderby'       => 'date',
                        'post_type'     => 'delegaciones',
                        'order'         => 'DESC'
                    ));
                    ?>
                    <div class="leftTitle col-md-4">
                    </div>
                    <div class="col-md-4">
                        <h3 class="delegation">DELEGACIONES</h3>
                    </div>
                    <div class="rightTitle  col-md-4">

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="gallery_full_width_images_area">
        <div class="container">
            <div class="row">
                <?php foreach($delegationAll as $dele):?>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="single_gallery_item four-column wow fadeInUp" data-wow-delay="0.2s">
                        <p class="title-delegation"><?= $dele->post_title;?></p>
                        <img src="<?= get_the_post_thumbnail_url($dele->ID);?>" alt="">
                        <div class="hover_overlay">
                            <div class="gallery_info">
                                <a href="<?php echo get_permalink($dele->ID); ?>"><h5><?= $dele->post_title;?></h5></a>
                                <p><?=$dele->name_abbreviated;?></p>
                            </div>
                            <div class="links">
                                <a href="<?php echo get_permalink($dele->ID); ?>"><i class="fa fa-search" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>
<?php get_footer()?>
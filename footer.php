<footer class="footer_area">
    <div class="footer_bottom_area">
        <div class="container">
            <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-3 footer">
                    <div class="footer_bottom wow fadeInDown" data-wow-delay="0.2s">
                        <p class="footer-cdevelopers">AMGP <i class="fa fa-copyright" aria-hidden="true"></i> 2017. Todo los derechos reservados</a></p>
                        <p class="footer-cdevelopers margin-footer" style="margin-top: 1em;"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;(+52 0155) 55 6788 40</p>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 footer">
                    <div class="footer_bottom wow fadeInDown" data-wow-delay="0.2s">
                        <p class="footer-cdevelopers">Poniente 134 No. 411 Colonia San Bartolo Atepehuacan Alcaldía Gustavo A. Madero, Ciudad de México</a></p>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 footer">
                    <div class="footer_bottom wow fadeInDown" data-wow-delay="0.2s">
                        <p class="footer-cdevelopers">Realizado <i class="fab fa-html5" aria-hidden="true"></i> por <a href="http://www.c-developers.com/">C-Developers</a></p>
                        <p class="footer-cdevelopers margin-footer" style="margin-top: 1em;"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;presidencia@amgp.org</p>
                    </div>
                </div>
		<div class="col-xs-2-col-sm-2 col-md-2 footer">

<span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=9ynoMfiHZh0dY5ffwwPhuugSLCv3F1FplYs6w5aEGgKIakdrXHpJJfXVzsKe"></script></span>
		</div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/jquery-1.12.3.min.js"></script>


<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/lightslider.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/jquery.nivoslider.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/nivoslider-active.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/Chart.bundle.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/chart.active.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/ajax-contact.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/coundown-timer.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/meanmenu.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/jquery.waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/counterup.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/jquery.scrollUp.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/jquery-Stepize.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/jquery.sticky.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/simple-lens.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/js/session/session.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/bootstrapV/formValidation.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/bootstrapV/bootstrapV.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/bootstrapV/es_ES.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/modernizr.custom.79639.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/custom.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/src/js/amgp.js"></script>
<script>
    $(document).ready(function() {
        $('#vertical').lightSlider({
            gallery:true,
            item:1,
            vertical:true,
            verticalHeight:410,
            vThumbWidth:250,
            thumbItem:4,
            thumbMargin:2,
            slideMargin:0,
            currentPagerPosition:'right'
        });
        $('#imageGallery').lightSlider({
            gallery:true,
            item:1,
            loop:true,
            thumbItem:9,
            slideMargin:0,
            enableDrag: false,
            currentPagerPosition:'left'
        });

    })
</script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58ba250934f52909"></script>

<script>
    setTimeout(function(){
        $(".at-expanding-share-button-toggle-bg").text("Compartir");
    },1000);
</script>
<script type="text/javascript">
    $(function(){
        $('#form1').StepizeForm()
    });
</script>
<script>
    $(document).ready(function() {
        var button = '<ul><li><a class="closedfinish" role="menuitem"  data-toggle="modal" data-target="#saveandcontinue" >Guardar y salir</a></li></ul>';
        $('div.actions').append(button);

        var buttonCancel = '<ul><li><a class="closedCancel" data-toggle="modal" data-target="#closeandcancel" role="menuitem">Cancelar</a></li></ul>';
        $('div.actions').append(buttonCancel);
    });

    $("#newClientProfile input[type=radio]").each(function(i){
        $(this).click(function () {
            if(i==1) { //3rd radiobutton
                $("#textbox1").attr("disabled", "disabled");
                $("#checkbox1").attr("disabled", "disabled");
                $("#name").attr("disabled", "disabled");
                $("#lastname").attr("disabled", "disabled");
                $("#secondname").attr("disabled", "disabled");
                $("#sex").attr("disabled", "disabled");
                $("#comid").attr("disabled", "disabled");
                $("#email").attr("disabled", "disabled");
                $("#phone").attr("disabled", "disabled");
                $("#tel").attr("disabled", "disabled");
                $("#telOficina").attr("disabled", "disabled");
                $("#birthdate").attr("disabled", "disabled");
                $("#asociadodesde").attr("disabled", "disabled");
                $("#estadoCivil").attr("disabled", "disabled");
                $("#nacionalidad").attr("disabled", "disabled");
                $("#rfc").attr("disabled", "disabled");
                $("#curp").attr("disabled", "disabled");
                $("#facebook").attr("disabled", "disabled");
                $("#twitter").attr("disabled", "disabled");
                $("#instagram").attr("disabled", "disabled")

            }
            else {
                $("#textbox1").removeAttr("disabled");
                $("#checkbox1").removeAttr("disabled");
                $("#name").removeAttr("disabled");
                $("#lastname").removeAttr("disabled");
                $("#secondname").removeAttr("disabled");
                $("#sex").removeAttr("disabled");
                $("#comid").removeAttr("disabled");
                $("#email").removeAttr("disabled");
                $("#phone").removeAttr("disabled");
                $("#tel").removeAttr("disabled");
                $("#telOficina").removeAttr("disabled");
                $("#birthdate").removeAttr("disabled");
                $("#asociadodesde").removeAttr("disabled");
                $("#estadoCivil").removeAttr("disabled");
                $("#nacionalidad").removeAttr("disabled");
                $("#rfc").removeAttr("disabled");
                $("#curp").removeAttr("disabled");
                $("#facebook").removeAttr("disabled");
                $("#twitter").removeAttr("disabled");
                $("#instagram").removeAttr("disabled");

            }
        });
    });
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/front/js/sweetalert2.all.js"></script>
</body>
</html>
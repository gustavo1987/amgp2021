<?php /* Template Name: Sociosfundadores */ ?>
<?php get_header();?>
<div id="myCarousel2" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <section class="our_advisor_area advisor section_padding_100_70about fundadores testimonial_client_area3dele">
                <div class="container">
                    <div class="row">

                        <div class="section_heading title-asociaciones">
                            <div class="leftTitle col-md-4">
                            </div>
                            <div class="col-md-4">
                                <h3>SOCIOS <span> FUNDADORES</span></h3>
                            </div>
                            <div class="rightTitle  col-md-4">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Acevedo C. F.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Acosta E. R.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Aldredge R. F.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Alvarez Jr. M.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Arellano A. R. V.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Barclay S.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Barnetche G. A.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Bellicard J. C.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Benavides G. L.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Bennison A. P.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Browles, Jr. A. C.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Castillo T. C.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Castillón B. M.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Chapin T.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Coffin O. J.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Gill J. P.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Ruíz E. J.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Rummerfield B. F.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Rodríguez A. R.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Salas G. P.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Schmitter E.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Rodríguez Aguilar M.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Schneider J. J.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Rivadeneira C. G.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Ríos Z. A.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Skala H.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Quiros M.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Soper E. K.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Suárez C. R.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Pérez F. R.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Thomas J. W.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Pauley E. W.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Torres I. L.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Weatherson D.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Young G. L.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Coleman F. Conley H. R.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Cumming J. L.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">De los Santos V. S.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Díaz Lozano E.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Figueroa H. S.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Folk S. H.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">García Rojas A.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Garfias V. R.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Gibson J. B.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Gillespie B. W.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Godde H. A.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Green C. H.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Green W. G.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Guerra P. F. Guseman L. F.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Gúzman E. J.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Hernández Sánchez Mejorada S.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Hurdall J. Kidd R. L.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Lafleur C. C.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Laso G. D.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Laso G. F.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Lassauzet R.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Lee Jr. J. E.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">López Ramos E.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">MacNaughton L. W.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Maldonado K. M.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">McCullaugh Jr. J.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Meneses de Gyves J.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Morán A.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Olhovich V. A.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Olivas R. M.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Ortíz T.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Pauley E. W.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 wow fadeInUp">
                            <div class="single_speciality sitios">
                                <div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 imagesitios">
                                    <div class="single_speciality_icon">
                                        <a class="namesitios" href="#" target="_blank"><img class="iconosSitios" src="<?php echo get_template_directory_uri(); ?>/front/src/images/about/logo.jpeg" alt="fundadores de la asociación mexicana de geologos petroleros"></a>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-8 col-sm-8 col-lg-8 titlesitios">
                                    <div class="single_speciality_text sitios">
                                        <p class="sociosname">Whetsel R. V.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<?php get_footer()?>
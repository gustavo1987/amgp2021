<?php /* Template Name: News */ ?>
<?php get_header();?>
<section class="blog_area section_padding_100 newsindex">
    <div class="container">
        <div class="row">
            <?php   $id=$_GET['delegacion'];
                    $category = get_cat_name($id);?>
            <div class="col-xs-12">
                <div class="section_heading text-center">
                    <?php   
                        if($id){
                            $news=$post_list = get_posts( array(
                            'post_status'   => 'publish',
                            'orderby'       => 'date',
                            'order'         => 'DESC',
                            'category'      =>  $id,
                            'post_type'     => 'noticias'
                            ));
                        }
                        else{
                            $news = get_posts( array(
                                'post_status'   => 'publish',
                                'orderby'       =>  'date',
                                'order'         =>  'DESC',
                                'post_type'     =>  'noticias'
                            ));
                        }

                    ?>
                    <h3 class="titlenew">Noticias de <span>AMGP <?php if($id){ echo $category;} else{ echo "Nacional";}?></span></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9 col-lg-9 col-xs-12">
                <?php foreach($news as $value):?>
                <div class="col-xs-12 col-md-6 col-sm-6 col-lg-6">
                    <div class="single_latest_news_area wow fadeInUp" data-wow-delay="0.2s">
                        <div class="single_latest_news_img_area">
                            <img class="allnews" src="<?php echo  get_the_post_thumbnail_url($value->ID);?>" alt="">
                        </div>
                        <div class="single_latest_news_text_area">
                            <div class="news_title">
                                <a href="<?php 
                                    echo get_permalink($value->ID); ?>">
                                    <?php $title = substr($value->post_title, 0, 110); ?>
                                    <h4 class="title-note"><?=$title;?></h4>
                                </a>
                            </div>
                            <div class="author_name_comments">
                                <div class="comments">
                                    <i class="fa fa-calendar color1" aria-hidden="true"></i>
                                    <p><?=$date2=date("Y",strtotime($value->post_date));?> <?php $date2=date("m",strtotime($value->post_date)); echo $meses[$date2];?><?=$date2=date("Y",strtotime($value->post_date));?></p>
                                </div>
                            </div>
                            <div class="news_content">
                                <p><?php  $category_detail=get_the_category($value->ID);
                                            foreach($category_detail as $cd){
                                            echo $cd->cat_name."-";}?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
            <div class="col-xs-12 col-md-3 testimonial_client_newview">
                <div class="event_sidebar">
                    <div class="latest_post">
                        <h4 id="gradNews">Noticias más <span>leídas</span></h4>
                        <?php foreach($more as $mor):?>
                        <div class="single_latest_post">
                            <a href="/noticias/<?=$mor->permalink;?>/<?=$mor->pid;?>"> <img src="/api/images/eventos/800x600/<?=$mor->image;?>" alt=""></a>
                            <a href="/noticias/<?=$mor->permalink;?>/<?=$mor->pid;?>">
                                <?php $title = substr($mor->title, 0, 100); ?>
                                <h4 class="title-news"><?=$title;?></h4>
                            </a>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer()?>